/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.actions;

import java.io.File;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import mpimp.phenotypingschemecomposer.db.DBUtil;
import mpimp.phenotypingschemecomposer.db.EntityRecord;
import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.db.ValueRecord;
import mpimp.phenotypingschemecomposer.util.FileUtilities;
import mpimp.phenotypingschemecomposer.util.TPCProperties;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.interceptor.ScopedModelDriven;

public class TPCAdministrationAction extends TPCAbstractAction
		implements ScopedModelDriven<TPCModel>, ServletRequestAware {

	private static final long serialVersionUID = -1906025849991420255L;

	@Override
	public String execute() throws Exception {
		if (buttonName_.equals("saveEntity")) {
			buttonName_ = "";
			return saveEntity();
		} else if (buttonName_.equals("cancelNewEntity")) {
			buttonName_ = "";
			return cancelNewEntity();
		} else if (buttonName_.equals("updateEntity")) {
			buttonName_ = "";
			return updateEntity();
		} else if (buttonName_.equals("newEntity")) {
			buttonName_ = "";
			return newEntity();
		} else if (buttonName_.equals("saveAttributeAndValue")) {
			buttonName_ = "";
			return saveAttributeAndValue();
		} else if (buttonName_.equals("saveValueGroup")) {
			buttonName_ = "";
			return saveValueGroup();
		} else if (buttonName_.equals("cancelNewValueGroup")) {
			buttonName_ = "";
			return cancelNewValueGroup();
		} else if (buttonName_.equals("newValueGroup")) {
			buttonName_ = "";
			return newValueGroup();
		} else if (buttonName_.equals("updateValueGroup")) {
			buttonName_ = "";
			return updateValueGroup();
		} else if (buttonName_.equals("cancelNewAttributeAndValue")) {
			buttonName_ = "";
			return cancelNewAttributeAndValue();
		} else if (buttonName_.equals("updateAttributeAndValue")) {
			buttonName_ = "";
			return updateAttributeAndValue();
		} else if (buttonName_.equals("newAttributeAndValue")) {
			buttonName_ = "";
			return newAttributeAndValue();
		} else if (buttonName_.equals("wantsToSaveChanges")) {
			buttonName_ = "";
			return wantsToSaveChanges();
		} else if (buttonName_.equals("cancelSaveChanges")) {
			buttonName_ = "";
			return cancelSaveChanges();
		} else if (buttonName_.equals("uploadImage")) {
			buttonName_ = "";
			return uploadImage();
		} else if (buttonName_.equals("saveProject")) {
			buttonName_ = "";
			return saveProject();
		} else if (buttonName_.equals("cancelNewProject")) {
			buttonName_ = "";
			return cancelNewProject();
		} else if (buttonName_.equals("newProject")) {
			buttonName_ = "";
			return newProject();
		} else if (buttonName_.equals("updateProject")) {
			buttonName_ = "";
			return updateProject();
		} else if (buttonName_.equals("backToMainPage")) {
			buttonName_ = "";
			return backToMainPage();
		} else {
			if (tpcModel_ != null) {
				if (tpcModel_.getSubmitMethod().equals("updateProjectSpecificData")) {
					updateProjectSpecificData();
				} else if (tpcModel_.getSubmitMethod().equals("updateEntityData")) {
					updateEntityData();
				} else if (tpcModel_.getSubmitMethod().equals("updateAttributeAndValueData")) {
					updateAttributeAndValueData();
				} else if (tpcModel_.getSubmitMethod().equals("readAndUpdateAttributeAndValueData")) {
					tpcModel_.setValuesForAdmin(DBUtil.readValues(0));
					updateAttributeAndValueData();
				}  else if (tpcModel_.getSubmitMethod().equals("updateValueGroupSpecificData")) {
					tpcModel_.setValuesForAdmin(DBUtil.readValues(0));
					updateValueGroupSpecificData();
				}
			} else {
				throw new Exception("Internal serious implementation error: No TPCModel instance.");
			}
			return SUCCESS;
		}
	}
	
	private String newProject() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.setNewProject(true);
			tpcModel_.setCurrentProjectForAdminID(0);
			tpcModel_.setCurrentProjectForAdminName("");
			updateProjectSpecificData();
			tpcModel_.setSelectedTab("tab4");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String newValueGroup() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.setNewValueGroup(true);
			tpcModel_.setCurrentValueGroupForAdminID(0);
			tpcModel_.setCurrentValueGroupForAdminName("");
			tpcModel_.setCurrentValueGroupForAdminNameD("");
			updateValueGroupSpecificData();
			tpcModel_.setSelectedTab("tab3");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String saveProject() throws Exception {
		if (tpcModel_ != null) {
			//updateLists();
			doSaveProject();
			tpcModel_.setNewProject(false);
			tpcModel_.setSelectedTab("tab4");
			updateProjectSpecificData();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String saveValueGroup() throws Exception {
		if (tpcModel_ != null) {
			//updateLists();
			doSaveValueGroup();
			tpcModel_.setNewValueGroup(false);
			tpcModel_.setSelectedTab("tab3");
			updateValueGroupSpecificData();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String updateProject() throws Exception {
		if (tpcModel_ != null) {
			//updateLists();
			doUpdateProject();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}
	
	private String updateValueGroup() throws Exception {
		if (tpcModel_ != null) {
			//updateLists();
			doUpdateValueGroup();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String newEntity() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.setNewEntity(true);
			tpcModel_.setCurrentEntityForAdminID(0);
			tpcModel_.setCurrentEntityForAdminParameter("");
			tpcModel_.setCurrentEntityForAdminParameterD("");
			tpcModel_.setCurrentEntityForAdminPO_id("");
			tpcModel_.setCurrentEntityForAdminDefinition("");
			updateEntityData();
			tpcModel_.setSelectedTab("tab1");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String newAttributeAndValue() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.setNewAttributeAndValue(true);
			tpcModel_.setCurrentValueForAdminID(0);
			tpcModel_.setCurrentValueForAdminAttribut_e("");
			tpcModel_.setCurrentValueForAdminAttribut_d("");
			tpcModel_.setCurrentValueForAdminValue("");
			updateAttributeAndValueData();
			tpcModel_.setSelectedTab("tab2");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String saveEntity() throws Exception {
		if (tpcModel_ != null) {
			//updateLists();
			doSaveEntity();
			tpcModel_.setNewEntity(false);
			tpcModel_.setSelectedTab("tab1");
			updateEntityData();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String updateEntity() throws Exception {
		if (tpcModel_ != null) {
			//updateLists();
			doUpdateEntity();
			tpcModel_.setSelectedTab("tab1");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String saveAttributeAndValue() throws Exception {
		if (tpcModel_ != null) {
			//updateLists();
			doSaveAttributeAndValue();
			tpcModel_.setNewAttributeAndValue(false);
			tpcModel_.setSelectedTab("tab2");
			updateAttributeAndValueData();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String updateAttributeAndValue() throws Exception {
		if (tpcModel_ != null) {
			//updateLists();
			doUpdateAttributeAndValue();
			tpcModel_.setSelectedTab("tab2");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String cancelNewEntity() throws Exception {
		clearAdministrationData();
		reinit();
		updateEntityData();
		return INPUT;
	}

	private String cancelNewProject() throws Exception {
		clearAdministrationData();
		reinit();
		updateProjectSpecificData();
		return INPUT;
	}

	private String cancelNewValueGroup() throws Exception {
		clearAdministrationData();
		reinit();
		updateValueGroupSpecificData();
		return INPUT;
	}
	
	private String cancelNewAttributeAndValue() throws Exception {
		clearAdministrationData();
		reinit();
		updateAttributeAndValueData();
		return INPUT;
	}

	private void updateProjectSpecificData() throws Exception {
		if (tpcModel_ != null) {
			if (tpcModel_.getCurrentProjectForAdminID() == 0) {
				tpcModel_.setSelectedEntities(new ArrayList<EntityRecord>());
				tpcModel_.setSelectedValues(new ArrayList<ValueRecord>());
			} else {
				tpcModel_.setSelectedEntities(DBUtil.readEntities(tpcModel_.getCurrentProjectForAdminID()));
				tpcModel_.setSelectedValues(DBUtil.readValues(tpcModel_.getCurrentProjectForAdminID()));
				tpcModel_.setCurrentProjectForAdminName(
						tpcModel_.getProjectRecordForId(tpcModel_.getCurrentProjectForAdminID()).getName());
			}
			tpcModel_.setNotSelectedEntities(DBUtil.readEntities(0));
			arrangeEntityLists(tpcModel_.getNotSelectedEntities(), tpcModel_.getSelectedEntities());
			tpcModel_.setNotSelectedValues(DBUtil.readValues(0));
			arrangeValueLists(tpcModel_.getNotSelectedValues(), tpcModel_.getSelectedValues());
			//updateLists();
			tpcModel_.setSelectedTab("tab4");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
	}

	private void updateValueGroupSpecificData() throws Exception {
		if (tpcModel_ != null) {
			if (tpcModel_.getCurrentValueGroupForAdminID() == 0) {
				tpcModel_.setSelectedValuesForValueGroup(new ArrayList<ValueRecord>());
			} else {
				tpcModel_.setSelectedValuesForValueGroup(DBUtil.readValuesForValueGroup(tpcModel_.getCurrentValueGroupForAdminID()));
				tpcModel_.setCurrentValueGroupForAdminName(tpcModel_.getValueGroupRecordForId(tpcModel_.getCurrentValueGroupForAdminID()).getNameE());
				tpcModel_.setCurrentValueGroupForAdminNameD(tpcModel_.getValueGroupRecordForId(tpcModel_.getCurrentValueGroupForAdminID()).getNameD());
			}
			tpcModel_.setNotSelectedValuesForValueGroup(DBUtil.readValues(0));
			arrangeValueLists(tpcModel_.getNotSelectedValuesForValueGroup(), tpcModel_.getSelectedValuesForValueGroup());
			//updateLists();
			tpcModel_.setSelectedTab("tab3");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
	}
	
	private void updateEntityData() throws Exception {
		if (tpcModel_ != null) {
			if (tpcModel_.getCurrentEntityForAdminID() == 0) {
				tpcModel_.setCurrentEntityForAdminParameter("");
				tpcModel_.setCurrentEntityForAdminParameterD("");
				tpcModel_.setCurrentEntityForAdminPO_id("");
				tpcModel_.setCurrentEntityForAdminDefinition("");
			} else {
				EntityRecord currentEntity = tpcModel_
						.getEntityRecordForAdminById(tpcModel_.getCurrentEntityForAdminID());
				tpcModel_.setCurrentEntityForAdminParameter(currentEntity.getParameter());
				tpcModel_.setCurrentEntityForAdminParameterD(currentEntity.getParameterD());
				tpcModel_.setCurrentEntityForAdminPO_id(currentEntity.getpO_id());
				tpcModel_.setCurrentEntityForAdminDefinition(currentEntity.getDefinition());
			}
			tpcModel_.setSelectedTab("tab1");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
	}

	private void updateAttributeAndValueData() throws Exception {
		if (tpcModel_ != null) {
			if (tpcModel_.getCurrentValueForAdminID() == 0) {
				tpcModel_.setCurrentValueForAdminAttribut_e("");
				tpcModel_.setCurrentValueForAdminAttribut_d("");
				tpcModel_.setCurrentValueForAdminValue("");
				tpcModel_.setCurrentValueForAdminValueD("");
				tpcModel_.setCurrentValueForAdminValueGroup(0);
				tpcModel_.setCurrentDataTypeForAdmin(5);
			} else {
				ValueRecord currentValueRecord = tpcModel_
						.getValueRecordForAdminById(tpcModel_.getCurrentValueForAdminID());
				tpcModel_.setCurrentValueForAdminAttribut_e(currentValueRecord.getAttribut_e());
				tpcModel_.setCurrentValueForAdminAttribut_d(currentValueRecord.getAttribut_d());
				tpcModel_.setCurrentValueForAdminValue(currentValueRecord.getValue());
				tpcModel_.setCurrentValueForAdminValueD(currentValueRecord.getValue_d());
				tpcModel_.setCurrentValueForAdminValueGroup(currentValueRecord.getValue_group_id());
				tpcModel_.setCurrentDataTypeForAdmin(currentValueRecord.getDataType());

				if (currentValueRecord.getImageFileName() != null
						&& !currentValueRecord.getImageFileName().equals("")) {
					this.copyImageForDisplay(currentValueRecord.getImageFileName());
					String imagePath = TPCProperties.getInstance().getProperty("imageDisplayDirectory") + File.separator
							+ tpcModel_.getUserLogin() + File.separator + currentValueRecord.getImageFileName();
					imagePath = FileUtilities.customizeSeparatorsForURL(imagePath);
					tpcModel_.setCurrentImageFileForDisplay(imagePath);
					tpcModel_.setCurrentImageCopyright(currentValueRecord.getImageCopyright());
				} else {
					tpcModel_.setCurrentImageFileForDisplay("");
					tpcModel_.setCurrentImageCopyright("");
				}
			}
			tpcModel_.setSelectedTab("tab2");
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
	}

	private String backToMainPage() throws Exception {
		clearAdministrationData();
		tpcModel_.setEntities(DBUtil.readEntities(tpcModel_.getCurrentProjectID()));
		tpcModel_.setValues(DBUtil.readValues(tpcModel_.getCurrentProjectID()));
		return "backToMainPage";
	}

	private String uploadImage() throws Exception {
		return "uploadImage";
	}

	private String wantsToSaveChanges() throws Exception {
		if (tpcModel_ != null) {
			if (tpcModel_.getCurrentImageFileForDisplay().equals("")) {
				return "uploadImage";
			} else {
				tpcModel_.setAboutToSaveChanges(true);
			}
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return INPUT;
	}

	private String cancelSaveChanges() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.setAboutToSaveChanges(false);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return INPUT;
	}

	private void clearAdministrationData() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.clearAdministrationData();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
	}

//	private void updateLists() throws Exception {
//		if (tpcModel_ != null) {
//
//		} else {
//			throw new Exception("Internal serious implementation error: No TPCModel instance.");
//		}
//	}

	private void doSaveProject() throws Exception {
		if (tpcModel_ != null) {
			DBUtil.insertProject(tpcModel_);
			tpcModel_.setProjects(DBUtil.readProjects());
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
	}
	
	private void doSaveValueGroup() throws Exception {
		if (tpcModel_ != null) {
			DBUtil.insertValueGroup(tpcModel_);
			tpcModel_.setValueGroups(DBUtil.readValueGroups());
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
	}
	
	private void doUpdateProject() throws Exception {
		DBUtil.updateProject(tpcModel_);
		updateProjectSpecificData();
	}

	private void doUpdateValueGroup() throws Exception {
		DBUtil.updateValueGroup(tpcModel_);
		updateValueGroupSpecificData();
	}
	
	private void doUpdateEntity() throws Exception {
		DBUtil.updateEntitiy(tpcModel_);
		tpcModel_.setEntitiesForAdmin(DBUtil.readEntities(0));
		updateEntityData();
	}

	private void doUpdateAttributeAndValue() throws Exception {
		DBUtil.updateAttributeAndValue(tpcModel_);
		tpcModel_.setValuesForAdmin(DBUtil.readValues(0));
		updateAttributeAndValueData();
	}

	private void copyImageForDisplay(String imageName) throws Exception {
		String imageSourcePath = TPCProperties.getInstance().getProperty("imageDirectoryPath") + File.separator
				+ imageName;
		File imageSource = new File(imageSourcePath);
		if (!imageSource.exists()) {
			addActionError("Image file: " + imageSourcePath + " does not exist!");
			return;
		}
		String imageDisplayDirPath = TPCProperties.getInstance().getProperty("webApplicationParentPath")
				+ File.separator + httpServletRequest_.getContextPath() + File.separator
				+ TPCProperties.getInstance().getProperty("imageDisplayDirectory") + File.separator
				+ tpcModel_.getUserLogin();
		File imageDisplayDir = new File(imageDisplayDirPath);
		if (imageDisplayDir.exists()) {
			FileUtilities.removeDirRecursively(imageDisplayDirPath);
		}
		imageDisplayDir.mkdirs();
		String imageDisplayPath = imageDisplayDirPath + File.separator + imageName;
		FileUtilities.copyFile(imageSourcePath, imageDisplayPath);
	}

	private void doSaveEntity() throws Exception {
		DBUtil.insertEntity(tpcModel_);
		tpcModel_.setEntitiesForAdmin(DBUtil.readEntities(0));
	}

	private void doSaveAttributeAndValue() throws Exception {
		DBUtil.insertAttributeAndValue(tpcModel_);
		tpcModel_.setValuesForAdmin(DBUtil.readValues(0));
	}

	private void arrangeEntityLists(ArrayList<EntityRecord> allEntities, ArrayList<EntityRecord> selectedEntities) {
		if (allEntities != null && selectedEntities != null) {
			for (EntityRecord currentER : selectedEntities) {
				allEntities.remove(currentER);
			}
		}
	}

	private void arrangeValueLists(ArrayList<ValueRecord> allValues, ArrayList<ValueRecord> selectedValues) {
		if (allValues != null && selectedValues != null) {
			for (ValueRecord currentV : selectedValues) {
				allValues.remove(currentV);
			}
		}
	}

	private void reinit() {
		tpcModel_.setSelectedEntityIDs(new ArrayList<Integer>());
		tpcModel_.setSelectedEntities(new ArrayList<EntityRecord>());
		tpcModel_.setNotSelectedEntityIDs(new ArrayList<Integer>());
		tpcModel_.setNotSelectedEntities(DBUtil.readEntities(0));
		tpcModel_.setEntitiesForAdmin(DBUtil.readEntities(0));

		tpcModel_.setSelectedValueIDs(new ArrayList<Integer>());
		tpcModel_.setSelectedValues(new ArrayList<ValueRecord>());
		tpcModel_.setNotSelectedValueIDs(new ArrayList<Integer>());
		tpcModel_.setNotSelectedValues(DBUtil.readValues(0));
		tpcModel_.setValuesForAdmin(DBUtil.readValues(0));
	}

	@Override
	public void validate() {
		if (buttonName_.equals("saveProject") || buttonName_.equals("updateProject") || buttonName_.equals("saveEntity") || buttonName_.equals("updateEntity")
				|| buttonName_.equals("saveAttributeAndValue") || buttonName_.equals("updateAttributeAndValue") || buttonName_.equals("loadImageFile")) {
			if (tpcModel_ == null) {
				addActionError("Internal serious implementation error: no TPCModel instance.");
			}

			if (tpcModel_.getNewProject() == true && tpcModel_.getCurrentProjectForAdminName().equals("")) {
				addFieldError("currentProjectForAdminName", getText("error.noProjectNameEntered"));
			}
			if (tpcModel_.getNewProject() == true
					&& tpcModel_.getProjectNames().contains(tpcModel_.getCurrentProjectForAdminName())) {
				addFieldError("currentProjectForAdminName", getText("error.duplicateProjectName"));
			}
			if (tpcModel_.getNewEntity() == true) {
				if (tpcModel_.getCurrentEntityForAdminParameter().equals("")) {
					addFieldError("currentEntityForAdminParameter", getText("error.noEntity"));
				}
				if (tpcModel_.getEntityNamesForAdmin().contains(tpcModel_.getCurrentEntityForAdminParameter())) {
					addFieldError("currentEntityForAdminParameter", getText("error.duplicateEntityName"));
				}
				// currentEntityForAdminParameterD
				// currentEntityForAdminPO_id
				// currentEntityForAdminDefinition
			}
			if (tpcModel_.getNewAttributeAndValue() == true) {
				if (tpcModel_.getCurrentValueForAdminAttribut_e().equals("")) {
					addFieldError("currentValueForAdminAttribut_e", getText("error.noAttribute_e"));
				}

				if (tpcModel_.getCurrentValueForAdminValue().equals("")) {
					addFieldError("currentValueForAdminValue", getText("error.noValue_e"));
				}

				ValueRecord validatingRecord = new ValueRecord();
				validatingRecord.setAttribut_e(tpcModel_.getCurrentValueForAdminAttribut_e());
				validatingRecord.setAttribut_d(tpcModel_.getCurrentValueForAdminAttribut_d());
				validatingRecord.setValue(tpcModel_.getCurrentValueForAdminValue());
				validatingRecord.setValue_d(tpcModel_.getCurrentValueForAdminValueD());

				if (tpcModel_.getValuesForAdmin().contains(validatingRecord)) {
					addFieldError("currentAttributeAndValueForAdmin", getText("error.duplicateAttributeValuePair"));
				}
			}
		}
	}

	public void setButtonName(String buttonName) {
		buttonName_ = buttonName;
	}

	@Override
	public void setServletRequest(HttpServletRequest httpServletRequest) {
		httpServletRequest_ = httpServletRequest;
	}

	private String buttonName_ = "";
	private HttpServletRequest httpServletRequest_;

}

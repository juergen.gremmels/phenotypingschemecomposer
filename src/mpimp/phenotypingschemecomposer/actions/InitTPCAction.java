/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.actions;

import java.io.File;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.util.ServletContextAware;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.interceptor.ScopedModelDriven;

import mpimp.phenotypingschemecomposer.db.DBUtil;
import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.db.TestProgramEntityRecord;
import mpimp.phenotypingschemecomposer.util.OrderNumberComparator;
import mpimp.phenotypingschemecomposer.util.TPCConstants;
import mpimp.phenotypingschemecomposer.util.TPCProperties;

public class InitTPCAction extends TPCAbstractAction implements
		ScopedModelDriven<TPCModel>, ModelDriven<TPCModel>, Preparable,
		ServletRequestAware, ServletContextAware {

	private static final long serialVersionUID = -7377119995987245715L;

	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}

	public void init() throws Exception {
		String propertiesPath = servletContext_.getRealPath("/")
				+ File.separator + "WEB-INF" + File.separator + "classes"
				+ File.separator + TPCConstants.getPropertiesfilename();
		TPCProperties.setPropertiesPath(propertiesPath);
		TPCProperties.reloadProperties();
		if (TPCProperties.getPropertiesLoaded_() == false) {
			throw new Exception("Unable to load properties from file.");
		}
		if (tpcModel_ == null) {
			tpcModel_ = new TPCModel();
		}
		tpcModel_.setEntities(DBUtil.readEntities(0));
		tpcModel_.setValues(DBUtil.readValues(0));
		tpcModel_.setProjects(DBUtil.readProjects());
		tpcModel_.setValueGroups(DBUtil.readValueGroups());
		tpcModel_.setTestObjectTypeRecords(DBUtil.readTestObjectTypes());
		tpcModel_.setDataTypes(DBUtil.readDataTypes());
		ArrayList<TestProgramEntityRecord> testprogramEntityRecords = new ArrayList<TestProgramEntityRecord>();
		testprogramEntityRecords.add(new TestProgramEntityRecord());
		tpcModel_.setTestProgramEntityRecords(testprogramEntityRecords);
		tpcModel_.setTestProgramRecords(DBUtil
				.readTestProgramRecords(tpcModel_));
		tpcModel_.setTpcComparator(new OrderNumberComparator(tpcModel_));

		if (tpcModel_.getOperator().getRoleName().equals("admin")) {
			tpcModel_.getOperator().setIsAdministrator(true);
		} else {
			tpcModel_.getOperator().setIsAdministrator(false);
		}
	}

	@Override
	public void prepare() throws Exception {
		init();
	}

	@Override
	public void setServletRequest(HttpServletRequest httpServletRequest) {
		httpServletRequest_ = httpServletRequest;
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		servletContext_ = servletContext;
	}

	protected HttpServletRequest httpServletRequest_;
	protected ServletContext servletContext_;

}

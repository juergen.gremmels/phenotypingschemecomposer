/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.actions;

import mpimp.phenotypingschemecomposer.db.TPCModel;

import com.opensymphony.xwork2.ActionSupport;

public class TPCAbstractAction extends ActionSupport {

	private static final long serialVersionUID = -177194340546406904L;
	
	protected TPCModel tpcModel_;

	public TPCModel getModel() {
		return tpcModel_;
	}

	public String getScopeKey() {
		return scopeKey_;
	}

	public void setModel(TPCModel tpcModel) {
		tpcModel_ = tpcModel;
	}

	public void setScopeKey(String scopeKey) {
		scopeKey_ = scopeKey;
	}

	protected String scopeKey_;

}

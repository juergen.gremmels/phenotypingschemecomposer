/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import mpimp.phenotypingschemecomposer.db.DBUtil;
import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.util.Hashgenerator;
import mpimp.phenotypingschemecomposer.util.TPCConstants;
import mpimp.phenotypingschemecomposer.util.TPCProperties;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.util.ServletContextAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.interceptor.ScopedModelDriven;

public class TPCLoginAction extends TPCAbstractAction implements
		ScopedModelDriven<TPCModel>, ModelDriven<TPCModel>,
		ServletRequestAware, ServletContextAware, Preparable {

	private static final long serialVersionUID = 7209015890529588153L;

	@Override
	public String execute() throws Exception {
		if (buttonName_.equals("login")) {
			return login();
		}
		return INPUT;
	}

	@Override
	public void prepare() throws Exception {
		init();
	}

	private String login() throws Exception {
		if (authenticateUser()) {
			return SUCCESS;
		} else {
			return INPUT;
		}
	}

	private void init() throws Exception {
		String propertiesPath = servletContext_.getRealPath("/")
				+ File.separator + "WEB-INF" + File.separator + "classes"
				+ File.separator + TPCConstants.getPropertiesfilename();
		TPCProperties.setPropertiesPath(propertiesPath);
		TPCProperties.reloadProperties();
		String keyFilePath = servletContext_.getRealPath("/")
				+ File.separator + "WEB-INF" + File.separator + "classes"
				+ File.separator + TPCProperties.getInstance().getProperty("keyFileName");
		TPCProperties.getInstance().setProperty("keyFilePath", keyFilePath);
		if (TPCProperties.getPropertiesLoaded_() == false) {
			throw new Exception("Unable to load properties from file.");
		}
		tpcModel_ = new TPCModel();
		ActionContext.getContext().setLocale(new Locale("de", "DE"));
		tpcModel_.setCurrentLocale(ActionContext.getContext().getLocale());
		Map<String, String> locales = new HashMap<String, String>();
		locales.put("en_US", "English");
		locales.put("de_DE", "Deutsch");
		tpcModel_.setLocales(locales);
		Map<String, String> exportFormats = new HashMap<String, String>();
		exportFormats.put("xml", "xml");
		exportFormats.put("zip", "zip");
		tpcModel_.setExportFormats(exportFormats);
	}

	private Boolean authenticateUser() throws Exception {
		Boolean userPresent = false;
		Boolean passwordPresent = false;
		/* Check that fields are not empty */
		if (tpcModel_.getPasswd().length() == 0) {
			addFieldError("Password", getText("error.passwordRequired"));
		} else {
			userPresent = true;
		}
		if (tpcModel_.getUserLogin().length() == 0) {
			addFieldError("Username", getText("error.loginRequired"));
		} else {
			passwordPresent = true;
		}
		if (!userPresent || !passwordPresent) {
			// login data not complete
			return false;
		}
		if (!DBUtil.readUserData(tpcModel_)) {
			ArrayList<Object> args = new ArrayList<Object>();
			args.add(tpcModel_.getUserLogin());
			String message = getText("error.noUserWithLogin", args);
			addFieldError("Username", message);
			return false;
		}
		String hashedPassword = Hashgenerator.sha1(tpcModel_.getPasswd(),
				TPCProperties.getInstance().getProperty("secureSalt"));
		if (tpcModel_.getOperator().getRoleName().equals("guest")) {
			addFieldError("Username", "No access for users with guest status!");
			return false;
		}
		else if (hashedPassword.equals(tpcModel_.getOperator().getPassword())
				&& tpcModel_.getOperator().getAllowLogin() == true) {
			tpcModel_.setUserAuthenticated(true);
			return true;
		} else {
			addFieldError("Username", getText("error.noValidLoginData"));
			addFieldError("Password", getText("error.noValidLoginData"));
			tpcModel_.setUserAuthenticated(false);
			return false;
		}
	}

	@Override
	public void setServletRequest(HttpServletRequest httpServletRequest) {
		httpServletRequest_ = httpServletRequest;
	}

	@Override
	public void setServletContext(ServletContext servletContext) {
		servletContext_ = servletContext;
	}

	public void setButtonName(String buttonName) {
		buttonName_ = buttonName;
	}

	protected HttpServletRequest httpServletRequest_;
	protected ServletContext servletContext_;
	private String buttonName_ = "";

}

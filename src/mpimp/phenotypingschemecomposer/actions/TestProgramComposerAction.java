/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.actions;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import mpimp.phenotypingschemecomposer.db.DBUtil;
import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.db.TestProgramEntityRecord;
import mpimp.phenotypingschemecomposer.util.AttributeAndValueComparator;
import mpimp.phenotypingschemecomposer.util.EntityComparator;
import mpimp.phenotypingschemecomposer.util.FileDownloadHelper;
import mpimp.phenotypingschemecomposer.util.OrderNumberComparator;
import mpimp.phenotypingschemecomposer.util.TPCProperties;
import mpimp.phenotypingschemecomposer.util.XMLWriter;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.interceptor.ScopedModelDriven;

public class TestProgramComposerAction extends TPCAbstractAction
		implements ScopedModelDriven<TPCModel>, ServletRequestAware {

	private static final long serialVersionUID = -5541878638099919471L;

	@Override
	public String execute() throws Exception {
		if (buttonName_.equals("logout")) {
			buttonName_ = "";
			return logout();
		} else if (buttonName_.equals("goToAdministrationPage")) {
			buttonName_ = "";
			return goToAdministrationPage();
		} else if (buttonName_.equals("changeLanguage")) {
			buttonName_ = "";
			return changeLanguage();
		} else if (buttonName_.equals("compareEntity")) {
			buttonName_ = "";
			return compareEntity();
		} else if (buttonName_.equals("compareAttributeAndValue")) {
			buttonName_ = "";
			return compareAttributeAndValue();
		} else if (buttonName_.equals("compareOrderNumber")) {
			buttonName_ = "";
			return compareOrderNumber();
		} else if (buttonName_.equals("removeEntityValuePair")) {
			buttonName_ = "";
			return removeEntityValuePair();
		} else if (buttonName_.equals("addEntityValuePair")) {
			buttonName_ = "";
			return addEntityValuePair();
		} else if (buttonName_.equals("newTestProgram")) {
			buttonName_ = "";
			return newTestProgram();
		} else if (buttonName_.equals("cloneCurrentTestProgram")) {
			buttonName_ = "";
			return cloneCurrentTestProgram();
		} else if (buttonName_.equals("cancel")) {
			buttonName_ = "";
			return cancel();
		} else if (buttonName_.equals("exportTestProgram")) {
			buttonName_ = "";
			return exportTestProgram();
		} else if (buttonName_.equals("saveTestProgram")) {
			buttonName_ = "";
			return saveTestProgram();
		} else if (buttonName_.equals("updateTestProgram")) {
			buttonName_ = "";
			return updateTestProgram();
		} else if (buttonName_.equals("exportImages")) {
			buttonName_ = "";
			return exportImages();
		} else if (buttonName_.equals("importTestProgram")) {
			buttonName_ = "";
			return importTestProgram();
		} else if (buttonName_.equals("changePersonalData")) {
			buttonName_ = "";
			return changePersonalData();
		} else if (buttonName_.equals("backToMainPage")) {
			buttonName_ = "";
			return backToMainPage();
		} else {
			if (tpcModel_.getSubmitMethod().equals("testProgramChanged")) {
				return readCurrentTestProgramDetails();
			} else if (tpcModel_.getSubmitMethod().equals("projectChanged")) {
				return projectChanged();
			}
			return INPUT;
		}

	}

	public void setButtonName(String buttonName) {
		buttonName_ = buttonName;
	}

	private String projectChanged() throws Exception {
		tpcModel_.setEntities(DBUtil.readEntities(tpcModel_.getCurrentProjectID()));
		tpcModel_.setValues(DBUtil.readValues(tpcModel_.getCurrentProjectID()));
		return SUCCESS;
	}

	private String changeLanguage() {
		tpcModel_.setCurrentLocale(ActionContext.getContext().getLocale());
		return SUCCESS;
	}

	private String logout() {
		return "logout";
	}

	private String changePersonalData() {
		return "changePersonalData";
	}

	private String goToAdministrationPage() {
		if (tpcModel_.getOperator().getIsAdministrator() == true) {
			return "goToAdministrationPage";
		} else {
			return INPUT;
		}
	}

	private String backToMainPage() {
		if (tpcModel_ != null) {
			resetTestProgramData();
			tpcModel_.setNewTestProgram(false);
			tpcModel_.setTestProgramChanged(false);
		}
		return "backToMainPage";
	}

	public ServletRequest getServletRequest() {
		return httpServletRequest_;
	}

	public void setServletRequest(HttpServletRequest httpServletRequest) {
		httpServletRequest_ = httpServletRequest;
	}

	public String getDownloadURL() {
		return downloadURL_;
	}

	public void setDownloadURL(String downloadURL) {
		downloadURL_ = downloadURL;
	}

	public Integer getAttachmentId() {
		return attachmentId_;
	}

	public void setAttachmentId(Integer attachmentId) {
		attachmentId_ = attachmentId;
	}

	private String compareEntity() throws Exception {
		if (tpcModel_ != null) {
			Boolean sortAscending = tpcModel_.getCompareEntityAscending();
			tpcModel_.setTpcComparator(new EntityComparator(tpcModel_, sortAscending));
			Collections.sort(tpcModel_.getTestProgramEntityRecords(), new EntityComparator(tpcModel_, sortAscending));
			tpcModel_.setCompareEntityAscending(!sortAscending);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String compareAttributeAndValue() throws Exception {
		if (tpcModel_ != null) {
			Boolean sortAscending = tpcModel_.getCompareAttributeAndValueAscending();
			tpcModel_.setTpcComparator(new AttributeAndValueComparator(tpcModel_, sortAscending));
			Collections.sort(tpcModel_.getTestProgramEntityRecords(),
					new AttributeAndValueComparator(tpcModel_, sortAscending));
			tpcModel_.setCompareAttributeAndValueAscending(!sortAscending);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String compareOrderNumber() throws Exception {
		if (tpcModel_ != null) {
			Boolean sortAscending = tpcModel_.getCompareOrderNumberAscending();
			tpcModel_.setTpcComparator(new OrderNumberComparator(tpcModel_, sortAscending));
			Collections.sort(tpcModel_.getTestProgramEntityRecords(),
					new OrderNumberComparator(tpcModel_, sortAscending));
			tpcModel_.setCompareOrderNumberAscending(!sortAscending);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String addEntityValuePair() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.getTestProgramEntityRecords().add(new TestProgramEntityRecord());
			tpcModel_.setTestProgramChanged(true);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String removeEntityValuePair() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.getTestProgramEntityRecords().remove(entityValueIndex_.intValue());
			tpcModel_.setTestProgramChanged(true);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String readCurrentTestProgramDetails() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.setCurrentProjectID(0);
			reloadEntitiesAndValues();
			DBUtil.readTestProgramDetails(tpcModel_);
			if (tpcModel_.getCurrentTestProgramID() == 0) {
				tpcModel_.setTestProgramLoaded(false);
			} else {
				tpcModel_.setTestProgramLoaded(true);
			}
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String newTestProgram() throws Exception {
		if (tpcModel_ != null) {
			resetTestProgramData();
			tpcModel_.setNewTestProgram(true);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String cloneCurrentTestProgram() throws Exception {
		if (tpcModel_ != null) {
			tpcModel_.setNewTestProgram(true);
			String clonedName = tpcModel_.getCurrentTestProgramName() + "_clone";
			tpcModel_.setCurrentTestProgramName(clonedName);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String cancel() {
		if (tpcModel_ != null) {
			resetTestProgramData();
			tpcModel_.setNewTestProgram(false);
			tpcModel_.setTestProgramChanged(false);
		}
		return INPUT;
	}

	private String saveTestProgram() throws Exception {
		if (tpcModel_ != null) {
			DBUtil.saveTestProgram(tpcModel_);
			tpcModel_.setNewTestProgram(false);
			tpcModel_.setTestProgramChanged(false);
			tpcModel_.setTestProgramLoaded(true);
			tpcModel_.setTestProgramRecords(DBUtil.readTestProgramRecords(tpcModel_));
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String updateTestProgram() throws Exception {
		if (tpcModel_ != null) {
			DBUtil.updateTestProgram(tpcModel_);
			tpcModel_.setTestProgramChanged(false);
			tpcModel_.setTestProgramRecords(DBUtil.readTestProgramRecords(tpcModel_));
			DBUtil.readTestProgramDetails(tpcModel_);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return SUCCESS;
	}

	private String exportTestProgram() throws Exception {
		if (tpcModel_ != null) {
			performExportTestProgram();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return "downloadWithBlancPage";
	}

	private String exportImages() throws Exception {
		if (tpcModel_ != null) {
			if (!performExportImagePackage()) {
				return INPUT;
			}
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return "downloadSamePage";
	}

	private String importTestProgram() throws Exception {
		if (tpcModel_ != null) {
			resetTestProgramData();
			tpcModel_.setNewTestProgram(true);
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return "import";
	}

	@Override
	public void validate() {
		if (buttonName_.equals("saveTestProgram") || buttonName_.equals("updateTestProgram")
				|| buttonName_.equals("exportTestProgram")) {
			if (tpcModel_ == null) {
				addActionError("Internal serious implementation error: no TPCModel instance.");
			}

			if (tpcModel_.getNewTestProgram() == false && tpcModel_.getCurrentTestProgramID().equals(0)) {
				addFieldError("currentTestProgramID", getText("error.noTestProgramSelected"));
			}

			if (tpcModel_.getNewTestProgram() == true && tpcModel_.getCurrentTestProgramName().equals("")) {
				addFieldError("currentTestProgramName", getText("error.noTestProgramEntered"));
			}

			if (tpcModel_.getNewTestProgram() == true) {
				String name = tpcModel_.getCurrentTestProgramName();
				if (name.contains("�") || name.contains("�") || name.contains("�")) {
					addFieldError("currentTestProgramName", getText("error.avoidGermanUmlauts"));
				}
			}

			if (tpcModel_.getCurrentTestProgramMessobjekt().equals("")) {
				addFieldError("currentTestProgramMessobjekt", getText("error.noDeviceUnderTest"));
			}

			if ((tpcModel_.getNewTestProgram() == true && tpcModel_.getCurrentTestObjectType() == 0)
					|| (tpcModel_.getNewTestProgram() == false
							&& tpcModel_.getCurrentTestObjectTypeRecord().getName().equals(""))
					|| (tpcModel_.getNewTestProgram() == false
							&& tpcModel_.getCurrentTestObjectTypeRecord().getNameD().equals(""))) {
				addFieldError("currentTestObjectType", getText("error.noTestObjectType"));
			}

			if (tpcModel_.getCurrentTestProgramVersion().equals("")) {
				addFieldError("currentTestProgramVersion", getText("error.noVersion"));
			}

			if (tpcModel_.getNewTestProgram() == true
					&& tpcModel_.getTestProgramNames().contains(tpcModel_.getCurrentTestProgramName())) {
				addFieldError("currentTestProgramName", getText("error.duplicateTestProgramName"));
			}

			validateEntityValuePairs();
		}
	}

	private boolean validateEntityValuePairs() {
		List<TestProgramEntityRecord> tpeRecords = tpcModel_.getTestProgramEntityRecords();
		for (TestProgramEntityRecord currentTPERecord : tpeRecords) {
			if (currentTPERecord.getEntityID() == 0) {
				addFieldError("entity", getText("error.noEntitySelected"));
			}
			if (currentTPERecord.getValueID() == null) {
				addFieldError("value", getText("error.noValueSelected"));
			}
		}
		return true;
	}

	private boolean performExportImagePackage() throws Exception {
		// this will be the absolute path in the file system
		String pathToDownloadDirectory = createDownloadTempDir();
		if (FileDownloadHelper.createImagePackage(pathToDownloadDirectory, tpcModel_)) {
			// this will be the path relative to the web application directory
			String downloadPath = File.separator + TPCProperties.getInstance().getProperty("downloadTempDirName")
					+ File.separator + tpcModel_.getUserLogin();
			downloadPath = FileDownloadHelper.createImagePackagePath(tpcModel_, downloadPath);
			downloadURL_ = createDownloadUrl(downloadPath);
			return true;
		} else {
			return false;
		}
	}

	private void performExportTestProgram() throws Exception {
		// this will be the absolute path in the file system
		String pathToDownloadDirectory = createDownloadTempDir();
		XMLWriter.writeXML(tpcModel_, pathToDownloadDirectory);
		if (tpcModel_.getCurrentExportFormat().equals("zip")) {
			FileDownloadHelper.zipFileForDownload(pathToDownloadDirectory, tpcModel_);
		}
		// this will be the path relative to the web application directory
		String downloadPath = File.separator + TPCProperties.getInstance().getProperty("downloadTempDirName")
				+ File.separator + tpcModel_.getUserLogin();
		if (tpcModel_.getCurrentExportFormat().equals("zip")) {
			downloadPath = FileDownloadHelper.createZIPFilePath(tpcModel_, downloadPath);
		} else {
			downloadPath = FileDownloadHelper.createXMLFilePath(tpcModel_, downloadPath);
		}
		downloadURL_ = createDownloadUrl(downloadPath);
	}

	private String createDownloadTempDir() throws Exception {
		String webApplicationParentPath = TPCProperties.getInstance().getProperty("webApplicationParentPath");
		String webApplicationName = httpServletRequest_.getContextPath();
//		if (webApplicationName.startsWith("/")) {
//			webApplicationName = webApplicationName.replace("/", "");
//		}
		String downloadTempDirName = TPCProperties.getInstance().getProperty("downloadTempDirName") + File.separator
				+ tpcModel_.getUserLogin() + File.separator;
		String tempDirPath = webApplicationParentPath + File.separator + webApplicationName + File.separator
				+ downloadTempDirName;
		FileDownloadHelper.createDownloadTempDir(tempDirPath);
		return tempDirPath;
	}

	private String createDownloadUrl(String filePath) throws Exception {
		String downloadUrl = "";
		URL url = null;
		String host = httpServletRequest_.getLocalName();
		String webRoot = httpServletRequest_.getContextPath();
		String protocol = httpServletRequest_.getScheme();
		int serverPort = httpServletRequest_.getServerPort();
		if (!filePath.startsWith("/")) {
			filePath = "/" + filePath;
		}
		filePath = webRoot + filePath;
		try {
			if (serverPort == 0) {
				url = new URL(protocol, host, filePath);
			} else {
				url = new URL(protocol, host, serverPort, filePath);
			}
		} catch (MalformedURLException me) {
			throw new Exception(me.getMessage());
		}
		if (url != null) {
			downloadUrl = url.toString();
		}
		return downloadUrl;
	}

	private void resetTestProgramData() {
		tpcModel_.resetTestProgramData();
		reloadEntitiesAndValues();
	}

	private void reloadEntitiesAndValues() {
		tpcModel_.setEntities(DBUtil.readEntities(tpcModel_.getCurrentProjectID()));
		tpcModel_.setValues(DBUtil.readValues(tpcModel_.getCurrentProjectID()));
	}

	public Integer getEntityValueIndex() {
		return entityValueIndex_;
	}

	public void setEntityValueIndex(Integer entityValueIndex) {
		entityValueIndex_ = entityValueIndex;
	}

	protected HttpServletRequest httpServletRequest_;
	protected String downloadURL_;
	protected Integer attachmentId_;
	private Integer entityValueIndex_;
	private String buttonName_ = "";

}

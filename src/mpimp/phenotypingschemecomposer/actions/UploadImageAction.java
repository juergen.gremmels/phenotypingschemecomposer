/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.actions;

import java.io.File;
import java.util.ArrayList;

import mpimp.phenotypingschemecomposer.db.DBUtil;
import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.db.ValueRecord;
import mpimp.phenotypingschemecomposer.util.FileUtilities;
import mpimp.phenotypingschemecomposer.util.TPCProperties;

import com.opensymphony.xwork2.interceptor.ScopedModelDriven;

public class UploadImageAction extends TPCAbstractAction implements ScopedModelDriven<TPCModel> {

	private static final long serialVersionUID = -1074162541929548289L;

	@Override
	public String execute() throws Exception {
		if (buttonName_.equals("backToAdministrationPage")) {
			buttonName_ = "";
			return  backToAdministrationPage();
		} else if (buttonName_.equals("loadImageFile")) {
			buttonName_ = "";
			return loadImageFile();
		} else {
			return INPUT;
		}
	}

	private String backToAdministrationPage() throws Exception {
		return "backToAdministrationPage";
	}

	private String loadImageFile() throws Exception {
		if (tpcModel_ != null) {
			doLoadImageFile();
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		tpcModel_.setSubmitMethod("readAndUpdateAttributeAndValueData");
		return "backToAdministrationPage";
	}

	@Override
	public void validate() {
		if (buttonName_.equals("loadImageFile")) {
			if (tpcModel_ == null) {
				addActionError("Internal serious implementation error: no TPCModel instance.");
			}
			if (tpcModel_.getCurrentImageFile() != null && !tpcModel_.getCurrentImageFile().equals("")) {
				checkImage();
			}
			if (tpcModel_.getCurrentImageCopyright() != null && tpcModel_.getCurrentImageCopyright().equals("")) {
				addFieldError("currentImageCopyright", getText("error.noCopyrightInformation"));
			}
		}
	}

	public void setButtonName(String buttonName) {
		buttonName_ = buttonName;
	}

	private void doLoadImageFile() throws Exception {
		ValueRecord currentValueRecord = tpcModel_.getValueRecordForAdminById(tpcModel_.getCurrentValueForAdminID());
		if (currentValueRecord.getImageFileName() != null && !currentValueRecord.getImageFileName().equals("")) {
			deleteOldImage();
			currentValueRecord.setImageFileName("");
			currentValueRecord.setImageCopyright("");
		}
		if (tpcModel_.getCurrentImageFile() != null && !tpcModel_.getCurrentImageFile().equals("")) {
			String imageDestinationFilePath = TPCProperties.getInstance().getProperty("imageDirectoryPath")
					+ File.separator + FileUtilities.createImageFileName(tpcModel_);
			FileUtilities.copyFile(tpcModel_.getCurrentImageFile(), imageDestinationFilePath);
			DBUtil.insertImageData(tpcModel_);
		}
	}

	private void deleteOldImage() throws Exception {
		ValueRecord currentValueRecord = tpcModel_.getValueRecordForAdminById(tpcModel_.getCurrentValueForAdminID());
		String imageSourcePath = TPCProperties.getInstance().getProperty("imageDirectoryPath") + File.separator
				+ currentValueRecord.getImageFileName();
		File oldImage = new File(imageSourcePath);
		if (oldImage.delete()) {
			DBUtil.deleteImageData(tpcModel_);
		} else {
			String message = "Could not delete image!";
			throw new Exception(message);
		}
	}

	private void checkImage() {
		File imageToCheck = new File(tpcModel_.getCurrentImageFile());
		long size = imageToCheck.length();
		size = size / 1000;
		if (size > Long.parseLong(TPCProperties.getInstance().getProperty("maxImageSizeInKb"))) {
			ArrayList<Object> arguments = new ArrayList<Object>();
			arguments.add(String.valueOf(size));
			arguments.add(TPCProperties.getInstance().getProperty("maxImageSizeInKb"));
			addFieldError("currentImageFile", getText("error.fileTooLarge", arguments));
			tpcModel_.setCurrentImageFile("");
		}
	}

	private String buttonName_ = "";
}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.actions;

import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.util.XMLReader;

import com.opensymphony.xwork2.interceptor.ScopedModelDriven;

public class ImportTestProgramAction extends TPCAbstractAction implements ScopedModelDriven<TPCModel> {

	private static final long serialVersionUID = 1932165684736593766L;

	@Override
	public String execute() throws Exception {
		if (buttonName_.equals("importTestProgram")) {
			buttonName_ = "";
			return importTestProgram();
		} else if (buttonName_.equals("backToMainPage")) {
			buttonName_ = "";
			return "backToMainPage";
		} else {
			return INPUT;
		}
	}

	public void setButtonName(String buttonName) {
		buttonName_ = buttonName;
	}

	private String importTestProgram() throws Exception {
		if (tpcModel_ != null) {
			doImportTestProgram();
			if (!tpcModel_.getXmlReadingErrors().equals("")) {
				addFieldError("sourceFile", tpcModel_.getXmlReadingErrors());
				return INPUT;
			}
		} else {
			throw new Exception("Internal serious implementation error: No TPCModel instance.");
		}
		return "backToMainPage";
	}

	private void doImportTestProgram() throws Exception {
		XMLReader.readTestProgramFile(tpcModel_);
	}

	@Override
	public void validate() {
		if (buttonName_.equals("importTestProgram")) {
			if (tpcModel_.getSourceFile() == null || tpcModel_.getSourceFile().equals("")) {
				addFieldError("sourceFile", getText("error.noFileSelected"));
			}
		}
	}

	private String buttonName_ = "";

}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.actions;

import java.util.ArrayList;

import com.opensymphony.xwork2.interceptor.ScopedModelDriven;

import mpimp.phenotypingschemecomposer.db.DBUtil;
import mpimp.phenotypingschemecomposer.db.DataTypeRecord;
import mpimp.phenotypingschemecomposer.db.EntityRecord;
import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.db.ValueRecord;
import mpimp.phenotypingschemecomposer.util.TPCProperties;

public class InitTPCAdministrationAction extends TPCAbstractAction implements ScopedModelDriven<TPCModel> {

	private static final long serialVersionUID = 5367686876486712048L;

	@Override
	public String execute() throws Exception {
		init();
		return SUCCESS;
	}

	private void init() {
		if (tpcModel_ != null) {
			tpcModel_.setSelectedEntityIDs(new ArrayList<Integer>());
			tpcModel_.setSelectedEntities(new ArrayList<EntityRecord>());
			tpcModel_.setNotSelectedEntityIDs(new ArrayList<Integer>());
			tpcModel_.setNotSelectedEntities(DBUtil.readEntities(0));
			tpcModel_.setEntitiesForAdmin(DBUtil.readEntities(0));

			tpcModel_.setSelectedValueIDs(new ArrayList<Integer>());
			tpcModel_.setSelectedValues(new ArrayList<ValueRecord>());
			tpcModel_.setNotSelectedValueIDs(new ArrayList<Integer>());
			tpcModel_.setNotSelectedValues(DBUtil.readValues(0));
			tpcModel_.setValuesForAdmin(DBUtil.readValues(0));
			
			tpcModel_.setSelectedValueIDsForValueGroup(new ArrayList<Integer>());
			tpcModel_.setSelectedValuesForValueGroup(new ArrayList<ValueRecord>());
			tpcModel_.setNotSelectedValueIDsForValueGroup(new ArrayList<Integer>());
			tpcModel_.setNotSelectedValuesForValueGroup(DBUtil.readValues(0));

			tpcModel_.setLinkToOntologyLookupService(
					TPCProperties.getInstance().getProperty("linkToOntologyLookupService"));
			
			tpcModel_.setDataTypes(DBUtil.readDataTypes());
			
		}
	}

}

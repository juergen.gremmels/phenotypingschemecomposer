/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import mpimp.phenotypingschemecomposer.db.hibernate.HibernateSessionFactory;
import mpimp.phenotypingschemecomposer.util.FileUtilities;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

public class DBUtil {

	@SuppressWarnings("unchecked")
	public static Boolean readUserData(TPCModel tpcModel) {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		// SQLQuery query = hibernateSession
		// .createSQLQuery(
		// "select op.id as operatorId, op.username as login, op.password, "
		// +
		// " op.name as firstName, op.lastname as lastName, op.allowlogin as allowLogin
		// "
		// + " from people op "
		// + " where op.username = :loginName")
		// .addScalar("operatorId", Hibernate.INTEGER)
		// .addScalar("login", Hibernate.STRING)
		// .addScalar("password", Hibernate.STRING)
		// .addScalar("firstName", Hibernate.STRING)
		// .addScalar("lastName", Hibernate.STRING)
		// .addScalar("allowLogin", Hibernate.BOOLEAN);

		SQLQuery query = hibernateSession
				.createSQLQuery("select op.id as operatorId, op.username as login, op.password, "
						+ " op.name as firstName, op.lastname as lastName, op.allowlogin as allowLogin, op.role as roleName"
						+ " from operator op " + " where op.username = :loginName")
				.addScalar("operatorId", Hibernate.INTEGER).addScalar("login", Hibernate.STRING)
				.addScalar("password", Hibernate.STRING).addScalar("firstName", Hibernate.STRING)
				.addScalar("lastName", Hibernate.STRING).addScalar("allowLogin", Hibernate.BOOLEAN)
				.addScalar("roleName", Hibernate.STRING);

		query.setString("loginName", tpcModel.getUserLogin());
		query.setResultTransformer(Transformers.aliasToBean(OperatorRecord.class));
		ArrayList<OperatorRecord> operatorRecords = new ArrayList<OperatorRecord>();
		operatorRecords = (ArrayList<OperatorRecord>) query.list();
		hibernateSession.getTransaction().commit();
		if (operatorRecords.size() == 1) {
			tpcModel.setOperator(operatorRecords.get(0));
			return true;
		} else {
			return false;
		}

		// Integer operatorId = tpcModel.getOperator().getOperatorId();
		//
		// hibernateSession = HibernateSessionFactory.getSessionFactory()
		// .getCurrentSession();
		// hibernateSession.beginTransaction();
		//
		// SQLQuery query2 = hibernateSession
		// .createSQLQuery(
		// "select ro.role_id as roleId, ro.name as roleName "
		// + " from role ro, people_role pr"
		// + " where pr.people_id = :operatorId and pr.role_id = ro.role_id")
		// .addScalar("roleId", Hibernate.INTEGER)
		// .addScalar("roleName", Hibernate.STRING);
		// query2.setInteger("operatorId", operatorId);
		// query2.setResultTransformer(Transformers.aliasToBean(RoleRecord.class));
		// ArrayList<RoleRecord> roleRecords = new ArrayList<RoleRecord>();
		// roleRecords = (ArrayList<RoleRecord>) query2.list();
		// hibernateSession.getTransaction().commit();
		// if (roleRecords.size() > 0) {
		// tpcModel.getOperator().setRoles(roleRecords);
		// return true;
		// } else {
		// return false;
		// }
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<EntityRecord> readEntities(Integer currentProjectId) {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query = null;

		if (currentProjectId == 0) {
			query = hibernateSession
					.createSQLQuery("select OrganID, Parameter, Parameter_dt as parameterD, PO_id as pO_id, definition "
							+ " from test_mpiscore_entities order by Parameter")
					.addScalar("organID", Hibernate.INTEGER).addScalar("parameter", Hibernate.STRING)
					.addScalar("parameterD", Hibernate.STRING).addScalar("pO_id", Hibernate.STRING)
					.addScalar("definition", Hibernate.STRING);
		} else {
			query = hibernateSession
					.createSQLQuery("select tme.OrganID, tme.Parameter, tme.Parameter_dt as parameterD, "
							+ " PO_id as pO_id, definition, project_entity_id as projectEntityID "
							+ " from test_mpiscore_entities tme, project_entity pe, project p "
							+ " where p.project_id = :projectID " + " and p.project_id = pe.project_id "
							+ " and pe.entity_id = tme.OrganID" + " order by tme.Parameter")
					.addScalar("organID", Hibernate.INTEGER).addScalar("parameter", Hibernate.STRING)
					.addScalar("parameterD", Hibernate.STRING).addScalar("pO_id", Hibernate.STRING)
					.addScalar("definition", Hibernate.STRING).addScalar("projectEntityID", Hibernate.INTEGER);
			;
			query.setInteger("projectID", currentProjectId);
		}

		query.setResultTransformer(Transformers.aliasToBean(EntityRecord.class));
		ArrayList<EntityRecord> entityRecords = new ArrayList<EntityRecord>();
		entityRecords = (ArrayList<EntityRecord>) query.list();
		hibernateSession.getTransaction().commit();

		return entityRecords;
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<ValueRecord> readValues(Integer currentProjectId) {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query = null;
		if (currentProjectId == 0) {
			query = hibernateSession.createSQLQuery(
					"select tmv.ValueID, tmv.Attribut_E, tmv.value, tmv.Attribut_D, tmv.Wert_D as value_d, "
							+ " tmv.format_pattern as formatPattern, tmv.data_type as dataType, dt.name as dataTypeName, vgv.value_group_id,"
							+ " i.file_name as imageFileName," 
							+ " i.copyright as imageCopyright"
							+ " from test_mpiscore_values tmv" 
							+ " inner join data_type dt on tmv.data_type = dt.id "
							+ " left join value_group_value vgv on tmv.ValueID = vgv.value_id"
							+ " left join image i on tmv.ValueID = i.value_id" 
							+ " order by Attribut_E");
		} else {
			query = hibernateSession.createSQLQuery(
					"select tmv.ValueID, tmv.Attribut_E, tmv.value, tmv.Attribut_D, tmv.Wert_D as value_d, "
							+ " tmv.format_pattern as formatPattern, tmv.data_type dataType, dt.name as dataTypeName, vgv.value_group_id, "
							+ " i.file_name as imageFileName," 
							+ " i.copyright as imageCopyright"
							+ " from test_mpiscore_values tmv"
							+ " inner join data_type dt on tmv.data_type = dt.id "
							+ " inner join project_value pe on pe.value_id = tmv.ValueID "
							+ " inner join project p on p.project_id = pe.project_id "
							+ " left join value_group_value vgv on tmv.ValueID = vgv.value_id "
							+ " left join image i on tmv.ValueID = i.value_id" 
							+ " where p.project_id = :projectID "
							+ " order by Attribut_E");
			query.setInteger("projectID", currentProjectId);
		}

		query.addScalar("valueID", Hibernate.INTEGER).addScalar("attribut_e", Hibernate.STRING)
				.addScalar("value", Hibernate.STRING).addScalar("attribut_d", Hibernate.STRING)
				.addScalar("value_d", Hibernate.STRING).addScalar("value_group_id", Hibernate.INTEGER)
				.addScalar("imageFileName", Hibernate.STRING).addScalar("imageCopyright", Hibernate.STRING)
				.addScalar("formatPattern", Hibernate.STRING).addScalar("dataType", Hibernate.INTEGER)
				.addScalar("dataTypeName", Hibernate.STRING);

		query.setResultTransformer(Transformers.aliasToBean(ValueRecord.class));
		ArrayList<ValueRecord> valueRecords = new ArrayList<ValueRecord>();
		valueRecords = (ArrayList<ValueRecord>) query.list();
		hibernateSession.getTransaction().commit();

		return valueRecords;
	}

	public static ArrayList<ValueRecord> readValuesForValueGroup(Integer valueGroupID) {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query = null;

		query = hibernateSession
				.createSQLQuery("select tmv.ValueID, tmv.Attribut_E, tmv.value, tmv.Attribut_D, tmv.Wert_D as value_d, "
						+ " tmv.format_pattern as formatPattern, tmv.data_type dataType, dt.name as dataTypeName, vgv.value_group_id, "
						+ " i.file_name as imageFileName," 
						+ " i.copyright as imageCopyright"
						+ " from test_mpiscore_values tmv"
						+ " inner join data_type dt on tmv.data_type = dt.id "
						+ " inner join value_group_value vgv on tmv.ValueID = vgv.value_id "
						+ " left join image i on tmv.ValueID = i.value_id"
						+ " where vgv.value_group_id = :valueGroupID " 
						+ " order by Attribut_E");
		query.setInteger("valueGroupID", valueGroupID);

		query.addScalar("valueID", Hibernate.INTEGER).addScalar("attribut_e", Hibernate.STRING)
				.addScalar("value", Hibernate.STRING).addScalar("attribut_d", Hibernate.STRING)
				.addScalar("value_d", Hibernate.STRING).addScalar("value_group_id", Hibernate.INTEGER)
				.addScalar("imageFileName", Hibernate.STRING).addScalar("imageCopyright", Hibernate.STRING)
				.addScalar("formatPattern", Hibernate.STRING).addScalar("dataType", Hibernate.INTEGER)
				.addScalar("dataTypeName", Hibernate.STRING);

		query.setResultTransformer(Transformers.aliasToBean(ValueRecord.class));
		ArrayList<ValueRecord> valueRecords = new ArrayList<ValueRecord>();
		valueRecords = (ArrayList<ValueRecord>) query.list();
		hibernateSession.getTransaction().commit();

		return valueRecords;
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<ProjectRecord> readProjects() {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();
		SQLQuery query = hibernateSession.createSQLQuery("select project_id, name from project")
				.addScalar("project_id", Hibernate.INTEGER).addScalar("name", Hibernate.STRING);
		query.setResultTransformer(Transformers.aliasToBean(ProjectRecord.class));
		ArrayList<ProjectRecord> projects = new ArrayList<ProjectRecord>();
		projects = (ArrayList<ProjectRecord>) query.list();
		hibernateSession.getTransaction().commit();
		return projects;
	}

	public static ArrayList<ValueGroupRecord> readValueGroups() {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();
		SQLQuery query = hibernateSession
				.createSQLQuery("select value_group_id as valueGroup_id, nameE, nameD from value_group")
				.addScalar("valueGroup_id", Hibernate.INTEGER).addScalar("nameE", Hibernate.STRING)
				.addScalar("nameD", Hibernate.STRING);
		query.setResultTransformer(Transformers.aliasToBean(ValueGroupRecord.class));
		ArrayList<ValueGroupRecord> valueGroups = new ArrayList<ValueGroupRecord>();
		valueGroups = (ArrayList<ValueGroupRecord>) query.list();
		hibernateSession.getTransaction().commit();
		return valueGroups;
	}

	@SuppressWarnings("unchecked")
	public static ArrayList<TestObjectTypeRecord> readTestObjectTypes() throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();
		SQLQuery query = hibernateSession
				.createSQLQuery("select test_object_type_id as testObjectType_id, test_object_name as name,"
						+ " test_object_name_D as nameD from test_object_type")
				.addScalar("testObjectType_id", Hibernate.INTEGER).addScalar("name", Hibernate.STRING)
				.addScalar("nameD", Hibernate.STRING);
		query.setResultTransformer(Transformers.aliasToBean(TestObjectTypeRecord.class));
		ArrayList<TestObjectTypeRecord> testObjectTypes = new ArrayList<TestObjectTypeRecord>();
		testObjectTypes = (ArrayList<TestObjectTypeRecord>) query.list();
		hibernateSession.getTransaction().commit();
		return testObjectTypes;
	}

	public static ArrayList<DataTypeRecord> readDataTypes() {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();
		SQLQuery query = hibernateSession
				.createSQLQuery("select id as dataTypeID, name as dataTypeName " + " from data_type")
				.addScalar("dataTypeID", Hibernate.INTEGER).addScalar("dataTypeName", Hibernate.STRING);
		query.setResultTransformer(Transformers.aliasToBean(DataTypeRecord.class));
		ArrayList<DataTypeRecord> testObjectTypes = new ArrayList<DataTypeRecord>();
		testObjectTypes = (ArrayList<DataTypeRecord>) query.list();
		hibernateSession.getTransaction().commit();
		return testObjectTypes;
	}

	public static ArrayList<TestProgramRecord> readTestProgramRecords(TPCModel tpcModel) {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query = hibernateSession
				.createSQLQuery("select Testprogramm_id, Testprogrammname from testprogramme tp"
						+ " inner join testprogramme_operator tpo on tp.Testprogramm_id=tpo.testprogramm "
						+ " where tpo.operator= :operator_id " + " order by Testprogrammname")
				.addScalar("testprogramm_id", Hibernate.INTEGER).addScalar("testprogrammname", Hibernate.STRING);
		query.setInteger("operator_id", tpcModel.getOperator().getOperatorId());

		query.setResultTransformer(Transformers.aliasToBean(TestProgramRecord.class));
		ArrayList<TestProgramRecord> testProgramRecords = new ArrayList<TestProgramRecord>();
		@SuppressWarnings("unchecked")
		ArrayList<TestProgramRecord> list = (ArrayList<TestProgramRecord>) query.list();
		testProgramRecords = list;
		hibernateSession.getTransaction().commit();
		return testProgramRecords;
	}

	@SuppressWarnings("unchecked")
	public static void readTestProgramDetails(TPCModel tpcModel) {
		if (tpcModel.getCurrentTestProgramID() != 0) {
			Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
			hibernateSession.beginTransaction();

			SQLQuery query1 = hibernateSession.createSQLQuery(
					"select tp.TestProgramm_id as testprogramm_id, " + " tp.Testprogrammname as testprogrammname,"
							+ " tp.Version as version," + " tp.Messobjekt as messobjekt,"
							+ " tp.Description as description, " + " tty.test_object_type_id as testObjectTypeId"
							+ " from testprogramme tp, test_object_type tty where TestProgramm_id = :test_program_id"
							+ " and tp.TestObjectType = tty.test_object_type_id " + " order by Testprogrammname");
			query1.setInteger("test_program_id", tpcModel.getCurrentTestProgramID());

			query1.addScalar("testprogramm_id", Hibernate.INTEGER).addScalar("testprogrammname", Hibernate.STRING)
					.addScalar("version", Hibernate.STRING).addScalar("messobjekt", Hibernate.STRING)
					.addScalar("description", Hibernate.STRING).addScalar("testObjectTypeId", Hibernate.INTEGER);

			query1.setResultTransformer(Transformers.aliasToBean(TestProgramDetails.class));

			ArrayList<TestProgramDetails> testProgramDetails = new ArrayList<TestProgramDetails>();
			testProgramDetails = (ArrayList<TestProgramDetails>) query1.list();

			if (testProgramDetails.size() == 1) {
				TestProgramDetails currentDetails = testProgramDetails.get(0);
				tpcModel.setCurrentTestProgramName(currentDetails.getTestprogrammname());
				tpcModel.setCurrentTestProgramVersion(currentDetails.getVersion());
				tpcModel.setCurrentTestProgramMessobjekt(currentDetails.getMessobjekt());
				tpcModel.setCurrentTestProgramDescription(currentDetails.getDescription());
				tpcModel.setCurrentTestObjectType(currentDetails.getTestObjectTypeId());
			}

			hibernateSession.getTransaction().commit();

			hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
			hibernateSession.beginTransaction();

			String queryString2 = "select e.OrganID as entityID, v.ValueID as valueID, te.OrderNumber as orderNumber,"
					+ " i.file_name as imageFileName" + " from testprogramm_entity te"
					+ " inner join test_mpiscore_entities e on e.OrganID = te.Entity"
					+ " inner join test_mpiscore_values v on v.ValueID = te.Value"
					+ " left join image i on i.value_id = v.ValueID" + " where te.testprogramm = :test_program_id"
					+ " order by orderNumber asc";

			SQLQuery query2 = hibernateSession.createSQLQuery(queryString2).addScalar("entityID", Hibernate.INTEGER)
					.addScalar("valueID", Hibernate.INTEGER).addScalar("orderNumber", Hibernate.INTEGER)
					.addScalar("imageFileName", Hibernate.STRING);
			query2.setInteger("test_program_id", tpcModel.getCurrentTestProgramID());

			query2.setResultTransformer(Transformers.aliasToBean(TestProgramEntityRecord.class));
			ArrayList<TestProgramEntityRecord> testProgramEntityRecords = new ArrayList<TestProgramEntityRecord>();
			testProgramEntityRecords = (ArrayList<TestProgramEntityRecord>) query2.list();
			tpcModel.setTestProgramEntityRecords(testProgramEntityRecords);

			hibernateSession.getTransaction().commit();
		} else {
			tpcModel.resetTestProgramData();
		}

	}

	@SuppressWarnings("unchecked")
	public static void saveTestProgram(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query1 = hibernateSession.createSQLQuery(
				"insert into testprogramme (Testprogrammname, Version, Messobjekt, Description, TestObjectType) "
						+ "values (:testprogrammname, :version, :messobjekt, :description, :testObjectType)");

		query1.setString("testprogrammname", tpcModel.getCurrentTestProgramName());
		query1.setString("version", tpcModel.getCurrentTestProgramVersion());
		query1.setString("messobjekt", tpcModel.getCurrentTestProgramMessobjekt());
		query1.setString("description", tpcModel.getCurrentTestProgramDescription());
		query1.setInteger("testObjectType", tpcModel.getCurrentTestObjectType());

		if (query1.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of testprogram " + tpcModel.getCurrentTestProgramName() + " failed.";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query2 = hibernateSession.createSQLQuery("select last_insert_id()");
		ArrayList<BigInteger> currentIds = new ArrayList<BigInteger>();
		currentIds = (ArrayList<BigInteger>) query2.list();
		if (currentIds.size() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of testprogram " + tpcModel.getCurrentTestProgramName()
					+ " failed. Fetching of last inserted id failed.";
			throw new Exception(message);
		}
		hibernateSession.getTransaction().commit();

		BigInteger currentId = currentIds.get(0);

		tpcModel.setCurrentTestProgramID(currentId.intValue());

		String queryString3 = "insert into testprogramm_entity (testprogramm, Entity, Value, OrderNumber) values ";

		ArrayList<TestProgramEntityRecord> testProgramEntityRecords = tpcModel.getTestProgramEntityRecords();
		Integer recordCount = testProgramEntityRecords.size();
		Integer count = 0;

		for (TestProgramEntityRecord currentRecord : testProgramEntityRecords) {
			count++;
			String valueString = "(:testprogramm_id, %s, %s, %s)";
			valueString = String.format(valueString, currentRecord.getEntityID(), currentRecord.getValueID(),
					currentRecord.getOrderNumber());
			queryString3 += valueString;
			if (count < recordCount) {
				queryString3 += ", ";
			}
		}

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();
		SQLQuery query3 = hibernateSession.createSQLQuery(queryString3);
		query3.setBigInteger("testprogramm_id", currentId);

		if (query3.executeUpdate() != recordCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of testprogram " + tpcModel.getCurrentTestProgramName()
					+ " failed. Writing of program details not possible.";
			throw new Exception(message);
		}
		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query4 = hibernateSession.createSQLQuery("insert into testprogramme_operator (testprogramm, operator)"
				+ " values (:testprogram_id, :operator_id)");
		Integer currentIdAsInteger = currentId.intValue();
		query4.setInteger("testprogram_id", currentIdAsInteger);
		query4.setInteger("operator_id", tpcModel.getOperator().getOperatorId());

		if (query4.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of testprogram " + tpcModel.getCurrentTestProgramName()
					+ " failed. Writing of user data not possible.";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	public static void updateTestProgram(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query1 = hibernateSession.createSQLQuery(
				"update testprogramme set Version = :version, Messobjekt = :messobjekt, Description = :description "
						+ " where testprogramme.testprogramm_id = :testprogramm_id");

		query1.setInteger("testprogramm_id", tpcModel.getCurrentTestProgramID());
		query1.setString("version", tpcModel.getCurrentTestProgramVersion());
		query1.setString("messobjekt", tpcModel.getCurrentTestProgramMessobjekt());
		query1.setString("description", tpcModel.getCurrentTestProgramDescription());

		if (query1.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Updating of testprogram " + tpcModel.getCurrentTestProgramName() + " failed.";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();

		hibernateSession.beginTransaction();

		SQLQuery queryCount = hibernateSession.createSQLQuery(
				"select count(testprogramm) from testprogramm_entity where testprogramm = :testprogramm_id");

		queryCount.setInteger("testprogramm_id", tpcModel.getCurrentTestProgramID());

		@SuppressWarnings("unchecked")
		List<BigInteger> entryCount = (List<BigInteger>) queryCount.list();

		if (entryCount.size() != 1) {
			throw new Exception("Error while trying to update testprogram " + tpcModel.getCurrentTestProgramName());
		}

		Integer entityValuePairCount = entryCount.get(0).intValue();

		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();

		hibernateSession.beginTransaction();

		SQLQuery query2 = hibernateSession.createSQLQuery(
				"delete from testprogramm_entity where testprogramm_entity.testprogramm = :testprogramm_id");

		query2.setInteger("testprogramm_id", tpcModel.getCurrentTestProgramID());

		if (query2.executeUpdate() != entityValuePairCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Deleting of obsolete records for testprogram " + tpcModel.getCurrentTestProgramName()
					+ " failed.";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();

		String queryString3 = "insert into testprogramm_entity (testprogramm, Entity, Value, OrderNumber) values ";

		ArrayList<TestProgramEntityRecord> testProgramEntityRecords = tpcModel.getTestProgramEntityRecords();
		Integer recordCount = testProgramEntityRecords.size();
		Integer count = 0;

		for (TestProgramEntityRecord currentRecord : testProgramEntityRecords) {
			count++;
			String valueString = "(:testprogramm_id, %s, %s, %s)";
			valueString = String.format(valueString, currentRecord.getEntityID(), currentRecord.getValueID(),
					currentRecord.getOrderNumber());
			queryString3 += valueString;
			if (count < recordCount) {
				queryString3 += ", ";
			}
		}

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();
		SQLQuery query3 = hibernateSession.createSQLQuery(queryString3);
		query3.setInteger("testprogramm_id", tpcModel.getCurrentTestProgramID());

		if (query3.executeUpdate() != recordCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of testprogram " + tpcModel.getCurrentTestProgramName()
					+ " failed. Updating of program details not possible.";
			throw new Exception(message);
		}
		hibernateSession.getTransaction().commit();
	}

	public static void updateProject(TPCModel tpcModel) throws Exception {
		try {
			deleteProjectEntityRelationships(tpcModel.getCurrentProjectForAdminID());
			deleteProjectValueRelationships(tpcModel.getCurrentProjectForAdminID());
			insertProjectEntityRelationships(tpcModel);
			insertProjectValueRelationships(tpcModel);
		} catch (Exception e) {
			String message = "Could not update project %s. Reason: %s";
			message = String.format(message, tpcModel.getCurrentProjectForAdminName(), e.getMessage());
			throw new Exception(message);
		}
	}

	@SuppressWarnings("unchecked")
	public static void insertProject(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString1 = "insert into project (name) values (:projectName)";

		SQLQuery query1 = hibernateSession.createSQLQuery(queryString1);

		query1.setString("projectName", tpcModel.getCurrentProjectForAdminName());

		if (query1.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of project " + tpcModel.getCurrentProjectForAdminName() + " failed.";
			throw new Exception(message);
		}
		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query2 = hibernateSession.createSQLQuery("select last_insert_id()");
		ArrayList<BigInteger> currentIds = new ArrayList<BigInteger>();
		currentIds = (ArrayList<BigInteger>) query2.list();
		if (currentIds.size() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of project " + tpcModel.getCurrentProjectForAdminName()
					+ " failed. Fetching of last inserted id failed.";
			throw new Exception(message);
		}
		hibernateSession.getTransaction().commit();

		BigInteger currentId = currentIds.get(0);

		tpcModel.setCurrentProjectForAdminID(currentId.intValue());

		try {
			insertProjectEntityRelationships(tpcModel);
			insertProjectValueRelationships(tpcModel);
		} catch (Exception e) {
			String message = "Could not save project %s. Reason: %s";
			message = String.format(message, tpcModel.getCurrentProjectForAdminName(), e.getMessage());
			throw new Exception(message);
		}
	}

	private static void insertProjectEntityRelationships(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "insert into project_entity (entity_id, project_id) values ";

		ArrayList<Integer> entityIds = tpcModel.getSelectedEntityIDs();
		Integer recordCount = entityIds.size();
		Integer count = 0;

		for (Integer currentEntityId : entityIds) {
			count++;
			String valueString = "(%s, :projectId)";
			valueString = String.format(valueString, currentEntityId);
			queryString += valueString;
			if (count < recordCount) {
				queryString += ", ";
			}
		}

		SQLQuery query = hibernateSession.createSQLQuery(queryString);
		query.setInteger("projectId", tpcModel.getCurrentProjectForAdminID());

		if (query.executeUpdate() != recordCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to insert entities for project with id = "
					+ tpcModel.getCurrentProjectForAdminID();
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	private static void insertProjectValueRelationships(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "insert into project_value (value_id, project_id) values ";

		ArrayList<Integer> valueIds = tpcModel.getSelectedValueIDs();
		Integer recordCount = valueIds.size();
		Integer count = 0;

		for (Integer currentValueId : valueIds) {
			count++;
			String valueString = "(%s, :projectId)";
			valueString = String.format(valueString, currentValueId);
			queryString += valueString;
			if (count < recordCount) {
				queryString += ", ";
			}
		}

		SQLQuery query = hibernateSession.createSQLQuery(queryString);
		query.setInteger("projectId", tpcModel.getCurrentProjectForAdminID());

		if (query.executeUpdate() != recordCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to insert values for project with id = " + tpcModel.getCurrentProjectForAdminID();
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	private static void insertValueGroupValueRelationships(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "insert into value_group_value (value_id, value_group_id) values ";

		ArrayList<Integer> valueIds = tpcModel.getSelectedValueIDsForValueGroup();
		Integer recordCount = valueIds.size();
		Integer count = 0;

		for (Integer currentValueId : valueIds) {
			count++;
			String valueString = "(%s, :value_group_id)";
			valueString = String.format(valueString, currentValueId);
			queryString += valueString;
			if (count < recordCount) {
				queryString += ", ";
			}
		}

		SQLQuery query = hibernateSession.createSQLQuery(queryString);
		query.setInteger("value_group_id", tpcModel.getCurrentValueGroupForAdminID());

		if (query.executeUpdate() != recordCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to insert values for value group with id = "
					+ tpcModel.getCurrentValueGroupForAdminID();
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	private static void deleteProjectValueRelationships(Integer projectID) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "select count(*) from project_value pe where pe.project_id = :projectId";

		SQLQuery query1 = hibernateSession.createSQLQuery(queryString);

		query1.setInteger("projectId", projectID);

		@SuppressWarnings("unchecked")
		List<BigInteger> entryCount = (List<BigInteger>) query1.list();

		if (entryCount.size() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to get number of project-value-entries for project with id = " + projectID;
			throw new Exception(message);
		}

		Integer projectValueCount = entryCount.get(0).intValue();

		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString2 = "delete from project_value where project_value.project_id = :peID";

		SQLQuery query2 = hibernateSession.createSQLQuery(queryString2);

		query2.setInteger("peID", projectID);

		if (query2.executeUpdate() != projectValueCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to delete old project-value-entries for project with id = " + projectID;
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	private static void deleteProjectEntityRelationships(Integer projectID) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "select count(*) from project_entity pe where pe.project_id = :projectId";

		SQLQuery query1 = hibernateSession.createSQLQuery(queryString);

		query1.setInteger("projectId", projectID);

		@SuppressWarnings("unchecked")
		List<BigInteger> entryCount = (List<BigInteger>) query1.list();

		if (entryCount.size() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to get number of project-entity-entries for project with id = " + projectID;
			throw new Exception(message);
		}

		Integer projectEntityCount = entryCount.get(0).intValue();

		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString2 = "delete from project_entity where project_entity.project_id = :peID";

		SQLQuery query2 = hibernateSession.createSQLQuery(queryString2);

		query2.setInteger("peID", projectID);

		if (query2.executeUpdate() != projectEntityCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to delete old project-entity-entries for project with id = " + projectID;
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	private static void deleteValueGroupValueRelationships(Integer currentValueGroupForAdminID) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "select count(*) from value_group_value vgv where vgv.value_group_id = :valueGroupID";

		SQLQuery query1 = hibernateSession.createSQLQuery(queryString);

		query1.setInteger("valueGroupID", currentValueGroupForAdminID);

		@SuppressWarnings("unchecked")
		List<BigInteger> entryCount = (List<BigInteger>) query1.list();

		if (entryCount.size() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to get number of value-group-value-entries for value group with id = "
					+ currentValueGroupForAdminID;
			throw new Exception(message);
		}

		Integer valueGroupValueCount = entryCount.get(0).intValue();

		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString2 = "delete from value_group_value vgv where vgv.value_group_id = :valueGroupID";

		SQLQuery query2 = hibernateSession.createSQLQuery(queryString2);

		query2.setInteger("valueGroupID", currentValueGroupForAdminID);

		if (query2.executeUpdate() != valueGroupValueCount) {
			hibernateSession.getTransaction().rollback();
			String message = "Unable to delete old value-group-value-entries for value group with id = "
					+ currentValueGroupForAdminID;
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	public static void insertEntity(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "insert into test_mpiscore_entities (Parameter, Parameter_dt, PO_id, definition)"
				+ " values (:parameter, :parameter_d, :po_id, :definition)";

		SQLQuery query = hibernateSession.createSQLQuery(queryString);

		query.setString("parameter", tpcModel.getCurrentEntityForAdminParameter());
		query.setString("parameter_d", tpcModel.getCurrentEntityForAdminParameterD());
		query.setString("po_id", tpcModel.getCurrentEntityForAdminPO_id());
		query.setString("definition", tpcModel.getCurrentEntityForAdminDefinition());

		if (query.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Insertion of new entity failed!";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	public static void updateEntitiy(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "update test_mpiscore_entities " + " set Parameter = :parameter, "
				+ "Parameter_dt = :parameter_dt, " + "PO_id = :po_id, " + "definition = :definition"
				+ " where OrganID = :organId";

		SQLQuery query = hibernateSession.createSQLQuery(queryString);

		query.setString("parameter", tpcModel.getCurrentEntityForAdminParameter());
		query.setString("parameter_dt", tpcModel.getCurrentEntityForAdminParameterD());
		query.setString("po_id", tpcModel.getCurrentEntityForAdminPO_id());
		query.setString("definition", tpcModel.getCurrentEntityForAdminDefinition());
		query.setInteger("organId", tpcModel.getCurrentEntityForAdminID());

		if (query.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Update of entity failed!";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	public static void insertAttributeAndValue(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "insert into test_mpiscore_values (Attribut_E, value, Attribut_D, Wert_D, data_type, format_pattern)"
				+ " values (:attribute, :value, :attribute_d, :value_d, :data_type, :format_pattern)";

		SQLQuery query = hibernateSession.createSQLQuery(queryString);

		query.setString("attribute", tpcModel.getCurrentValueForAdminAttribut_e());
		query.setString("value", tpcModel.getCurrentValueForAdminValue());
		query.setString("attribute_d", tpcModel.getCurrentValueForAdminAttribut_d());
		query.setString("value_d", tpcModel.getCurrentValueForAdminValueD());
		query.setInteger("data_type", tpcModel.getCurrentDataTypeForAdmin());
		query.setString("format_pattern", tpcModel.getCurrentFormatPatternForAdmin());

		if (query.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Insertion of new value failed!";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	public static void insertImageData(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "insert into image (file_name, value_id, copyright) "
				+ " values (:fileName, :value_id, :copyright)";

		SQLQuery query = hibernateSession.createSQLQuery(queryString);

		query.setString("fileName", FileUtilities.createImageFileName(tpcModel));
		query.setInteger("value_id", tpcModel.getCurrentValueForAdminID());
		query.setString("copyright", tpcModel.getCurrentImageCopyright());

		if (query.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Insertion of image failed!";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	public static void deleteImageData(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "delete from image i where i.value_id = :valueID";

		SQLQuery query = hibernateSession.createSQLQuery(queryString);

		query.setInteger("valueID", tpcModel.getCurrentValueForAdminID());

		if (query.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Deletion of image failed!";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	public static void updateAttributeAndValue(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString = "update test_mpiscore_values " + " set Attribut_E = :attribute_e, " + "value = :value, "
				+ " Attribut_D = :attribute_d, " + "Wert_D = :value_d, "
				+ " format_pattern = :format_pattern, data_type = :data_type " + " where ValueID = :value_id";

		SQLQuery query = hibernateSession.createSQLQuery(queryString);

		query.setString("attribute_e", tpcModel.getCurrentValueForAdminAttribut_e());
		query.setString("value", tpcModel.getCurrentValueForAdminValue());
		query.setString("attribute_d", tpcModel.getCurrentValueForAdminAttribut_d());
		query.setString("value_d", tpcModel.getCurrentValueForAdminValueD());
		query.setInteger("value_id", tpcModel.getCurrentValueForAdminID());
		query.setInteger("data_type", tpcModel.getCurrentDataTypeForAdmin());
		query.setString("format_pattern", tpcModel.getCurrentFormatPatternForAdmin());

		if (query.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Update of value failed!";
			throw new Exception(message);
		}

		hibernateSession.getTransaction().commit();
	}

	public static void insertValueGroup(TPCModel tpcModel) throws Exception {
		Session hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		String queryString1 = "insert into value_group (nameE, nameD) values (:valueGroupNameE, :valueGroupNameD)";

		SQLQuery query1 = hibernateSession.createSQLQuery(queryString1);

		query1.setString("valueGroupNameE", tpcModel.getCurrentValueGroupForAdminName());
		query1.setString("valueGroupNameD", tpcModel.getCurrentValueGroupForAdminNameD());

		if (query1.executeUpdate() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of value group " + tpcModel.getCurrentValueGroupForAdminName() + " failed.";
			throw new Exception(message);
		}
		hibernateSession.getTransaction().commit();

		hibernateSession = HibernateSessionFactory.getSessionFactory().getCurrentSession();
		hibernateSession.beginTransaction();

		SQLQuery query2 = hibernateSession.createSQLQuery("select last_insert_id()");
		ArrayList<BigInteger> currentIds = new ArrayList<BigInteger>();
		currentIds = (ArrayList<BigInteger>) query2.list();
		if (currentIds.size() != 1) {
			hibernateSession.getTransaction().rollback();
			String message = "Saving of value group " + tpcModel.getCurrentValueGroupForAdminName()
					+ " failed. Fetching of last inserted id failed.";
			throw new Exception(message);
		}
		hibernateSession.getTransaction().commit();

		BigInteger currentId = currentIds.get(0);

		tpcModel.setCurrentValueGroupForAdminID(currentId.intValue());

		try {
			insertValueGroupValueRelationships(tpcModel);
		} catch (Exception e) {
			String message = "Could not save value group %s. Reason: %s";
			message = String.format(message, tpcModel.getCurrentValueGroupForAdminName(), e.getMessage());
			throw new Exception(message);
		}

	}

	public static void updateValueGroup(TPCModel tpcModel) throws Exception {
		try {
			deleteValueGroupValueRelationships(tpcModel.getCurrentValueGroupForAdminID());
			insertValueGroupValueRelationships(tpcModel);
		} catch (Exception e) {
			String message = "Could not update value group %s. Reason: %s";
			message = String.format(message, tpcModel.getCurrentValueGroupForAdminName(), e.getMessage());
			throw new Exception(message);
		}
	}

}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db;

public class TestProgramDetails {

	public Integer getTestprogramm_id() {
		return testprogramm_id_;
	}
	
	public void setTestprogramm_id(Integer testprogramm_id) {
		testprogramm_id_ = testprogramm_id;
	}
	
	public String getTestprogrammname() {
		return testprogrammname_;
	}
	
	public void setTestprogrammname(String testprogrammname) {
		testprogrammname_ = testprogrammname;
	}
	
	public String getVersion() {
		return version_;
	}
	
	public void setVersion(String version) {
		version_ = version;
	}
	
	public String getMessobjekt() {
		return messobjekt_;
	}
	
	public void setMessobjekt(String messobjekt) {
		messobjekt_ = messobjekt;
	}
	
	public String getDescription() {
		return description_;
	}
	
	public void setDescription(String description) {
		description_ = description;
	}
	
	public Integer getTestObjectTypeId() {
		return testObjectTypeId_;
	}
	public void setTestObjectTypeId(Integer testObjectTypeId) {
		testObjectTypeId_ = testObjectTypeId;
	}

	Integer testprogramm_id_;
	String testprogrammname_;
	String version_;
	String messobjekt_;
	String description_;
	Integer testObjectTypeId_;
	
	
}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db;

import java.util.ArrayList;
import java.util.List;

public class TestProgramEntityRecord extends TPCDataRecord {
	
	public Integer getTestprogramm() {
		return testprogramm_;
	}
	
	public void setTestprogramm(Integer testprogramm) {
		testprogramm_ = testprogramm;
	}
	
	public Integer getEntityID() {
		return entityID_;
	}
	
	public void setEntityID(Integer entity) {
		entityID_ = entity;
	}
	
	public void setValueID(Integer valueID) {
		valueID_ = valueID;
	}
	
	public Integer getValueID() {
		return valueID_;
	}
	
	public Integer getOrderNumber() {
		return orderNumber_;
	}
	
	public void setOrderNumber(Integer orderNumber) {
		orderNumber_ = orderNumber;
	}

	public String getImageFileName() {
		return imageFileName_;
	}

	public void setImageFileName(String imageFileName) {
		imageFileName_ = imageFileName;
	}

//	public List<ValueRecord> getValueRecords() {
//		return valueRecords_;
//	}
//
//	public void setValueRecords(List<ValueRecord> valueRecords) {
//		this.valueRecords_ = valueRecords;
//	}
//
//	public ValueRecord getValueRecord() {
//		return valueRecord_;
//	}
//
//	public void setValueRecord_(ValueRecord valueRecord) {
//		this.valueRecord_ = valueRecord;
//	}

	public String getDataType() {
		return dataType_;
	}

	public void setDataType(String dataType) {
		dataType_ = dataType;
	}

	public String getFormatPattern() {
		return formatPattern_;
	}

	public void setFormatPattern(String formatPattern) {
		formatPattern_ = formatPattern;
	}

	private Integer testprogramm_;
	private Integer entityID_;
	//private Integer value_;
	private Integer orderNumber_ = 0;
	private String imageFileName_;
	//private List<ValueRecord> valueRecords_;
	//private ValueRecord valueRecord_;
	private Integer valueID_;
	private String dataType_;
	private String formatPattern_;
	
}

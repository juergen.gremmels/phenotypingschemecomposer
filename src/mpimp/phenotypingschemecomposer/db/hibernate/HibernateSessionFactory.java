/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db.hibernate;

import mpimp.phenotypingschemecomposer.util.Encrypter;
import mpimp.phenotypingschemecomposer.util.TPCProperties;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

/**
 * Configures and provides access to Hibernate sessions, tied to the
 * current thread of execution.  Follows the Thread Local Session
 * pattern, see {@link http://hibernate.org/42.html }.
 */
public class HibernateSessionFactory {

    /** 
     * Location of hibernate.cfg.xml file.
     * Location should be on the classpath as Hibernate uses  
     * #resourceAsStream style lookup for its configuration file. 
     * The default classpath location of the hibernate config file is 
     * in the default package. Use #setConfigFile() to update 
     * the location of the configuration file for the current session.   
     */
	private static final ThreadLocal<Session> threadLocal = new ThreadLocal<Session>();
    private static Configuration configuration = new Configuration();    
    private static org.hibernate.SessionFactory sessionFactory;

	static {
		setConfigurationProperties();
		sessionFactory = configuration.buildSessionFactory();
    }
	
    private HibernateSessionFactory() {
    }
	
	/**
     * Returns the ThreadLocal Session instance.  Lazy initialize
     * the <code>SessionFactory</code> if needed.
     *
     *  @return Session
     *  @throws HibernateException
     */
    public static Session getSession() throws HibernateException {
        Session session = (Session) threadLocal.get();

		if (session == null || !session.isOpen()) {
			if (sessionFactory == null) {
				rebuildSessionFactory();
			}
			session = (sessionFactory != null) ? sessionFactory.openSession()
					: null;
			threadLocal.set(session);
		}

        return session;
    }

	/**
     *  Rebuild hibernate session factory
     *
     */
	public static void rebuildSessionFactory() {
		setConfigurationProperties();
		configuration.configure();
		sessionFactory = configuration.buildSessionFactory();
		System.err.println("%%%% Error Creating SessionFactory %%%%");
	}

	/**
     *  Close the single hibernate session instance.
     *
     *  @throws HibernateException
     */
    public static void closeSession() throws HibernateException {
        Session session = (Session) threadLocal.get();
        threadLocal.set(null);

        if (session != null) {
            session.close();
        }
    }

	/**
     *  return session factory
     *
     */
	public static org.hibernate.SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	private static void setConfigurationProperties() {
		try {
		String username = "";
		String password = "";
		if (TPCProperties.getInstance().getProperty("useEncryptedDatabaseLoginData").equals("true")) {
			username = Encrypter.decrypt(TPCProperties.getInstance().getProperty("hibernateConnectionUsername"));
			password = Encrypter.decrypt(TPCProperties.getInstance().getProperty("hibernateConnectionPassword"));
		} else {
			username = TPCProperties.getInstance().getProperty("hibernateConnectionUsername");
			password = TPCProperties.getInstance().getProperty("hibernateConnectionPassword");
		}
		configuration.setProperty("hibernate.connection.username", username);
		configuration.setProperty("hibernate.connection.url", HibernateSessionFactory.createConnectionUrl());
		configuration.setProperty("hibernate.dialect", TPCProperties.getInstance().getProperty("hibernateDialect"));
		configuration.setProperty("hibernate.myeclipse.connection.profile", TPCProperties.getInstance().getProperty("hibernateConnectionProfile"));
		configuration.setProperty("hibernate.connection.password", password);
		configuration.setProperty("hibernate.connection.driver_class", TPCProperties.getInstance().getProperty("hibernateConnectionDriverClass"));
		configuration.setProperty("hibernate.c3p0.max_size", TPCProperties.getInstance().getProperty("hibernateC3POMaxSize"));
		configuration.setProperty("hibernate.c3p0.min_size", TPCProperties.getInstance().getProperty("hibernateC3POMinSize"));
		configuration.setProperty("hibernate.c3p0.timeout", TPCProperties.getInstance().getProperty("hibernateC3POTimeout"));
		configuration.setProperty("hibernate.c3p0.max_statements", TPCProperties.getInstance().getProperty("hibernateC3POMaxStatements"));
		configuration.setProperty("hibernate.current_session_context_class", TPCProperties.getInstance().getProperty("hibernateCurrentSessionContextClass"));
		configuration.setProperty("hibernate.connection.provider_class", TPCProperties.getInstance().getProperty("hibernateConnectionProviderClass"));
		configuration.setProperty("hibernate.show_sql", TPCProperties.getInstance().getProperty("hibernateShowSQL"));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private static String createConnectionUrl() {
		String connectionUrl = "jdbc:mysql://";
		connectionUrl += TPCProperties.getInstance().getProperty("databaseServerName");
		connectionUrl += "/";
		connectionUrl += TPCProperties.getInstance().getProperty("databaseName");
		return connectionUrl;
	}
	
	/**
     *  return hibernate configuration
     *
     */
	public static Configuration getConfiguration() {
		return configuration;
	}

}
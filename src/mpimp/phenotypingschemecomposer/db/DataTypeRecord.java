package mpimp.phenotypingschemecomposer.db;

public class DataTypeRecord extends TPCDataRecord {

	public DataTypeRecord() {
		
	}

	public Integer getDataTypeID() {
		return dataTypeID_;
	}
	
	public void setDataTypeID(Integer dataTypeID) {
		dataTypeID_ = dataTypeID;
	}
	
	public String getDataTypeName() {
		return dataTypeName_;
	}
	
	public void setDataTypeName(String dataTypeName) {
		dataTypeName_ = dataTypeName;
	}
	
	private Integer dataTypeID_;
	private String dataTypeName_;
	
}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import mpimp.phenotypingschemecomposer.util.EntityRecordComparator;
import mpimp.phenotypingschemecomposer.util.TPCBasicComparator;
import mpimp.phenotypingschemecomposer.util.ValueRecordComparator;

public class TPCModel {

	public ArrayList<EntityRecord> getEntities() {
		Collections.sort(entities_, new EntityRecordComparator(this));
		return entities_;
	}

	public EntityRecord getEntityRecordForId(Integer entityId) {
		return entityMap_.get(entityId);
	}

	public void setEntities(ArrayList<EntityRecord> entities) {
		entities_ = entities;
		entityMap_ = new HashMap<Integer, EntityRecord>();
		for (EntityRecord currentRecord : entities) {
			entityMap_.put(currentRecord.getOrganID(), currentRecord);
		}
	}

	public ArrayList<EntityRecord> getSelectedEntities() {
		return selectedEntities_;
	}

	public void setSelectedEntities(ArrayList<EntityRecord> selectedEntities) {
		selectedEntities_ = selectedEntities;
	}

	public ArrayList<Integer> getSelectedEntityIDs() {
		return selectedEntityIDs_;
	}

	public void setSelectedEntityIDs(ArrayList<Integer> selectedEntityIDs) {
		selectedEntityIDs_ = selectedEntityIDs;
	}

	public ArrayList<EntityRecord> getNotSelectedEntities() {
		return notSelectedEntities_;
	}

	public void setNotSelectedEntities(
			ArrayList<EntityRecord> notSelectedEntities) {
		notSelectedEntities_ = notSelectedEntities;
	}

	public ArrayList<Integer> getNotSelectedEntityIDs() {
		return notSelectedEntityIDs_;
	}

	public void setNotSelectedEntityIDs(ArrayList<Integer> notSelectedEntityIDs) {
		notSelectedEntityIDs_ = notSelectedEntityIDs;
	}

	public ArrayList<ValueRecord> getNotSelectedValues() {
		return notSelectedValues_;
	}

	public void setNotSelectedValues(ArrayList<ValueRecord> notSelectedValues) {
		notSelectedValues_ = notSelectedValues;
	}

	public ArrayList<Integer> getNotSelectedValueIDs() {
		return notSelectedValueIDs_;
	}

	public void setNotSelectedValueIDs(ArrayList<Integer> notSelectedValueIDs) {
		notSelectedValueIDs_ = notSelectedValueIDs;
	}

	public ArrayList<ValueRecord> getSelectedValues() {
		return selectedValues_;
	}

	public void setSelectedValues(ArrayList<ValueRecord> selectedValues) {
		selectedValues_ = selectedValues;
	}

	public ArrayList<Integer> getSelectedValueIDs() {
		return selectedValueIDs_;
	}

	public void setSelectedValueIDs(ArrayList<Integer> selectedValueIDs) {
		selectedValueIDs_ = selectedValueIDs;
	}

	public ArrayList<ValueRecord> getSelectedValuesForValueGroup() {
		return selectedValuesForValueGroup_;
	}

	public void setSelectedValuesForValueGroup(ArrayList<ValueRecord> selectedValuesForValueGroup) {
		selectedValuesForValueGroup_ = selectedValuesForValueGroup;
	}

	public ArrayList<Integer> getSelectedValueIDsForValueGroup() {
		return selectedValueIDsForValueGroup_;
	}

	public void setSelectedValueIDsForValueGroup(ArrayList<Integer> selectedValueIDsForValueGroup) {
		selectedValueIDsForValueGroup_ = selectedValueIDsForValueGroup;
	}

	public ArrayList<ValueRecord> getNotSelectedValuesForValueGroup() {
		return notSelectedValuesForValueGroup_;
	}

	public void setNotSelectedValuesForValueGroup(ArrayList<ValueRecord> notSelectedValuesForValueGroup) {
		notSelectedValuesForValueGroup_ = notSelectedValuesForValueGroup;
	}

	public ArrayList<Integer> getNotSelectedValueIDsForValueGroup() {
		return notSelectedValueIDsForValueGroup_;
	}

	public void setNotSelectedValueIDsForValueGroup(ArrayList<Integer> notSelectedValueIDsForValueGroup) {
		notSelectedValueIDsForValueGroup_ = notSelectedValueIDsForValueGroup;
	}

	public EntityRecord getNewEntityRecord() {
		return newEntityRecord_;
	}

	public void setNewEntityRecord(EntityRecord newEntityRecord) {
		newEntityRecord_ = newEntityRecord;
	}

	public Integer getCurrentEntityForAdminID() {
		return currentEntityForAdminID_;
	}

	public void setCurrentEntityForAdminID(Integer currentEntityForAdminID) {
		currentEntityForAdminID_ = currentEntityForAdminID;
	}

	public ArrayList<ValueRecord> getValues() {
		Collections.sort(values_, new ValueRecordComparator(this));
		return values_;
	}

	public ValueRecord getValueRecordForId(Integer valueId) {
		return valueMap_.get(valueId);
	}

	public ValueRecord getNewValueRecord() {
		return newValueRecord_;
	}

	public void setNewValueRecord(ValueRecord newValueRecord) {
		newValueRecord_ = newValueRecord;
	}

	public Integer getCurrentValueForAdminID() {
		return currentValueForAdminID_;
	}

	public void setCurrentValueForAdminID(Integer currentValueForAdminID) {
		currentValueForAdminID_ = currentValueForAdminID;
	}

	public void setValues(ArrayList<ValueRecord> values) {
		values_ = values;
		valueMap_ = new HashMap<Integer, ValueRecord>();
		for (ValueRecord currentRecord : values) {
			valueMap_.put(currentRecord.getValueID(), currentRecord);
		}
	}

	public Integer getCurrentProjectID() {
		return currentProjectID_;
	}

	public void setCurrentProjectID(Integer currentProjectID) {
		currentProjectID_ = currentProjectID;
	}

	public Integer getCurrentProjectForAdminID() {
		return currentProjectForAdminID_;
	}

	public void setCurrentProjectForAdminID(Integer currentProjectForAdminID) {
		currentProjectForAdminID_ = currentProjectForAdminID;
	}

	public Boolean getNewProject() {
		return newProject_;
	}

	public void setNewProject(Boolean newProject) {
		newProject_ = newProject;
	}

	public String getCurrentProjectForAdminName() {
		return currentProjectForAdminName_;
	}

	public void setCurrentProjectForAdminName(String currentProjectForAdminName) {
		currentProjectForAdminName_ = currentProjectForAdminName;
	}

	public ArrayList<ProjectRecord> getProjects() {
		return projects_;
	}

	public ArrayList<String> getProjectNames() {
		return projectNames_;
	}

	public ProjectRecord getNewProjectRecord() {
		return newProjectRecord_;
	}

	public void setNewProjectRecord(ProjectRecord newProjectRecord) {
		newProjectRecord_ = newProjectRecord;
	}

	public void setProjects(ArrayList<ProjectRecord> projects) {
		projects_ = projects;
		projectMap_ = new HashMap<Integer, ProjectRecord>();
		projectNames_ = new ArrayList<String>();
		for (ProjectRecord currentProject : projects_) {
			projectMap_.put(currentProject.getProject_id(), currentProject);
			projectNames_.add(currentProject.getName());
		}
	}

	public ProjectRecord getProjectRecordForId(Integer projectId) {
		return projectMap_.get(projectId);
	}

	public ValueGroupRecord getValueGroupRecordForId(Integer valueGroupId) {
		return valueGroupMap_.get(valueGroupId);
	}
	
	public ArrayList<TestProgramEntityRecord> getTestProgramEntityRecords() {
		return testProgramEntityRecords_;
	}

	public void setTestProgramEntityRecords(
			ArrayList<TestProgramEntityRecord> testProgramEntityRecords) {
		testProgramEntityRecords_ = testProgramEntityRecords;
	}

	public Integer getTestProgramEntityRecordCount() {
		if (testProgramEntityRecords_ != null) {
			return testProgramEntityRecords_.size();
		} else {
			return 0;
		}
	}

	public ArrayList<TestProgramRecord> getTestProgramRecords() {
		return testProgramRecords_;
	}

	public void setTestProgramRecords(
			ArrayList<TestProgramRecord> testProgramRecords) {
		testProgramRecords_ = testProgramRecords;
		testProgramNames_ = new ArrayList<String>();
		for (TestProgramRecord currentTPR : testProgramRecords_) {
			testProgramNames_.add(currentTPR.getTestprogrammname());
		}
	}

	public ArrayList<String> getTestProgramNames() {
		return testProgramNames_;
	}

	public String getDefaultOption() {
		return defaultOption_;
	}

	public void setDefaultOption(String defaultOption) {
		defaultOption_ = defaultOption;
	}

	public String getDefaultOptionD() {
		return defaultOptionD;
	}

	public void setDefaultOptionD(String defaultOptionD) {
		this.defaultOptionD = defaultOptionD;
	}

	public String getNoProjectOption() {
		return noProjectOption;
	}

	public void setNoProjectOption(String noProjectOption) {
		this.noProjectOption = noProjectOption;
	}

	public String getNoProjectOptionD() {
		return noProjectOptionD;
	}

	public void setNoProjectOptionD(String noProjectOptionD) {
		this.noProjectOptionD = noProjectOptionD;
	}

	public Boolean getNewTestProgram() {
		return newTestProgram_;
	}

	public Boolean getTestProgramChanged() {
		return testProgramChanged_;
	}

	public void setTestProgramChanged(Boolean testProgramChanged) {
		testProgramChanged_ = testProgramChanged;
	}

	public void setNewTestProgram(Boolean newTestProgram) {
		newTestProgram_ = newTestProgram;
	}

	public Integer getCurrentTestProgramID() {
		return currentTestProgramID_;
	}

	public void setCurrentTestProgramID(Integer currentTestProgramID) {
		currentTestProgramID_ = currentTestProgramID;
	}

	public String getCurrentTestProgramName() {
		return currentTestProgramName_;
	}

	public void setCurrentTestProgramName(String currentTestProgramName) {
		currentTestProgramName_ = currentTestProgramName;
	}

	public String getCurrentTestProgramVersion() {
		return currentTestProgramVersion_;
	}

	public void setCurrentTestProgramVersion(String currentTestProgramVersion) {
		currentTestProgramVersion_ = currentTestProgramVersion;
	}

	public String getCurrentTestProgramMessobjekt() {
		return currentTestProgramMessobjekt_;
	}

	public void setCurrentTestProgramMessobjekt(
			String currentTestProgramMessobjekt) {
		currentTestProgramMessobjekt_ = currentTestProgramMessobjekt;
	}

	public String getCurrentTestProgramDescription() {
		return currentTestProgramDescription_;
	}

	public void setCurrentTestProgramDescription(
			String currentTestProgramDescription) {
		currentTestProgramDescription_ = currentTestProgramDescription;
	}

	public TPCBasicComparator getTpcComparator() {
		return tpcComparator_;
	}

	public void setTpcComparator(TPCBasicComparator tpcComparator) {
		tpcComparator_ = tpcComparator;
	}

	public String getUserLogin() {
		return userLogin_;
	}

	public void setUserLogin(String userLogin) {
		userLogin_ = userLogin;
	}

	public String getPasswd() {
		return passwd_;
	}

	public void setPasswd(String password) {
		passwd_ = password;
	}

	public Boolean getUserAuthenticated() {
		return userAuthenticated_;
	}

	public void setUserAuthenticated(Boolean userAuthenticated) {
		userAuthenticated_ = userAuthenticated;
	}

	public OperatorRecord getOperator() {
		return operator_;
	}

	public void setOperator(OperatorRecord operator) {
		operator_ = operator;
	}

	public Map<String, String> getLocales() {
		return locales_;
	}

	public void setLocales(Map<String, String> locales) {
		this.locales_ = locales;
	}

	public Map<String, String> getExportFormats() {
		return exportFormats_;
	}

	public void setExportFormats(Map<String, String> exportFormats) {
		exportFormats_ = exportFormats;
	}

	public String getCurrentExportFormat() {
		return currentExportFormat_;
	}

	public void setCurrentExportFormat(String currentExportFormat) {
		currentExportFormat_ = currentExportFormat;
	}

	public String getCurrentLocaleString() {
		String currentLocaleString = currentLocale_.toString();
		return currentLocaleString;
	}

	public Locale getCurrentLocale() {
		return currentLocale_;
	}

	public void setCurrentLocale(Locale currentLocale) {
		currentLocale_ = currentLocale;
	}

	public Boolean getCompareAttributeAndValueAscending() {
		return compareAttributeAndValueAscending_;
	}

	public void setCompareAttributeAndValueAscending(
			Boolean compareAttributeAndValueAscending) {
		compareAttributeAndValueAscending_ = compareAttributeAndValueAscending;
	}

	public Boolean getCompareEntityAscending() {
		return compareEntityAscending_;
	}

	public void setCompareEntityAscending(Boolean compareEntityAscending) {
		compareEntityAscending_ = compareEntityAscending;
	}

	public Boolean getCompareOrderNumberAscending() {
		return compareOrderNumberAscending_;
	}

	public void setCompareOrderNumberAscending(
			Boolean compareOrderNumberAscending) {
		compareOrderNumberAscending_ = compareOrderNumberAscending;
	}

	public Integer getCurrentTestObjectType() {
		return currentTestObjectType_;
	}

	public void setCurrentTestObjectType(Integer currentTestObjectType) {
		currentTestObjectType_ = currentTestObjectType;
	}

	public TestObjectTypeRecord getCurrentTestObjectTypeRecord() {
		return testObjectTypeRecordMap_.get(currentTestObjectType_);
	}

	public ArrayList<TestObjectTypeRecord> getTestObjectTypeRecords() {
		return testObjectTypeRecords_;
	}

	public void setTestObjectTypeRecords(
			ArrayList<TestObjectTypeRecord> testObjectTypeRecords) {
		testObjectTypeRecords_ = testObjectTypeRecords;
		testObjectTypeRecordMap_ = new HashMap<Integer, TestObjectTypeRecord>();
		for (TestObjectTypeRecord currrentTORecord : testObjectTypeRecords_) {
			testObjectTypeRecordMap_.put(
					currrentTORecord.getTestObjectType_id(), currrentTORecord);
		}
	}

	public String getSelectedTab() {
		return selectedTab_;
	}

	public void setSelectedTab(String selectedTab) {
		selectedTab_ = selectedTab;
	}

	public String getSubmitMethod() {
		return submitMethod_;
	}

	public void setSubmitMethod(String submitMethod) {
		submitMethod_ = submitMethod;
	}

	public Boolean getTestProgramLoaded() {
		return testProgramLoaded_;
	}

	public void setTestProgramLoaded(Boolean testProgramLoaded) {
		testProgramLoaded_ = testProgramLoaded;
	}

	public Boolean getNewEntity() {
		return newEntity_;
	}

	public void setNewEntity(Boolean newEntity) {
		newEntity_ = newEntity;
	}

	public Boolean getNewAttributeAndValue() {
		return newAttributeAndValue_;
	}

	public void setNewAttributeAndValue(Boolean newAttributeAndValue) {
		newAttributeAndValue_ = newAttributeAndValue;
	}

	public ArrayList<EntityRecord> getEntitiesForAdmin() {
		Collections.sort(entitiesForAdmin_, new EntityRecordComparator(this));
		return entitiesForAdmin_;
	}

	public void setEntitiesForAdmin(ArrayList<EntityRecord> entitiesForAdmin) {
		entitiesForAdmin_ = entitiesForAdmin;
		entityMapForAdmin_ = new HashMap<Integer, EntityRecord>();
		entityNamesForAdmin_ = new ArrayList<String>();
		for (EntityRecord currentEntityRecord : entitiesForAdmin_) {
			entityMapForAdmin_.put(currentEntityRecord.getOrganID(), currentEntityRecord);
			entityNamesForAdmin_.add(currentEntityRecord.getParameter());
		}
	}

	public EntityRecord getEntityRecordForAdminById(Integer entityId) {
		return entityMapForAdmin_.get(entityId);
	}
	
	public ArrayList<ValueRecord> getValuesForAdmin() {
		Collections.sort(valuesForAdmin_, new ValueRecordComparator(this));
		return valuesForAdmin_;
	}

	public void setValuesForAdmin(ArrayList<ValueRecord> valuesForAdmin) {
		valuesForAdmin_ = valuesForAdmin;
		valueMapForAdmin_ = new HashMap<Integer, ValueRecord>();
		valueNamesForAdmin_ = new ArrayList<String>();
		attributeNamesForAdmin_ = new ArrayList<String>();
		for (ValueRecord currentValueRecord : valuesForAdmin_) {
			valueMapForAdmin_.put(currentValueRecord.getValueID(), currentValueRecord);
			valueNamesForAdmin_.add(currentValueRecord.getValue());
			attributeNamesForAdmin_.add(currentValueRecord.getAttribut_e());
		}
	}

	public ValueRecord getValueRecordForAdminById(Integer valueId) {
		return valueMapForAdmin_.get(valueId);
	}
	
	public String getCurrentEntityForAdminParameter() {
		return currentEntityForAdminParameter_;
	}

	public void setCurrentEntityForAdminParameter(
			String currentEntityForAdminParameter) {
		currentEntityForAdminParameter_ = currentEntityForAdminParameter;
	}

	public String getCurrentEntityForAdminParameterD() {
		return currentEntityForAdminParameterD_;
	}

	public void setCurrentEntityForAdminParameterD(
			String currentEntityForAdminParameterD) {
		currentEntityForAdminParameterD_ = currentEntityForAdminParameterD;
	}

	public String getCurrentEntityForAdminPO_id() {
		return currentEntityForAdminPO_id_;
	}

	public void setCurrentEntityForAdminPO_id(String currentEntityForAdminPO_id) {
		currentEntityForAdminPO_id_ = currentEntityForAdminPO_id;
	}

	public String getCurrentEntityForAdminDefinition() {
		return currentEntityForAdminDefinition_;
	}

	public void setCurrentEntityForAdminDefinition(
			String currentEntityForAdminDefinition) {
		currentEntityForAdminDefinition_ = currentEntityForAdminDefinition;
	}

	public String getCurrentValueForAdminAttribut_d() {
		return currentValueForAdminAttribut_d_;
	}

	public void setCurrentValueForAdminAttribut_d(
			String currentValueForAdminAttribut_d) {
		currentValueForAdminAttribut_d_ = currentValueForAdminAttribut_d;
	}

	public String getCurrentValueForAdminAttribut_e() {
		return currentValueForAdminAttribut_e_;
	}

	public void setCurrentValueForAdminAttribut_e(
			String currentValueForAdminAttribut_e) {
		currentValueForAdminAttribut_e_ = currentValueForAdminAttribut_e;
	}

	public String getCurrentValueForAdminValue() {
		return currentValueForAdminValue_;
	}

	public void setCurrentValueForAdminValue(String currentValueForAdminValue) {
		currentValueForAdminValue_ = currentValueForAdminValue;
	}

	public String getCurrentValueForAdminValueD() {
		return currentValueForAdminValueD_;
	}

	public void setCurrentValueForAdminValueD(String currentValueForAdminValueD) {
		currentValueForAdminValueD_ = currentValueForAdminValueD;
	}

	public Integer getCurrentValueForAdminValueGroup() {
		return currentValueForAdminValueGroup_;
	}

	public void setCurrentValueForAdminValueGroup(
			Integer currentValueForAdminValueGroup) {
		currentValueForAdminValueGroup_ = currentValueForAdminValueGroup;
	}
	
	public Integer getCurrentDataTypeForAdmin() {
		return currentDataTypeForAdmin_;
	}

	public void setCurrentDataTypeForAdmin(Integer currentDataTypeForAdmin) {
		currentDataTypeForAdmin_ = currentDataTypeForAdmin;
	}

	public String getCurrentFormatPatternForAdmin() {
		return currentFormatPatternForAdmin_;
	}

	public void setCurrentFormatPatternForAdmin(String currentFormatPatternForAdmin) {
		currentFormatPatternForAdmin_ = currentFormatPatternForAdmin;
	}

	public String getSourceFile() {
		return sourceFile_;
	}

	public void setSourceFile(String sourceFile) {
		sourceFile_ = sourceFile;
	}

	public ArrayList<String> getEntityNamesForAdmin() {
		return entityNamesForAdmin_;
	}

	public ArrayList<String> getAttributeNamesForAdmin() {
		return attributeNamesForAdmin_;
	}

	public ArrayList<String> getValueNamesForAdmin() {
		return valueNamesForAdmin_;
	}

	public ArrayList<DataTypeRecord> getDataTypes() {
		return dataTypes_;
	}

	public void setDataTypes(ArrayList<DataTypeRecord> dataTypes) {
		dataTypes_ = dataTypes;
		dataTypeMap_ = new HashMap<Integer, DataTypeRecord>();
		if (dataTypes_ != null) {
			for (DataTypeRecord currentDtRecord : dataTypes_) {
				dataTypeMap_.put(currentDtRecord.getDataTypeID(), currentDtRecord);
			}
		}
	}

	public String getDataTypeNameForID(Integer dataTypeID) {
		if (dataTypeMap_ != null && dataTypeMap_.get(dataTypeID) != null) {
			return dataTypeMap_.get(dataTypeID).getDataTypeName();
		} else {
			return "other";
		}
	}
	
	public String getCurrentImageFile() {
		return currentImageFile_;
	}

	public void setCurrentImageFile(String currentImageFile) {
		currentImageFile_ = currentImageFile;
	}

	public String getCurrentImageFileFileName() {
		return currentImageFileFileName_;
	}

	public void setCurrentImageFileFileName(String currentImageFileFileName) {
		currentImageFileFileName_ = currentImageFileFileName;
	}

	public String getCurrentImageFileContentType() {
		return currentImageFileContentType_;
	}

	public void setCurrentImageFileContentType(String currentImageFileContentType) {
		currentImageFileContentType_ = currentImageFileContentType;
	}

	public String getCurrentImageFileForDisplay() {
		return currentImageFileForDisplay_;
	}

	public void setCurrentImageFileForDisplay(String currentImageFileForDisplay) {
		currentImageFileForDisplay_ = currentImageFileForDisplay;
	}

	public String getCurrentImageCopyright() {
		return currentImageCopyright_;
	}

	public void setCurrentImageCopyright(String currentImageCopyright) {
		currentImageCopyright_ = currentImageCopyright;
	}

	public Boolean getAboutToSaveChanges() {
		return aboutToSaveChanges_;
	}

	public void setAboutToSaveChanges(Boolean aboutToSaveChanges) {
		aboutToSaveChanges_ = aboutToSaveChanges;
	}

	public String getXmlReadingErrors() {
		return xmlReadingErrors_;
	}

	public void setXmlReadingErrors(String xmlReadingErrors) {
		xmlReadingErrors_ = xmlReadingErrors;
	}

	public String getLinkToOntologyLookupService() {
		return linkToOntologyLookupService_;
	}

	public void setLinkToOntologyLookupService(String linkToOntologyLookupService) {
		linkToOntologyLookupService_ = linkToOntologyLookupService;
	}

	public ArrayList<ValueGroupRecord> getValueGroups() {
		return valueGroups_;
	}

	public void setValueGroups(ArrayList<ValueGroupRecord> valueGroups) {
		valueGroups_ = valueGroups;
		valueGroupMap_ = new HashMap<Integer, ValueGroupRecord>();
		valueGroupNames_ = new ArrayList<String>();
		for (ValueGroupRecord currentValueGroup : valueGroups_) {
			valueGroupMap_.put(currentValueGroup.getValueGroup_id(), currentValueGroup);
			valueGroupNames_.add(currentValueGroup.getNameE());
		}
	}

	public Integer getCurrentValueGroupID() {
		return currentValueGroupID_;
	}

	public void setCurrentValueGroupID(Integer currentValueGroupID) {
		currentValueGroupID_ = currentValueGroupID;
	}

	public Integer getCurrentValueGroupForAdminID() {
		return currentValueGroupForAdminID_;
	}

	public void setCurrentValueGroupForAdminID(Integer currentValueGroupForAdminID) {
		currentValueGroupForAdminID_ = currentValueGroupForAdminID;
	}

	public Boolean getNewValueGroup() {
		return newValueGroup_;
	}

	public void setNewValueGroup(Boolean newValueGroup) {
		newValueGroup_ = newValueGroup;
	}

	public String getCurrentValueGroupForAdminName() {
		return currentValueGroupForAdminName_;
	}

	public void setCurrentValueGroupForAdminName(String currentValueGroupForAdminName) {
		currentValueGroupForAdminName_ = currentValueGroupForAdminName;
	}

	public String getCurrentValueGroupForAdminNameD() {
		return currentValueGroupForAdminNameD_;
	}

	public void setCurrentValueGroupForAdminNameD(String currentValueGroupForAdminNameD) {
		currentValueGroupForAdminNameD_ = currentValueGroupForAdminNameD;
	}

	public ValueGroupRecord getNewValueGroupRecord() {
		return newValueGroupRecord_;
	}

	public void setNewValueGroupRecord(ValueGroupRecord newValueGroupRecord) {
		newValueGroupRecord_ = newValueGroupRecord;
	}

	public void resetTestProgramData() {
		testProgramEntityRecords_ = new ArrayList<TestProgramEntityRecord>();
		testProgramEntityRecords_.add(new TestProgramEntityRecord());
		currentTestProgramID_ = 0;
		currentTestProgramName_ = "";
		currentTestProgramVersion_ = "";
		currentTestProgramMessobjekt_ = "";
		currentTestObjectType_ = 0;
		currentTestProgramDescription_ = "";
		newTestProgram_ = false;
		testProgramLoaded_ = false;
		currentProjectID_ = 0;
		currentValueGroupID_ = 0;
	}

	public void clearAdministrationData() {
		notSelectedEntities_ = new ArrayList<EntityRecord>();
		selectedEntities_ = new ArrayList<EntityRecord>();
		notSelectedEntityIDs_ = new ArrayList<Integer>();
		selectedEntityIDs_ = new ArrayList<Integer>();
		entitiesForAdmin_ = new ArrayList<EntityRecord>();
		newEntityRecord_ = new EntityRecord();
		currentEntityForAdminID_ = 0;
		currentEntityForAdminParameter_ = "";
		currentEntityForAdminParameterD_ = "";
		currentEntityForAdminPO_id_ = "";
		currentEntityForAdminDefinition_ = "";
		notSelectedValues_ = new ArrayList<ValueRecord>();
		selectedValues_ = new ArrayList<ValueRecord>();
		notSelectedValueIDs_ = new ArrayList<Integer>();
		selectedValueIDs_ = new ArrayList<Integer>();
		selectedValuesForValueGroup_ = new ArrayList<ValueRecord>();
		selectedValueIDsForValueGroup_ = new ArrayList<Integer>();
		notSelectedValuesForValueGroup_ = new ArrayList<ValueRecord>();
		notSelectedValueIDsForValueGroup_ = new ArrayList<Integer>();
		newValueRecord_ = new ValueRecord();
		valuesForAdmin_ = new ArrayList<ValueRecord>();
		currentValueForAdminID_ = 0;
		currentValueForAdminAttribut_d_ = "";
		currentValueForAdminAttribut_e_ = "";
		currentValueForAdminValue_ = "";
		currentValueForAdminValueD_ = "";
		currentValueForAdminValueGroup_ = 0;
		currentDataTypeForAdmin_ = 5;
		currentFormatPatternForAdmin_ = "";
		currentProjectForAdminID_ = 0;
		currentProjectForAdminName_ = "";
		currentValueGroupForAdminID_ = 0;
		currentValueGroupForAdminName_ = "";
		currentValueGroupForAdminNameD_ = "";
		currentImageFile_ = "";
		currentImageFileFileName_ = "";
		currentImageFileContentType_ = "";
		currentImageFileForDisplay_ = "";
		currentImageCopyright_ = "";
		aboutToSaveChanges_ = false;

		newProject_ = false;
		newEntity_ = false;
		newAttributeAndValue_ = false;
		newValueGroup_ = false;
	}

	// entities
	private ArrayList<EntityRecord> entities_;
	private ArrayList<EntityRecord> entitiesForAdmin_; // for administration
	private ArrayList<String> entityNamesForAdmin_; // for administration
	private ArrayList<EntityRecord> notSelectedEntities_; // for administration
	private ArrayList<Integer> notSelectedEntityIDs_; // for administration
	private ArrayList<EntityRecord> selectedEntities_; // for administration
	private ArrayList<Integer> selectedEntityIDs_; // for administration
	private HashMap<Integer, EntityRecord> entityMap_;
	private HashMap<Integer, EntityRecord> entityMapForAdmin_;  // for administration
	private EntityRecord newEntityRecord_; // for administration
	private Integer currentEntityForAdminID_; // for administration
	private String currentEntityForAdminParameter_; // for administration
	private String currentEntityForAdminParameterD_; // for administration
	private String currentEntityForAdminPO_id_; // for administration
	private String currentEntityForAdminDefinition_; // for administration
	private Boolean newEntity_ = false; // for administration 
	
	// values
	private ArrayList<ValueRecord> values_;
	private ArrayList<ValueRecord> valuesForAdmin_; // for administration
	private ArrayList<String> attributeNamesForAdmin_; // for administration
	private ArrayList<String> valueNamesForAdmin_; // for administration
	private ArrayList<ValueRecord> notSelectedValues_; // for administration
	private ArrayList<Integer> notSelectedValueIDs_; // for administration
	private ArrayList<ValueRecord> selectedValues_; // for administration
	private ArrayList<Integer> selectedValueIDs_; // for administration
	private ArrayList<ValueRecord> selectedValuesForValueGroup_; // for administration
	private ArrayList<Integer> selectedValueIDsForValueGroup_; // for administration
	private ArrayList<ValueRecord> notSelectedValuesForValueGroup_; // for administration
	private ArrayList<Integer> notSelectedValueIDsForValueGroup_; // for administration
	private HashMap<Integer, ValueRecord> valueMap_;
	private HashMap<Integer, ValueRecord> valueMapForAdmin_;  // for administration
	private ValueRecord newValueRecord_; // for administration
	private Integer currentValueForAdminID_; // for administration
	private String currentValueForAdminAttribut_d_; // for administration
	private String currentValueForAdminAttribut_e_; // for administration
	private String currentValueForAdminValue_; // for administration
	private String currentValueForAdminValueD_; // for administration
	private Integer currentValueForAdminValueGroup_; // for administration
	private String currentImageFile_; // for administration
	private String currentImageFileFileName_; // for administration
	private String currentImageFileContentType_;  // for administration
	private Boolean newAttributeAndValue_ = false; // for administration
	private String currentImageFileForDisplay_; // for administration
	private String currentImageCopyright_; // for administration
	private Integer currentDataTypeForAdmin_ = 5; // for administration
	private String currentFormatPatternForAdmin_; // for administration
	
	// value groups
	private ArrayList<ValueGroupRecord> valueGroups_;
	private ArrayList<String> valueGroupNames_;
	private HashMap<Integer, ValueGroupRecord> valueGroupMap_;
	private Integer currentValueGroupID_;
	private Integer currentValueGroupForAdminID_; // for administration
	private Boolean newValueGroup_ = false; // for administration
	private String currentValueGroupForAdminName_; // for administration
	private String currentValueGroupForAdminNameD_; // for administration
	private ValueGroupRecord newValueGroupRecord_; // for administration
	
	// projects
	private ArrayList<ProjectRecord> projects_;
	private ArrayList<String> projectNames_;
	private HashMap<Integer, ProjectRecord> projectMap_;
	private Integer currentProjectID_;
	private Integer currentProjectForAdminID_; // for administration
	private Boolean newProject_ = false; // for administration
	private String currentProjectForAdminName_; // for administration
	private ProjectRecord newProjectRecord_;
	
	// data types
	private ArrayList<DataTypeRecord> dataTypes_;
	private Map<Integer, DataTypeRecord> dataTypeMap_;
	
	// test programs / test schemes
	private ArrayList<TestProgramEntityRecord> testProgramEntityRecords_;
	private ArrayList<TestProgramRecord> testProgramRecords_;
	private ArrayList<String> testProgramNames_;
	private Boolean newTestProgram_ = false;
	private Integer currentTestProgramID_;
	private String currentTestProgramName_;
	private String currentTestProgramVersion_;
	private String currentTestProgramMessobjekt_;
	private String currentTestProgramDescription_;
	private Boolean testProgramLoaded_ = false;
	private Boolean testProgramChanged_ = false;
	
	// test object
	private Integer currentTestObjectType_;
	private ArrayList<TestObjectTypeRecord> testObjectTypeRecords_;
	private HashMap<Integer, TestObjectTypeRecord> testObjectTypeRecordMap_;
	
	// miscellaneous
	private String defaultOption_ = "(please select)";
	private String defaultOptionD = "(bitte w�hlen)";
	private String noProjectOption = "(no project selected)";
	private String noProjectOptionD = "(kein Projekt ausgew�hlt)";
	private TPCBasicComparator tpcComparator_;
	private String userLogin_;
	private String passwd_;
	private Boolean userAuthenticated_ = false;
	private OperatorRecord operator_;
	private Map<String, String> locales_;
	private Map<String, String> exportFormats_;
	private String currentExportFormat_ = "xml";
	private Locale currentLocale_;
	private Boolean compareAttributeAndValueAscending_ = true;
	private Boolean compareEntityAscending_ = true;
	private Boolean compareOrderNumberAscending_ = true;
	private String selectedTab_ = "tab1";
	private String submitMethod_;
	private String sourceFile_;
	private Boolean aboutToSaveChanges_ = false;
	private String xmlReadingErrors_ = "";
	private String linkToOntologyLookupService_;
}

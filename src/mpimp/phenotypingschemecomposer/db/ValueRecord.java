/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db;

public class ValueRecord extends TPCDataRecord {
	
	public Integer getValueID() {
		return valueID_;
	}
	
	public void setValueID(Integer valueID) {
		valueID_ = valueID;
	}
	
	public String getAttribut_e() {
		return attribut_e_;
	}
	
	public void setAttribut_e(String attribut_e) {
		attribut_e_ = attribut_e;
	}
	
	public void setValue(String value) {
		value_ = value;
	}
	
	public String getValue() {
		return value_;
	}
	
	public String getAttributeAndValue() {
		return attribut_e_ + " | " + value_ + " | " + dataTypeName_;
	}
	
	public String getAttributeAndValueD() {
		return attribut_d_ + " | " + value_d_ + " | " + dataTypeName_;
	}
	
	public String getAttribut_d() {
		return attribut_d_;
	}
	
	public void setAttribut_d(String attribut_d) {
		attribut_d_ = attribut_d;
	}
	
	public String getValue_d() {
		return value_d_;
	}
	
	public void setValue_d(String value_d) {
		value_d_ = value_d;
	}

	public Integer getOrderNumber() {
		return orderNumber_;
	}
	
	public void setOrderNumber(Integer orderNumber) {
		orderNumber_ = orderNumber;
	}
	
	public Integer getValue_group_id() {
		return value_group_id_;
	}

	public void setValue_group_id(Integer value_group_id) {
		value_group_id_ = value_group_id;
	}

	public String getImageFileName() {
		return imageFileName_;
	}

	public void setImageFileName(String imageFileName) {
		imageFileName_ = imageFileName;
	}

	public String getImageCopyright() {
		return imageCopyright_;
	}

	public void setImageCopyright(String imageCopyright) {
		imageCopyright_ = imageCopyright;
	}

	public Integer getDataType() {
		return dataType_;
	}

	public void setDataType(Integer dataType) {
		dataType_ = dataType;
	}

	public String getDataTypeName() {
		return dataTypeName_;
	}

	public void setDataTypeName(String dataTypeName) {
		dataTypeName_ = dataTypeName;
	}

	public String getFormatPattern() {
		return formatPattern_;
	}

	public void setFormatPattern(String formatPattern) {
		formatPattern_ = formatPattern;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ValueRecord) {
			ValueRecord other = (ValueRecord)obj;
			if ((!other.getValue().equals("") && !other.getAttribut_e().equals("")) 
					&& (other.getValue().equals(this.value_) && other.getAttribut_e().equals(this.attribut_e_))) {
				return true;
			}
			if ((!other.getValue_d().equals("") && !other.getAttribut_d().equals(""))
					&& (other.getValue_d().equals(this.value_d_) && other.getAttribut_d().equals(this.attribut_d_))) {
				return true;
			}
		}
		return false;
	}

	private Integer valueID_;
	private String attribut_e_;
	private String value_;
	private String attribut_d_;
	private String value_d_;
	private Integer orderNumber_;
	private Integer value_group_id_ = 0;
	private String imageFileName_;
	private String imageCopyright_;
	private Integer dataType_;
	private String dataTypeName_;
	private String formatPattern_;
	
	
}

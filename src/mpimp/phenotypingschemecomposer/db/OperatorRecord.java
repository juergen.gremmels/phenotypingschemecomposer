/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db;

public class OperatorRecord {

	public Integer getOperatorId() {
		return operatorId_;
	}

	public void setOperatorId(Integer operatorId) {
		operatorId_ = operatorId;
	}

	public Integer getRoleId() {
		return roleId_;
	}

	public void setRoleId(Integer roleId) {
		roleId_ = roleId;
	}

	public String getRoleName() {
		return roleName_;
	}

	public void setRoleName(String roleName) {
		roleName_ = roleName;
	}

	public String getLogin() {
		return login_;
	}

	public void setLogin(String login) {
		login_ = login;
	}

	public String getPassword() {
		return password_;
	}

	public void setPassword(String password) {
		password_ = password;
	}

	public String getFirstName() {
		return firstName_;
	}

	public void setFirstName(String firstName) {
		firstName_ = firstName;
	}

	public String getLastName() {
		return lastName_;
	}

	public void setLastName(String lastName) {
		lastName_ = lastName;
	}

	public Boolean getAllowLogin() {
		return allowLogin_;
	}

	public void setAllowLogin(Boolean allowLogin) {
		allowLogin_ = allowLogin;
	}

	public Boolean getIsAdministrator() {
		return isAdministrator_;
	}

	public void setIsAdministrator(Boolean isAdministrator) {
		isAdministrator_ = isAdministrator;
	}

//	public ArrayList<RoleRecord> getRoles() {
//		return roles_;
//	}
//
//	public void setRoles(ArrayList<RoleRecord> roles) {
//		roles_ = roles;
//	}

	private Integer operatorId_;
	private String login_;
	private String password_;
	private String firstName_;
	private String lastName_;
	private Boolean allowLogin_;
	private Integer roleId_;
	private Boolean isAdministrator_;
	private String roleName_;
	//private ArrayList<RoleRecord> roles_;
}

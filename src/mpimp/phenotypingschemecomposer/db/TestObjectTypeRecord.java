/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db;

public class TestObjectTypeRecord extends TPCDataRecord {

	public Integer getTestObjectType_id() {
		return testObjectType_id_;
	}
	
	public void setTestObjectType_id(Integer testObjectType_id) {
		testObjectType_id_ = testObjectType_id;
	}
	
	public String getName() {
		return name_;
	}
	
	public void setName(String name) {
		name_ = name;
	}
	
	public String getNameD() {
		return nameD_;
	}
	
	public void setNameD(String nameD) {
		nameD_ = nameD;
	}
	
	private Integer testObjectType_id_;
	private String name_;
	private String nameD_;
	
}

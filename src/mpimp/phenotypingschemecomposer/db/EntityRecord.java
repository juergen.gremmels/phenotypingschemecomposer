/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.db;

public class EntityRecord extends TPCDataRecord {

	public Integer getOrganID() {
		return organID_;
	}
	public void setOrganID(Integer organID) {
		organID_ = organID;
	}
	public String getParameter() {
		return parameter_;
	}
	public void setParameter(String parameter) {
		parameter_ = parameter;
	}
	
	public String getParameterD() {
		return parameterD_;
	}
	public void setParameterD(String parameterD) {
		parameterD_ = parameterD;
	}

	public String getDefinition() {
		return definition_;
	}
	public void setDefinition(String definition) {
		definition_ = definition;
	}
	public String getpO_id() {
		return pO_id_;
	}
	public void setpO_id(String pO_id) {
		pO_id_ = pO_id;
	}
	public Integer getProjectEntityID() {
		return projectEntityID;
	}
	public void setProjectEntityID(Integer projectEntityID) {
		this.projectEntityID = projectEntityID;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof EntityRecord) {
			EntityRecord other = (EntityRecord)obj;
			if (other.getOrganID().equals(this.organID_)) {
				return true;
			}
		}
		return false;
	}

	private Integer organID_;
	private String parameter_;
	private String parameterD_;
	private String definition_;
	private String pO_id_;
	private Integer projectEntityID = 0;
	
}

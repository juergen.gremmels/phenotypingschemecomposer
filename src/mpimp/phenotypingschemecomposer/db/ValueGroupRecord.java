package mpimp.phenotypingschemecomposer.db;

public class ValueGroupRecord extends TPCDataRecord {

	public Integer getValueGroup_id() {
		return valueGroup_id_;
	}
	
	public void setValueGroup_id(Integer valueGroup_id) {
		valueGroup_id_ = valueGroup_id;
	}
	
	public String getNameE() {
		return nameE_;
	}
	
	public void setNameE(String nameE) {
		nameE_ = nameE;
	}
	
	public String getNameD() {
		return nameD_;
	}

	public void setNameD(String nameD) {
		nameD_ = nameD;
	}

	private Integer valueGroup_id_;
	private String nameE_;
	private String nameD_;
	
}

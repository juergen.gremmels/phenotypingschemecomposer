/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;

public class Encrypter {

	public static String encrypt(String clearTextString) throws Exception {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE,
				loadKeyFromFile(TPCProperties.getInstance().getProperty("keyFilePath")));
		String encryptedString = "";
		// Encode the string into bytes using utf-8
		byte[] utf8 = clearTextString.getBytes("UTF8");

		// Encrypt
		byte[] enc = cipher.doFinal(utf8);

		// Encode bytes to base64 to get a string
		encryptedString = Base64.encodeBase64String(enc);
		return encryptedString;
	}

	public static String decrypt(String encryptedString) throws Exception {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE,
				loadKeyFromFile(TPCProperties.getInstance().getProperty("keyFilePath")));
		String decryptedString = "";
		// Decode base64 to get bytes
		byte[] dec = Base64.decodeBase64(encryptedString);

		// Decrypt
		byte[] utf8 = cipher.doFinal(dec);

		// Decode using utf-8
		decryptedString = new String(utf8, "UTF8");
		return decryptedString;
	}

	private static SecretKey loadKeyFromFile(String keyfileName) throws Exception {
		ObjectInputStream keyIn;
		SecretKey key = null;
		keyIn = new ObjectInputStream(new FileInputStream(keyfileName));
		key = (SecretKey) keyIn.readObject();
		keyIn.close();
		return key;
	}

}

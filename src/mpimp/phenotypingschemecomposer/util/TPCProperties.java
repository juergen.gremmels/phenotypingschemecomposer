/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;

public class TPCProperties extends Properties {

	private static final long serialVersionUID = -6615161057815469936L;
	
	public static void setPropertiesPath(String propertiesPath) {
		TPCProperties.propertiesPath_ = propertiesPath;
	}

	private TPCProperties(String propertiesFilePath) {
		this.loadProperties(propertiesFilePath);
	}

	public static void reloadProperties() {
		tpcProperties_ = new TPCProperties(propertiesPath_);
	}
	
	public static TPCProperties getInstance() {
		if (tpcProperties_ == null) {
			tpcProperties_ = new TPCProperties(propertiesPath_);
		}
		return tpcProperties_;
	}
	
	private void loadProperties(String propertiesFilePath) {
		FileInputStream fis;
		try {
			fis = new FileInputStream(propertiesFilePath);
			loadFromXML(fis);
			propertiesLoaded_ = true;
		} catch (InvalidPropertiesFormatException e) {
			propertiesLoaded_ = false;
			e.printStackTrace();
		} catch (IOException e) {
			propertiesLoaded_ = false;
			e.printStackTrace();
		}
	}

	public static Boolean getPropertiesLoaded_() {
		return propertiesLoaded_;
	}

	private static String propertiesPath_;
	private static TPCProperties tpcProperties_ = null;
	private static Boolean propertiesLoaded_ = false;
}

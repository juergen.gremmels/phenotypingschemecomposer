/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.util.Set;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLUtilities {

	public static Node getSingleNodeByTagName(Element parent, String tagName) {
		if (parent != null && parent.hasChildNodes()) {
			NodeList childNodes = parent.getElementsByTagName(tagName);
			if (childNodes != null && childNodes.getLength() > 0) {
				return childNodes.item(0);
			}
		}
		return null;
	}

	public static Node getSingleNodeByTagName(Node parent, String tagName) {
		if (parent != null && parent instanceof Element
				&& parent.hasChildNodes()) {
			Element parentElement = (Element) parent;
			NodeList childNodes = parentElement.getElementsByTagName(tagName);
			if (childNodes != null && childNodes.getLength() > 0) {
				return childNodes.item(0);
			}
		}
		return null;
	}

	public static NodeList getNodeListByTagName(Element parent, String tagName) {
		if (parent != null && parent.hasChildNodes()) {
			NodeList childNodes = parent.getElementsByTagName(tagName);
			return childNodes;
		}
		return null;
	}

	public static NodeList getNodeListByTagName(Node parent, String tagName) {
		if (parent != null && parent instanceof Element
				&& parent.hasChildNodes()) {
			Element parentElement = (Element) parent;
			NodeList childNodes = parentElement.getElementsByTagName(tagName);
			return childNodes;
		}
		return null;
	}

	public static Node getSingleNodeByTagNameList(Node parent,
			Set<String> tagNames) {
		for (String tagName : tagNames) {
			Node childNode = getSingleNodeByTagName(parent, tagName);
			if (childNode != null) {
				return childNode;
			}
		}
		return null;
	}

	public static NodeList getNodeListByTagNameList(Node parent,
			Set<String> tagNames) {
		for (String tagName : tagNames) {
			NodeList childNodes = getNodeListByTagName(parent, tagName);
			if (childNodes != null && childNodes.getLength() > 0) {
				return childNodes;
			}
		}
		return null;
	}

	public static Attr getAttributeNodeByName(Node parent, String attrName) {
		if (parent != null && parent.hasAttributes()) {
			NamedNodeMap attributes = parent.getAttributes();
			Node attributeNode = attributes.getNamedItem(attrName);
			if (attributeNode != null) {
				Attr attribute = (Attr) attributeNode;
				return attribute;
			}
		}
		return null;
	}

	public static Element createTag(Document doc, String tagName) {
		return doc.createElement(tagName);
	}

	public static Element createTagWithText(Document doc,
			String tagName, String textContent) {
		Element element = createTag(doc, tagName);
		element.setTextContent(textContent);
		return element;
	}

	public static Element createTagWithAttribute(Document doc,
			String tagNamePropertyKey, String attributeName,
			String attributeValue) {
		Element element = createTag(doc, tagNamePropertyKey);
		element.setAttribute(attributeName, attributeValue);
		return element;
	}
}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import mpimp.phenotypingschemecomposer.db.EntityRecord;
import mpimp.phenotypingschemecomposer.db.TPCDataRecord;
import mpimp.phenotypingschemecomposer.db.TPCModel;

public class EntityRecordComparator extends TPCBasicComparator {

	public EntityRecordComparator(TPCModel tpcModel) {
		super(tpcModel);
	}

	protected int doComparation(TPCDataRecord firstRecord,
			TPCDataRecord secondRecord) {
		int ret = 0;
		if (firstRecord != null && secondRecord != null) {
			if (firstRecord instanceof EntityRecord
					&& secondRecord instanceof EntityRecord) {
				if (tpcModel_.getCurrentLocaleString().equals("de_DE")) {
					return doStandardComparation(
							((EntityRecord) firstRecord).getParameterD(),
							((EntityRecord) secondRecord).getParameterD());
				} else {
					return doStandardComparation(
							((EntityRecord) firstRecord).getParameter(),
							((EntityRecord) secondRecord).getParameter());
				}
			}
		} else if (firstRecord == null && secondRecord != null) {
			ret = -1;
		} else if (firstRecord != null && secondRecord == null) {
			ret = 1;
		}
		return ret;
	}

}

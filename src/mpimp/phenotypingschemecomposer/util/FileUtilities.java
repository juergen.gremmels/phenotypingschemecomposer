/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import mpimp.phenotypingschemecomposer.db.TPCModel;

public class FileUtilities {

	public static void removeDirRecursively(String dirPath) throws Exception {
		File dir = new File(dirPath);
		File[] content = dir.listFiles();
		if (content == null) {
			String message = "Could not delete file/directory '"
					+ dir.getAbsolutePath() + "'.";
			Exception e = new Exception(message);
			throw e;
		}
		for (int i = 0; i < content.length; i++) {
			if (content[i].isDirectory()) {
				removeDirRecursively(content[i].getAbsolutePath());
			} else {
				if (!content[i].delete()) {
					String message = "Could not delete file '"
							+ dir.getAbsolutePath() + "'.";
					Exception e = new Exception(message);
					throw e;
				}
			}
		}
		if (!dir.delete()) {
			String message = "Could not delete file/directory '"
					+ dir.getAbsolutePath() + "'.";
			Exception e = new Exception(message);
			throw e;
		}
	}

	public static void copyFile(String sourcePath, String destinationPath)
			throws Exception {
		File sourceFile = new File(sourcePath);
		File destinationFile = new File(destinationPath);
		destinationFile.setWritable(true);
		copyFile(sourceFile, destinationFile);
	}

	public static void copyFile(File sourceFile, File destinationFile)
			throws Exception {
		FileInputStream in = null;
		FileOutputStream out = null;
		try {
			in = new FileInputStream(sourceFile);
			out = new FileOutputStream(destinationFile);
			int bytes;
			while ((bytes = in.read()) != -1) {
				out.write(bytes);
			}
		} finally {
			if (in != null) {
				in.close();
			}
			if (out != null) {
				out.close();
			}
		}
	}

	public static String createFilePath(String fileName, String directoryPath) {
		return directoryPath + File.separator + fileName;
	}

	public static String extractExtension(String fileName) {
		String extension = "";
		String[] partsOfFileName = fileName.split("\\.");
		int length = partsOfFileName.length;
		if (length > 1) {
			extension = partsOfFileName[length - 1];
		}
		return extension;
	}

	public static String createImageFileName(TPCModel tpcModel) {
		String extension = extractExtension(tpcModel.getCurrentImageFileFileName());
		String imageFileName = tpcModel.getCurrentValueForAdminID() + "_"
				+ tpcModel.getCurrentValueForAdminAttribut_e() + "_"
				+ tpcModel.getCurrentValueForAdminValue() + "." + extension;
		imageFileName = imageFileName.replace(" ", "_");
		imageFileName = imageFileName.replace("%", "pc");
		imageFileName = imageFileName.replace("<", "lt");
		imageFileName = imageFileName.replace(">", "gt");
		return imageFileName;
	}
	
	public static String customizeSeparators(String path) {
		String newPath = path;
		String currentSeparator = File.separator;
		newPath = path.replace("/", currentSeparator);
		newPath = path.replace("\\", currentSeparator);
		return newPath;
	}

	public static String customizeSeparatorsForURL(String path) {
		return path.replace("\\", "/");
	}
}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.db.TestProgramEntityRecord;
import mpimp.phenotypingschemecomposer.db.ValueRecord;

public class FileDownloadHelper {

	private static final long serialVersionUID = 2331674791110260812L;

	public static void zipFileForDownload(String downloadPath, TPCModel tpcModel)
			throws Exception {

		String targetFileNameWithExtension = createZIPFilePath(tpcModel, downloadPath);
		File tempFile = new File(createXMLFilePath(tpcModel, downloadPath));

		int BUFFER = 2048;
		byte data[] = new byte[BUFFER];

		FileInputStream in = new FileInputStream(tempFile.getAbsolutePath());
		BufferedInputStream origin = new BufferedInputStream(in, BUFFER);
		ZipEntry zipEntry = new ZipEntry(tempFile.getName());
		FileOutputStream dest = new FileOutputStream(targetFileNameWithExtension);
		ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest));
		out.putNextEntry(zipEntry);
		int count;
		while ((count = origin.read(data, 0, BUFFER)) != -1) {
			out.write(data, 0, count);
		}
		origin.close();
		in.close();
		out.close();
	}

	public static boolean createImagePackage(String downloadPath,
			TPCModel tpcModel) throws Exception {
		// collect image file names for current test program
		ArrayList<TestProgramEntityRecord> tpeRecords = tpcModel
				.getTestProgramEntityRecords();
		String imageList = "";
		String imageDirectoryPath = TPCProperties.getInstance().getProperty(
				"imageDirectoryPath");
		for (TestProgramEntityRecord currentTPERecord : tpeRecords) {
			ValueRecord currentValueRecord = tpcModel
					.getValueRecordForId(currentTPERecord.getValueID());
			String imageFileName = currentValueRecord.getImageFileName();
			if (imageFileName != null && !imageFileName.equals("")) {
				String imageFilePath = imageDirectoryPath + File.separator
						+ imageFileName;
				imageList += imageFilePath + " ";
			}
		}
		if (!imageList.equals("")) {
			Runtime runtime = Runtime.getRuntime();
			String command = TPCProperties.getInstance().getProperty(
					"zipProgramPath");
			String archiveName = "\"" + downloadPath + File.separator
					+ tpcModel.getCurrentTestProgramName()
					+ TPCProperties.getInstance().getProperty("zipExtension")
					+ "\"";
			command = String.format(command, archiveName, imageList);
			command = command.replace("\\\\", "\\");
			Process process = runtime.exec(command);
			int result = process.waitFor();
			if (result != 0) {
				String message = "Error while creating zip archive "
						+ archiveName + "\\nCommand: " + command
						+ "\\nCurrent user: " + System.getProperty("user.name");
				throw new Exception(message);
			}
			return true;
		} else {
			return false;
		}
	}

	public static void createDownloadTempDir(String downloadTempDirPath)
			throws Exception {
		File attachmentTempDir = new File(downloadTempDirPath);
		if (attachmentTempDir.exists()) {
			FileUtilities.removeDirRecursively(downloadTempDirPath);
		}
		if (!attachmentTempDir.mkdirs()) {
			String errorMessage = "Could not create directory "
					+ downloadTempDirPath;
			throw new Exception(errorMessage);
		}
		attachmentTempDir.setWritable(true,false);
	}

	public static String createXMLFilePath(TPCModel tpcModel, String dirPath)
			throws Exception {
		return dirPath + File.separator + createXMLFileName(tpcModel);
	}

	public static String createZIPFilePath(TPCModel tpcModel, String dirPath)
			throws Exception {
		return createXMLFilePath(tpcModel, dirPath)
				+ TPCProperties.getInstance().getProperty("zipExtension");
	}

	public static String createImagePackagePath(TPCModel tpcModel,
			String dirPath) throws Exception {
		return dirPath + File.separator + tpcModel.getCurrentTestProgramName()
				+ TPCProperties.getInstance().getProperty("zipExtension");
	}

	public static String createXMLFileName(TPCModel tpcModel) throws Exception {
		String fileName = tpcModel.getCurrentTestProgramName();
		return replaceGermanUmlauts(fileName)
				+ TPCProperties.getInstance().getProperty("xmlExtension");
	}

	public static void deleteAttachmentTempDir(String attachmentTempDirPath)
			throws Exception {
		if (new File(attachmentTempDirPath).exists()) {
			FileUtilities.removeDirRecursively(attachmentTempDirPath);
		}
	}

	public static String replaceGermanUmlauts(String inputString) {
		String output = inputString;
		output = output.replace("�", "ae");
		output = output.replace("�", "oe");
		output = output.replace("�", "ue");
		output = output.replace("�", "Ae");
		output = output.replace("�", "Oe");
		output = output.replace("�", "Ue");
		return output;
	}

}
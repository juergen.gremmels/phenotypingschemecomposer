/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import mpimp.phenotypingschemecomposer.db.TPCModel;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class TPCXMLErrorHandler implements ErrorHandler {

	public TPCXMLErrorHandler(TPCModel tpcModel) {
		tpcModel_ = tpcModel;
	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		addMessageToTPCModel("Warning", exception);
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		addMessageToTPCModel("Error", exception);
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		addMessageToTPCModel("Fatal error", exception);
	}

	private void addMessageToTPCModel(String messageType, SAXParseException exception) {
		String message = "%s (line %d, column %d): %s";
		message = String.format(message, messageType,
				exception.getLineNumber(), exception.getColumnNumber(),
				exception.getMessage());
		String storedMessage = tpcModel_.getXmlReadingErrors();
		if (!storedMessage.equals("")) {
			storedMessage += "\\n";
		}
		storedMessage += message;
		tpcModel_.setXmlReadingErrors(storedMessage);
	}

	private TPCModel tpcModel_;

}

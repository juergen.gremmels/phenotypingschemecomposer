/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.io.PrintWriter;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.Result;
import com.opensymphony.xwork2.util.ValueStack;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

public class JSONResult implements Result {

	private static final long serialVersionUID = 2355853660396171898L;

	public static final String DEFAULT_PARAM = "classAlias";
	
	String classAlias_;
	
	public String getClassAlias() {
		return classAlias_;
	}

	public void setClassAlias(String classAlias) {
		classAlias_ = classAlias;
	}

	@Override
	public void execute(ActionInvocation invocation) throws Exception {
		
		ServletActionContext.getResponse().setContentType("text/plain");
		PrintWriter responseStream = ServletActionContext.getResponse().getWriter();
		
		ValueStack valueStack = invocation.getStack();
		Object jsonModel = valueStack.findValue("jsonModel");
		
		XStream xstream = new XStream(new JettisonMappedXmlDriver());
		
		if (classAlias_ == null) {
			classAlias_ = "object";
		}
		if (jsonModel != null) {
			xstream.alias(classAlias_, jsonModel.getClass());
			responseStream.println(xstream.toXML(jsonModel));
		} 
	}

}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import mpimp.phenotypingschemecomposer.db.EntityRecord;
import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.db.TestProgramEntityRecord;
import mpimp.phenotypingschemecomposer.db.ValueRecord;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLWriter {

	// <?xml version="1.0" encoding="utf-16"?>
	// <TESTPROGRAM ID="" NAME="" VERSION="" OBJECT="" DESCRIPTION=""
	// OBJECT_TYPE="">
	// <PARAMETER ID="" NAME_E="" NAME_D="">
	// <ATTRIBUTE ID="" NAME_E="" NAME_D="" ORDER_NUMBER="">
	// <VALUE NAME_E="" NAME_D="" IMAGE_FILE="" />
	// </ATTRIBUTE>
	// <VALUE_GROUP ID="">
	// <ATTRIBUTE ID="" NAME_E="" NAME_D="" ORDER_NUMBER="">
	// <VALUE NAME_E="" NAME_D="" IMAGE_FILE="" />
	// </VALUE_GROUP>
	// </PARAMETER>
	// </TESTPROGRAM>

	public static String writeXML(TPCModel tpcModel, String downloadTempDir) throws Exception {

		HashMap<EntityRecord, ArrayList<ValueRecord>> dataMap = arrangeData(tpcModel);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.newDocument();

		Element testProgram = XMLUtilities.createTag(doc, XMLConstants.tagNameTestProgram);

		testProgram.setAttribute(XMLConstants.attributeNameNameSpace, XMLConstants.attributeValueNameSpace);

		testProgram.setAttribute(XMLConstants.attributeNameID, tpcModel.getCurrentTestProgramID().toString());
		testProgram.setAttribute(XMLConstants.attributeNameName, tpcModel.getCurrentTestProgramName());
		testProgram.setAttribute(XMLConstants.attributeNameVersion, tpcModel.getCurrentTestProgramVersion());
		testProgram.setAttribute(XMLConstants.attributeNameObject, tpcModel.getCurrentTestProgramMessobjekt());
		testProgram.setAttribute(XMLConstants.attributeNameDescription, tpcModel.getCurrentTestProgramDescription());
		testProgram.setAttribute(XMLConstants.attributeNameObjectType, tpcModel.getCurrentTestObjectType().toString());

		doc.appendChild(testProgram);

		Set<EntityRecord> entityRecords = dataMap.keySet();
		for (EntityRecord currentEntityRecord : entityRecords) {
			Element parameterTag = XMLUtilities.createTag(doc, XMLConstants.tagNameParameter);
			parameterTag.setAttribute(XMLConstants.attributeNameID, currentEntityRecord.getOrganID().toString());
			parameterTag.setAttribute(XMLConstants.attributeNameName_E, currentEntityRecord.getParameter());
			parameterTag.setAttribute(XMLConstants.attributeNameName_D, currentEntityRecord.getParameterD());

			List<ValueRecord> valueRecords = dataMap.get(currentEntityRecord);
			HashMap<Integer, List<ValueRecord>> valueGroupMap = new HashMap<Integer, List<ValueRecord>>();
			for (ValueRecord currentValueRecord : valueRecords) {

				if (currentValueRecord.getValue_group_id() == null) {
					writeAttributeValueSection(doc, parameterTag, currentValueRecord, tpcModel);
				} else {
					// fill value group map first
					if (valueGroupMap.containsKey(currentValueRecord.getValue_group_id())) {
						valueGroupMap.get(currentValueRecord.getValue_group_id()).add(currentValueRecord);
					} else {
						List<ValueRecord> recordsForGroup = new ArrayList<ValueRecord>();
						recordsForGroup.add(currentValueRecord);
						valueGroupMap.put(currentValueRecord.getValue_group_id(), recordsForGroup);
					}
				}
			}
			// and write value groups - if there are any
			Set<Integer> valueGroupIds = valueGroupMap.keySet();
			for (Integer currentValueGroupId : valueGroupIds) {
				List<ValueRecord> recordsOfValueGroup = valueGroupMap.get(currentValueGroupId);
				writeAttributeCategoricalValueSection(doc, parameterTag, recordsOfValueGroup);
			}
			testProgram.appendChild(parameterTag);
		}
		String filePath = FileDownloadHelper.createXMLFilePath(tpcModel, downloadTempDir);
		writeDocumentToFile(doc, filePath);
		return filePath;
	}

	private static void writeAttributeValueSection(Document doc, Element parameterTag, ValueRecord currentValueRecord, TPCModel tpcModel) {
		Element attributeTag = XMLUtilities.createTag(doc, XMLConstants.tagNameAttribute);
		attributeTag.setAttribute(XMLConstants.attributeNameID, currentValueRecord.getValueID().toString());
		attributeTag.setAttribute(XMLConstants.attributeNameName_E, currentValueRecord.getAttribut_e());
		attributeTag.setAttribute(XMLConstants.attributeNameName_D, currentValueRecord.getAttribut_d());
		Integer orderNumberInteger = currentValueRecord.getOrderNumber();
		if (orderNumberInteger != null) {
			attributeTag.setAttribute(XMLConstants.attributeNameOrderNumber, orderNumberInteger.toString());
		} else {
			attributeTag.setAttribute(XMLConstants.attributeNameOrderNumber, "0");
		}

		Element valueTag = XMLUtilities.createTag(doc, XMLConstants.tagNameValue);
		valueTag.setAttribute(XMLConstants.attributeNameName_E, currentValueRecord.getValue());
		valueTag.setAttribute(XMLConstants.attributeNameName_D, currentValueRecord.getValue_d());
		valueTag.setAttribute(XMLConstants.attributeNameImageFileName, currentValueRecord.getImageFileName());
		valueTag.setAttribute(XMLConstants.attributeNameDataType, tpcModel.getDataTypeNameForID(currentValueRecord.getDataType()));
		valueTag.setAttribute(XMLConstants.attributeNameFormatPattern, currentValueRecord.getFormatPattern());

		attributeTag.appendChild(valueTag);

		parameterTag.appendChild(attributeTag);
	}

	private static void writeAttributeCategoricalValueSection(Document doc, Element parameterTag,
			List<ValueRecord> valueRecords) {
		String attributeID = "";
		String attributeName_E = "";
		String attributeName_D = "";
		Integer orderNumberInteger = 0;
		if (valueRecords.size() >= 1) {
			attributeID = valueRecords.get(0).getValueID().toString();
			attributeName_E = valueRecords.get(0).getAttribut_e();
			attributeName_D = valueRecords.get(0).getAttribut_d();
			orderNumberInteger = valueRecords.get(0).getOrderNumber();
		} else {
			// TODO: error handling
			return;
		}
		Element attributeCategoricalTag = XMLUtilities.createTag(doc, XMLConstants.tagNameAttributeCategorical);
		attributeCategoricalTag.setAttribute(XMLConstants.attributeNameID, attributeID);
		attributeCategoricalTag.setAttribute(XMLConstants.attributeNameName_E, attributeName_E);
		attributeCategoricalTag.setAttribute(XMLConstants.attributeNameName_D, attributeName_D);
		if (orderNumberInteger != null) {
			attributeCategoricalTag.setAttribute(XMLConstants.attributeNameOrderNumber, orderNumberInteger.toString());
		} else {
			attributeCategoricalTag.setAttribute(XMLConstants.attributeNameOrderNumber, "0");
		}

		for (ValueRecord currentValueRecord : valueRecords) {
			Element valueTag = XMLUtilities.createTag(doc, XMLConstants.tagNameValue);
			valueTag.setAttribute(XMLConstants.attributeNameName_E, currentValueRecord.getValue());
			valueTag.setAttribute(XMLConstants.attributeNameName_D, currentValueRecord.getValue_d());
			valueTag.setAttribute(XMLConstants.attributeNameImageFileName, currentValueRecord.getImageFileName());
			Integer valueOrderNumberInteger = currentValueRecord.getOrderNumber();
			if (valueOrderNumberInteger != null) {
				valueTag.setAttribute(XMLConstants.attributeNameOrderNumber, valueOrderNumberInteger.toString());	
			} else {
				valueTag.setAttribute(XMLConstants.attributeNameOrderNumber, "0");
			}
			
			attributeCategoricalTag.appendChild(valueTag);

			parameterTag.appendChild(attributeCategoricalTag);
		}
	}

	private static void writeDocumentToFile(Document doc, String path) throws Exception {
		FileOutputStream fos = new FileOutputStream(path);
		Transformer transformer = TransformerFactory.newInstance().newTransformer();
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-16");
		transformer.transform(new DOMSource(doc), new StreamResult(fos));
		fos.close();
	}

	private static HashMap<EntityRecord, ArrayList<ValueRecord>> arrangeData(TPCModel tpcModel) {
		HashMap<EntityRecord, ArrayList<ValueRecord>> dataMap = new HashMap<EntityRecord, ArrayList<ValueRecord>>();
		ArrayList<TestProgramEntityRecord> tpeRecords = tpcModel.getTestProgramEntityRecords();
		for (TestProgramEntityRecord currentTPERecord : tpeRecords) {
			EntityRecord currentEntityRecord = tpcModel.getEntityRecordForId(currentTPERecord.getEntityID());
			ValueRecord currentValueRecord = tpcModel.getValueRecordForId(currentTPERecord.getValueID());
			currentValueRecord.setOrderNumber(currentTPERecord.getOrderNumber());
			if (dataMap.containsKey(currentEntityRecord)) {
				dataMap.get(currentEntityRecord).add(currentValueRecord);
			} else {
				ArrayList<ValueRecord> valueRecords = new ArrayList<ValueRecord>();
				valueRecords.add(currentValueRecord);
				dataMap.put(currentEntityRecord, valueRecords);
			}
		}
		return dataMap;
	}

}

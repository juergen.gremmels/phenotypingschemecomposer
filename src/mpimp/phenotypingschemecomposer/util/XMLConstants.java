package mpimp.phenotypingschemecomposer.util;

public class XMLConstants {
	public static final String tagNameTestProgram = "TESTPROGRAM";
	public static final String tagNameParameter = "PARAMETER";
	public static final String tagNameAttribute = "ATTRIBUTE";
	public static final String tagNameAttributeCategorical = "ATTRIBUTE_CATEGORICAL";
	public static final String tagNameValue = "VALUE";
	public static final String tagNameValueCategorical = "VALUE_CATEGORICAL";
	public static final String attributeNameID = "ID";
	public static final String attributeNameName = "NAME";
	public static final String attributeNameVersion = "VERSION";
	public static final String attributeNameObject = "OBJECT";
	public static final String attributeNameDescription = "DESCRIPTION";
	public static final String attributeNameObjectType = "OBJECT_TYPE";
	public static final String attributeNameName_E = "NAME_E";
	public static final String attributeNameName_D = "NAME_D";
	public static final String attributeNameOrderNumber = "ORDER_NUMBER";
	public static final String attributeNameImageFileName = "IMAGE_FILE";
	public static final String attributeNameNameSpace = "xmlns";
	public static final String attributeValueNameSpace = "http://tempuri.org/TestProgram_2.0.xsd";
	public static final String attributeNameDataType = "DATA_TYPE";
	public static final String attributeNameFormatPattern = "FORMAT_PATTERN";
}

/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.util.Comparator;

import mpimp.phenotypingschemecomposer.db.TPCDataRecord;
import mpimp.phenotypingschemecomposer.db.TPCModel;

public abstract class TPCBasicComparator implements Comparator<TPCDataRecord> {

	public TPCBasicComparator(TPCModel tpcModel) {
		tpcModel_ = tpcModel;
	}

	public TPCBasicComparator(TPCModel tpcModel, Boolean sortAscending) {
		tpcModel_ = tpcModel;
		sortAscending_ = sortAscending;
	}

	@Override
	public int compare(TPCDataRecord firstRecord, TPCDataRecord secondRecord) {
		if (sortAscending_) {
			return doComparation(firstRecord, secondRecord);
		} else {
			return -doComparation(firstRecord, secondRecord);
		}
	}

	protected abstract int doComparation(TPCDataRecord firstRecord,
			TPCDataRecord secondRecord);

	protected int doStandardComparation(Object firstItem, Object secondItem) {
		int ret = 0;
		if (firstItem != null && secondItem != null) {
			if (firstItem instanceof String && secondItem instanceof String) {
				ret = doStringComparation((String) firstItem,
						(String) secondItem);
			} else if (firstItem instanceof Integer
					&& secondItem instanceof Integer) {
				ret = doIntegerComparation((Integer) firstItem,
						(Integer) secondItem);
			} else if (firstItem instanceof Double
					&& secondItem instanceof Double) {
				ret = doDoubleComparation((Double) firstItem,
						(Double) secondItem);
			}
		} else if (firstItem == null && secondItem != null) {
			ret = -1;
		} else if (firstItem != null && secondItem == null) {
			ret = 1;
		}
		return ret;
	}

	/*
	 * Invokes String.compareToIgnoreCase(String, String) for non-empty strings. 
	 * An empty string is regarded as smaller than any non-empty string.
	 */
	protected int doStringComparation(String firstItem, String secondItem) {
		if (firstItem.equalsIgnoreCase("") && secondItem.equalsIgnoreCase("")) {
			return 0;
		} else if (firstItem.equalsIgnoreCase("")
				&& !secondItem.equalsIgnoreCase("")) {
			return 1;
		} else if (!firstItem.equalsIgnoreCase("")
				&& secondItem.equalsIgnoreCase("")) {
			return -1;
		} else {
			return firstItem.compareToIgnoreCase(secondItem);
		}
	}

	protected int doIntegerComparation(Integer firstItem, Integer secondItem) {
		return firstItem.compareTo(secondItem);
	}

	protected int doDoubleComparation(Double firstItem, Double secondItem) {
		return firstItem.compareTo(secondItem);
	}

	protected Boolean sortAscending_ = true;
	protected TPCModel tpcModel_;
}

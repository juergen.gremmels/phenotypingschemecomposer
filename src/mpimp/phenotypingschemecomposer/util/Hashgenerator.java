/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Hashgenerator {
	
	public static String sha1(String input, String securitySalt) throws NoSuchAlgorithmException {
		input = securitySalt + input;
		MessageDigest mDigest = MessageDigest.getInstance("SHA-1");
		byte[] result = mDigest.digest(input.getBytes());
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < result.length; i++) {
			String part = Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1);
			sb.append(part);
		}
		return sb.toString();
	}
	
	public static void main(String[] args) {
		if (args.length == 2) {
			try {
				String hashedString = Hashgenerator.sha1(args[0], args[1]);
				System.out.println("Clear text: " + args[0] + "\nKey: " + args[1] + "\nHashed text: " + hashedString);
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		} else if (args.length == 3) {
			try {
				String hashedString = Hashgenerator.sha1(args[0], args[1]);
				File output = new File(args[2]);
				BufferedWriter bw = new BufferedWriter(new FileWriter(output));
				bw.write(hashedString);
				bw.close();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} 
		} else {
			System.out.println("Usage:\n Hashgenerator <clear text string> <key> [<output file>]");
		}
	}
}

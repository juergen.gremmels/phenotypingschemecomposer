/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

package mpimp.phenotypingschemecomposer.util;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import mpimp.phenotypingschemecomposer.db.TPCModel;
import mpimp.phenotypingschemecomposer.db.TestProgramEntityRecord;

public class XMLReader {

	// <?xml version="1.0" encoding="utf-16"?>
	// <TESTPROGRAM ID="" NAME="" VERSION="" OBJECT="" DESCRIPTION=""
	// OBJECT_TYPE="">
	// <PARAMETER ID="" NAME_E="" NAME_D="">
	// <ATTRIBUTE ID="" NAME_E="" NAME_D="" ORDER_NUMBER="">
	// <VALUE NAME_E="" NAME_D="" IMAGE_FILE="" />
	// </ATTRIBUTE>
	// <VALUE_GROUP ID="">
	// <ATTRIBUTE ID="" NAME_E="" NAME_D="" ORDER_NUMBER="">
	// <VALUE NAME_E="" NAME_D="" IMAGE_FILE="" />
	// </VALUE_GROUP>
	// </PARAMETER>
	// </TESTPROGRAM>

	public static void readTestProgramFile(TPCModel tpcModel) throws Exception {

		File xmlFile = null;
		try {
			xmlFile = new File(tpcModel.getSourceFile());
		} catch (Exception e) {
			String message = "Can not open file %s. Reason: %s";
			message = String.format(message, tpcModel.getSourceFile(), e.getMessage());
			throw new Exception(message);
		}

		String webApplicationParentPath = TPCProperties.getInstance().getProperty("webApplicationParentPath");

		String schemaPath = "file:///" + webApplicationParentPath + File.separator + "TestProgramComposer"
				+ File.separator + "WEB-INF" + File.separator + "classes" + File.separator + "Testprogram_2.0.xsd";

		URL urlSchema = new URL(schemaPath);
		Schema testProgramSchema = SchemaFactory.newInstance(javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(urlSchema);

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		factory.setValidating(false);
		factory.setSchema(testProgramSchema);

		DocumentBuilder builder = null;
		builder = factory.newDocumentBuilder();
		tpcModel.setXmlReadingErrors(""); // ensure that there are no old parsing errors
		builder.setErrorHandler(new TPCXMLErrorHandler(tpcModel));
		Document doc;

		try {
			doc = builder.parse(xmlFile);

			if (!tpcModel.getXmlReadingErrors().equals("")) {
				new FileInputStream(xmlFile).close();
				return;
			}

			Element programElement = doc.getDocumentElement();

			if (programElement != null && programElement.getTagName().equals(XMLConstants.tagNameTestProgram)) {
				String idString = programElement.getAttribute(XMLConstants.attributeNameID);
				Integer testProgramId = Integer.parseInt(idString);
				String name = programElement.getAttribute(XMLConstants.attributeNameName);
				String version = programElement.getAttribute(XMLConstants.attributeNameVersion);
				String object = programElement.getAttribute(XMLConstants.attributeNameObject);
				String description = programElement.getAttribute(XMLConstants.attributeNameDescription);
				Attr testObjectTypeNode = XMLUtilities.getAttributeNodeByName(programElement, XMLConstants.attributeNameObjectType);

				Integer testObjectType = 0;
				// Attribute may be not present in older versions
				if (testObjectTypeNode != null) {
					try {
						testObjectType = Integer.parseInt(testObjectTypeNode.getValue());
					} catch (NumberFormatException nfe) {
						// do nothing - we are dealing with an old version of result
						// file
//						String message = "Non parsable value ("
//								+ testObjectTypeNode.getValue()
//								+ ") for test object type.";
//						errorMessages.add(message);
					}
				} else {
//					String message = "Warning: you are using a result file produced by an old version of Phenotyper!";
//					errorMessages.add(message);
				}

				tpcModel.setCurrentTestProgramName(name);
				tpcModel.setCurrentTestProgramVersion(version);
				tpcModel.setCurrentTestProgramMessobjekt(object);
				tpcModel.setCurrentTestProgramDescription(description);
				tpcModel.setCurrentTestObjectType(testObjectType);

				ArrayList<TestProgramEntityRecord> testProgramEntityRecords = new ArrayList<TestProgramEntityRecord>();

				NodeList parameters = XMLUtilities.getNodeListByTagName(programElement, XMLConstants.tagNameParameter);

				for (int p = 0; p < parameters.getLength(); p++) {
					Element parameterElement = (Element) parameters.item(p);
					String parameterIdString = parameterElement.getAttribute(XMLConstants.attributeNameID);
					Integer parameterId = Integer.parseInt(parameterIdString);

					NodeList attributes = XMLUtilities.getNodeListByTagName(parameterElement, XMLConstants.tagNameAttribute);

					for (int a = 0; a < attributes.getLength(); a++) {
						TestProgramEntityRecord currentTestProgramEntityRecord = new TestProgramEntityRecord();

						Element attributeElement = (Element) attributes.item(a);
						String attributeIdString = attributeElement.getAttribute(XMLConstants.attributeNameID);
						Integer attributeId = Integer.parseInt(attributeIdString);
						String orderNumberString = attributeElement.getAttribute(XMLConstants.attributeNameOrderNumber);
						Integer orderNumber = 0;
						if (!orderNumberString.equals("")) { // order number is not mandatory
							orderNumber = Integer.parseInt(orderNumberString);
						}
						String dataType = attributeElement.getAttribute(XMLConstants.attributeNameDataType);
						String formatPattern = attributeElement.getAttribute(XMLConstants.attributeNameFormatPattern);
						
						currentTestProgramEntityRecord.setTestprogramm(testProgramId);
						currentTestProgramEntityRecord.setEntityID(parameterId);
						currentTestProgramEntityRecord.setValueID(attributeId);
						currentTestProgramEntityRecord.setOrderNumber(orderNumber);
						currentTestProgramEntityRecord.setDataType(dataType);
						currentTestProgramEntityRecord.setFormatPattern(formatPattern);

						testProgramEntityRecords.add(currentTestProgramEntityRecord);
					}

					NodeList attributesCategorical = XMLUtilities.getNodeListByTagName(parameterElement,
							XMLConstants.tagNameAttributeCategorical);

					for (int a = 0; a < attributesCategorical.getLength(); a++) {
						Element attributeElement = (Element) attributesCategorical.item(a);

						NodeList categoricalValueNodes = XMLUtilities.getNodeListByTagName(attributeElement,
								XMLConstants.tagNameValueCategorical);

						for (int b = 0; b < categoricalValueNodes.getLength(); b++) {

							Element currentCategoricalValueNode = (Element) categoricalValueNodes.item(b);
							
							String valueIDString = currentCategoricalValueNode.getAttribute(XMLConstants.attributeNameID);
							Integer valueID = Integer.parseInt(valueIDString);
							
							String orderNumberString = currentCategoricalValueNode.getAttribute(XMLConstants.attributeNameOrderNumber);
							Integer orderNumber = 0;
							if (!orderNumberString.equals("")) { // order number is not mandatory
								orderNumber = Integer.parseInt(orderNumberString);
							} 
							
							TestProgramEntityRecord currentTestProgramEntityRecord = new TestProgramEntityRecord();
							currentTestProgramEntityRecord.setTestprogramm(testProgramId);
							currentTestProgramEntityRecord.setEntityID(parameterId);
							currentTestProgramEntityRecord.setValueID(valueID);
							currentTestProgramEntityRecord.setOrderNumber(orderNumber);

							testProgramEntityRecords.add(currentTestProgramEntityRecord);
						}
					}
				}

				tpcModel.setTestProgramEntityRecords(testProgramEntityRecords);

			} else {
				String message = "File %s seems not to be a correct phenotyping scheme file!";
				message = String.format(message, tpcModel.getSourceFile());
				throw new Exception(message);
			}

		} catch (SAXException saxEx) {
			// the following step should ensure that an xml file
			// is closed even if an exception is thrown during reading
			new FileInputStream(xmlFile).close();
			throw saxEx;
		}
	}
}

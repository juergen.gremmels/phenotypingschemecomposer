package mpimp.phenotypingschemecomposer.test;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.apache.commons.codec.binary.Base64;

import mpimp.phenotypingschemecomposer.util.Encrypter;
import mpimp.phenotypingschemecomposer.util.TPCProperties;

public class EncrypterTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String decryptedString = "";
		
		try {
			decryptedString = decrypt("qu8zbotJ/przDV6azEnIYA==");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println(decryptedString);
		
	}

	public static String decrypt(String encryptedString) throws Exception {
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE,
				loadKeyFromFile("F:\\devel\\MyEclipseWorkspace\\PhenotypingSchemeComposer\\src\\keyfile_fromServerCOSMOS"));
		String decryptedString = "";
		// Decode base64 to get bytes
		byte[] dec = Base64.decodeBase64(encryptedString);

		// Decrypt
		byte[] utf8 = cipher.doFinal(dec);

		// Decode using utf-8
		decryptedString = new String(utf8, "UTF8");
		return decryptedString;
	}
	
	private static SecretKey loadKeyFromFile(String keyfileName) throws Exception {
		ObjectInputStream keyIn;
		SecretKey key = null;
		keyIn = new ObjectInputStream(new FileInputStream(keyfileName));
		key = (SecretKey) keyIn.readObject();
		keyIn.close();
		return key;
	}
	
}

-- MySQL dump 10.19  Distrib 10.3.39-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: phenotyping_schemes
-- ------------------------------------------------------
-- Server version	10.3.39-MariaDB-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `data_type`
--

DROP TABLE IF EXISTS `data_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `image`
--

DROP TABLE IF EXISTS `image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `image` (
  `image_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(45) NOT NULL,
  `value_id` smallint(6) NOT NULL,
  `copyright` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`image_id`),
  KEY `image_fk1` (`value_id`),
  CONSTRAINT `image_fk1` FOREIGN KEY (`value_id`) REFERENCES `test_mpiscore_values` (`ValueID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `operator`
--

DROP TABLE IF EXISTS `operator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `operator` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `allowlogin` tinyint(4) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `project_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_entity`
--

DROP TABLE IF EXISTS `project_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_entity` (
  `project_entity_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `entity_id` smallint(6) NOT NULL,
  `project_id` smallint(6) NOT NULL,
  PRIMARY KEY (`project_entity_id`),
  KEY `project_entity_fk1` (`entity_id`),
  KEY `project_entity_fk2` (`project_id`),
  CONSTRAINT `project_entity_fk1` FOREIGN KEY (`entity_id`) REFERENCES `test_mpiscore_entities` (`OrganID`),
  CONSTRAINT `project_entity_fk2` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_value`
--

DROP TABLE IF EXISTS `project_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_value` (
  `project_value_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `project_id` smallint(6) NOT NULL,
  `value_id` smallint(6) NOT NULL,
  PRIMARY KEY (`project_value_id`),
  KEY `project_value_fk1` (`value_id`),
  KEY `project_value_fk2` (`project_id`),
  CONSTRAINT `project_value_fk1` FOREIGN KEY (`value_id`) REFERENCES `test_mpiscore_values` (`ValueID`),
  CONSTRAINT `project_value_fk2` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=494 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_mpiscore_entities`
--

DROP TABLE IF EXISTS `test_mpiscore_entities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_mpiscore_entities` (
  `OrganID` smallint(6) NOT NULL AUTO_INCREMENT,
  `Parameter` varchar(50) DEFAULT NULL,
  `Parameter_dt` varchar(50) DEFAULT NULL,
  `PO_id` varchar(50) DEFAULT NULL,
  `definition` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`OrganID`)
) ENGINE=InnoDB AUTO_INCREMENT=817 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_mpiscore_values`
--

DROP TABLE IF EXISTS `test_mpiscore_values`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_mpiscore_values` (
  `ValueID` smallint(6) NOT NULL AUTO_INCREMENT,
  `Attribut_E` varchar(50) DEFAULT NULL,
  `value` varchar(50) DEFAULT NULL,
  `Attribut_D` varchar(50) DEFAULT NULL,
  `Wert_D` varchar(50) DEFAULT NULL,
  `format_pattern` varchar(45) DEFAULT NULL,
  `test_mpiscore_valuescol` varchar(45) DEFAULT NULL,
  `data_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`ValueID`),
  KEY `test_mpiscore_values_fk1_idx` (`data_type`),
  CONSTRAINT `test_mpiscore_values_fk1` FOREIGN KEY (`data_type`) REFERENCES `data_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `test_object_type`
--

DROP TABLE IF EXISTS `test_object_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `test_object_type` (
  `test_object_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `test_object_name` varchar(45) NOT NULL,
  `test_object_name_D` varchar(45) NOT NULL,
  PRIMARY KEY (`test_object_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testprogramm_entity`
--

DROP TABLE IF EXISTS `testprogramm_entity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testprogramm_entity` (
  `Testprogramm_Entity_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `testprogramm` smallint(6) DEFAULT NULL,
  `Entity` smallint(6) DEFAULT NULL,
  `Value` smallint(6) DEFAULT NULL,
  `OrderNumber` smallint(6) DEFAULT 0,
  PRIMARY KEY (`Testprogramm_Entity_id`),
  KEY `testprogramm_entity_FK1` (`testprogramm`),
  KEY `testprogramm_entity_FK2` (`Entity`),
  KEY `testprogramm_entity_FK3` (`Value`),
  CONSTRAINT `testprogramm_entity_FK1` FOREIGN KEY (`testprogramm`) REFERENCES `testprogramme` (`Testprogramm_id`),
  CONSTRAINT `testprogramm_entity_FK2` FOREIGN KEY (`Entity`) REFERENCES `test_mpiscore_entities` (`OrganID`),
  CONSTRAINT `testprogramm_entity_FK3` FOREIGN KEY (`Value`) REFERENCES `test_mpiscore_values` (`ValueID`)
) ENGINE=InnoDB AUTO_INCREMENT=2187 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testprogramme`
--

DROP TABLE IF EXISTS `testprogramme`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testprogramme` (
  `Testprogramm_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `Testprogrammname` varchar(200) DEFAULT NULL,
  `Version` varchar(20) DEFAULT NULL,
  `Messobjekt` varchar(100) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `TestObjectType` int(11) NOT NULL,
  PRIMARY KEY (`Testprogramm_id`),
  KEY `testprogramme_FK1` (`TestObjectType`),
  CONSTRAINT `testprogramme_FK1` FOREIGN KEY (`TestObjectType`) REFERENCES `test_object_type` (`test_object_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=243 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `testprogramme_operator`
--

DROP TABLE IF EXISTS `testprogramme_operator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `testprogramme_operator` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `testprogramm` smallint(6) NOT NULL,
  `operator` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `testprogramme_operator_FK1` (`operator`),
  KEY `testprogramme_operator_FK2` (`testprogramm`),
  CONSTRAINT `testprogramme_operator_FK2` FOREIGN KEY (`testprogramm`) REFERENCES `testprogramme` (`Testprogramm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=234 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `value_group`
--

DROP TABLE IF EXISTS `value_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `value_group` (
  `value_group_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `nameE` varchar(100) DEFAULT NULL,
  `nameD` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`value_group_id`),
  UNIQUE KEY `value_grop_id_UNIQUE` (`value_group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `value_group_value`
--

DROP TABLE IF EXISTS `value_group_value`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `value_group_value` (
  `value_group_value_id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `value_id` smallint(6) DEFAULT NULL,
  `value_group_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`value_group_value_id`),
  UNIQUE KEY `value_group_value_id_UNIQUE` (`value_group_value_id`),
  KEY `value_groups_FK1` (`value_id`),
  CONSTRAINT `value_groups_FK1` FOREIGN KEY (`value_id`) REFERENCES `test_mpiscore_values` (`ValueID`)
) ENGINE=InnoDB AUTO_INCREMENT=251 DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-09-18 15:05:12

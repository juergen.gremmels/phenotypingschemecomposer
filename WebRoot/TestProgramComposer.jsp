<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="TPCnew.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<script type="text/javascript" src="TPC.js"></script>
</head>

<body>

	<s:form action="TestProgramComposerAction" theme="simple">
		<s:hidden id="submitMethod" name="submitMethod" value="" />
		<s:hidden id="entityValueIndex" name="entityValueIndex" value="" />
		<h3>Manage phenotyping schemes</h3>
		<s:if test="model == null">
			<table>
				<tr>
					<td class="errorText"><s:text
							name="No access to Test Program Composer" /></td>
				</tr>
				<tr>
					<td><s:submit type="button" value="logout" name="buttonName">
							<s:text name="label.goToLogin" />
						</s:submit></td>
				</tr>
			</table>
		</s:if>
		<s:else>
			<s:if test="userAuthenticated == false">
				<table>
					<tr>
						<td class="errorText"><s:text
								name="No access to Test Program Composer" /></td>
					</tr>
					<tr>
						<td><s:submit type="button" value="logout" name="buttonName">
								<s:text name="label.goToLogin" />
							</s:submit></td>
					</tr>
				</table>
			</s:if>
			<s:else>
				<table class="tpc">
					<tr>
						<td colspan=2><s:actionerror /></td>
					</tr>

					<tr class="oddGrey">
						<td><s:text name="operator.firstName" /> <s:text
								name="operator.lastName" /></td>
						<td><nobr>
								<s:radio list="locales" name="request_locale" value="locale"
									theme="simple" />
							</nobr></td>
						<td><s:submit type="button" name="buttonName"
								value="goToAdministrationPage"
								disabled="operator.isAdministrator == false">
								<s:text name="label.goToAdministrationPage" />
							</s:submit></td>
					</tr>
					<tr class="oddGrey">
						<td><s:submit type="button" name="buttonName" value="logout">
								<s:text name="label.logout" />
							</s:submit></td>
						<td><s:submit type="button" name="buttonName"
								value="changeLanguage">
								<s:text name="label.changeLanguage" />
							</s:submit></td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan=3>
							<table width="100%">
								<tr>
									<td class="sectionHeadingGrey"><s:if test="newTestProgram">
											<s:text name="label.testProgramName" />
											<s:textfield name="currentTestProgramName" theme="simple" />
											<s:fielderror cssClass="errorText">
												<s:param value="%{'currentTestProgramName'}" />
											</s:fielderror>
										</s:if> <s:else>
											<s:text name="label.testProgramName" />
											<s:if test="currentLocaleString == 'en_US'">
												<s:select list="testProgramRecords"
													listKey="testprogramm_id" listValue="testprogrammname"
													name="currentTestProgramID" headerKey="0"
													headerValue="%{defaultOption}" value="currentTestProgramID"
													theme="simple" onchange="updateTestProgramDetails()" />
											</s:if>
											<s:elseif test="currentLocaleString == 'de_DE'">
												<s:select list="testProgramRecords"
													listKey="testprogramm_id" listValue="testprogrammname"
													name="currentTestProgramID" headerKey="0"
													headerValue="%{defaultOptionD}"
													value="currentTestProgramID" theme="simple"
													onchange="updateTestProgramDetails()" />
											</s:elseif>

											<s:fielderror cssClass="errorText">
												<s:param value="%{'currentTestProgramID'}" />
											</s:fielderror>
										</s:else></td>
									<td class="sectionHeadingGrey"><s:text
											name="label.version" /> <s:textfield
											name="currentTestProgramVersion" theme="simple" /> <s:fielderror
											cssClass="errorText">
											<s:param value="%{'currentTestProgramVersion'}" />
										</s:fielderror></td>
									<td class="sectionHeadingGrey">&nbsp;</td>
								</tr>
								<tr>
									<td class="sectionHeadingGrey"><s:text
											name="label.measuringObject" /> <s:textfield
											name="currentTestProgramMessobjekt" theme="simple" /> <s:fielderror
											cssClass="errorText">
											<s:param value="%{'currentTestProgramMessobjekt'}" />
										</s:fielderror></td>
									<td class="sectionHeadingGrey"><s:text
											name="label.testObjectType" /> <s:if
											test="currentLocaleString == 'en_US'">
											<s:if test="newTestProgram">
												<s:select list="testObjectTypeRecords"
													listKey="testObjectType_id" listValue="name"
													name="currentTestObjectType" headerKey="0"
													headerValue="%{defaultOption}"
													value="currentTestObjectType" theme="simple" />
											</s:if>
											<s:else>
												<s:textfield name="currentTestObjectTypeRecord.name"
													theme="simple" readonly="true" />
											</s:else>
										</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
											<s:if test="newTestProgram">
												<s:select list="testObjectTypeRecords"
													listKey="testObjectType_id" listValue="nameD"
													name="currentTestObjectType" headerKey="0"
													headerValue="%{defaultOptionD}"
													value="currentTestObjectType" theme="simple" />
											</s:if>
											<s:else>
												<s:textfield name="currentTestObjectTypeRecord.nameD"
													theme="simple" readonly="true" />
											</s:else>
										</s:elseif> <s:fielderror cssClass="errorText">
											<s:param value="%{'currentTestObjectType'}" />
										</s:fielderror></td>
									<td class="sectionHeadingGrey"><s:text
											name="label.description" /> <s:textfield
											name="currentTestProgramDescription" theme="simple" /></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<th align="left"><s:submit type="button" name="buttonName"
								value="compareEntity" title="%{getText('tooltip.sortedColumn')}"
								disabled="newTestProgram == false && testProgramLoaded == false">
								<s:text name="label.entity" />
							</s:submit></th>
						<th align="left"><s:submit type="button" name="buttonName"
								value="compareAttributeAndValue"
								title="%{getText('tooltip.sortedColumn')}"
								disabled="newTestProgram == false && testProgramLoaded == false">
								<s:text name="label.parameterAndValue" />
							</s:submit></th>
						<th align="left"><s:submit type="button" name="buttonName"
								value="compareOrderNumber"
								title="%{getText('tooltip.sortedColumn')}"
								disabled="newTestProgram == false && testProgramLoaded == false">
								<s:text name="label.orderNumber" />
							</s:submit></th>
					</tr>
					<s:sort comparator="tpcComparator"
						source="testProgramEntityRecords">
						<s:iterator status="rowStatusTPRecords">
							<tr class="oddGrey">
								<td><s:if test="currentLocaleString == 'en_US'">
										<s:select list="entities"
											name="testProgramEntityRecords[%{#rowStatusTPRecords.index}].entityID"
											value="%{entityID}" listKey="organID" listValue="parameter"
											headerKey="0" headerValue="%{defaultOption}" theme="simple" />
									</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
										<s:select list="entities"
											name="testProgramEntityRecords[%{#rowStatusTPRecords.index}].entityID"
											value="%{entityID}" listKey="organID" listValue="parameterD"
											headerKey="0" headerValue="%{defaultOptionD}" theme="simple" />
									</s:elseif></td>
								<td><s:if test="currentLocaleString == 'en_US'">
										<s:select list="values"
											name="testProgramEntityRecords[%{#rowStatusTPRecords.index}].valueID"
											value="%{valueID}" listKey="valueID"
											listValue="attributeAndValue" headerKey="0"
											headerValue="%{defaultOption}" theme="simple" />
									</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
										<s:select list="values"
											name="testProgramEntityRecords[%{#rowStatusTPRecords.index}].valueID"
											value="%{valueID}" listKey="valueID"
											listValue="attributeAndValueD" headerKey="0"
											headerValue="%{defaultOptionD}" theme="simple" />
									</s:elseif> <s:submit type="button" name="buttonName"
										value="removeEntityValuePair"
										onclick="removeEntityValuePair('%{#rowStatusTPRecords.index}')">
										<s:text name="label.remove" />
									</s:submit></td>
								<td><s:textfield
										name="testProgramEntityRecords[%{#rowStatusTPRecords.index}].orderNumber"
										theme="simple" cssStyle="width:2em" value="%{orderNumber}" />
								</td>
							</tr>
						</s:iterator>
					</s:sort>
					<tr>
						<td><s:fielderror cssClass="errorText">
								<s:param value="%{'entity'}" />
							</s:fielderror></td>
						<td><s:fielderror cssClass="errorText">
								<s:param value="%{'value'}" />
							</s:fielderror></td>
					</tr>
					<tr>
						<td colspan=3>
							<table width="100%">
								<colgroup>
									<col width="25%">
									<col width="25%">
									<col width="25%">
									<col width="25%">
								</colgroup>
								<tr class="oddGrey">
									<td colspan="4"><s:submit type="button" name="buttonName"
											value="addEntityValuePair" theme="simple"
											disabled="testProgramLoaded == false && newTestProgram == false">
											<s:text name="label.addParameterValueCombination"></s:text>
										</s:submit></td>
								</tr>
								<tr>
									<td><s:submit type="button" name="buttonName"
											value="newTestProgram" theme="simple"
											disabled="newTestProgram == true">
											<s:text name="label.newTestProgram" />
										</s:submit></td>
									<td><s:submit type="button" name="buttonName"
											value="cloneCurrentTestProgram" theme="simple"
											disabled="newTestProgram == true || testProgramLoaded == false">
											<s:text name="label.cloneTestProgram" />
										</s:submit></td>
									<td><s:submit type="button" name="buttonName"
											value="cancel" theme="simple"
											disabled="newTestProgram == false && testProgramChanged == false">
											<s:text name="label.cancel" />
										</s:submit></td>
									<td class="sectionHeadingGrey" align="right"><s:text
											name="label.project" /></td>
								</tr>
								<tr>
									<td class="sectionHeadingGrey"><s:submit type="button"
											name="buttonName" value="exportTestProgram" theme="simple"
											disabled="newTestProgram == true || testProgramLoaded == false">
											<s:text name="label.exportTestProgram" />
										</s:submit></td>
									<td class="sectionHeadingGrey"><s:submit type="button"
											name="buttonName" value="saveTestProgram" theme="simple"
											disabled="newTestProgram == false">
											<s:text name="label.saveTestProgram" />
										</s:submit></td>
									<td class="sectionHeadingGrey"><s:submit type="button"
											name="buttonName" value="updateTestProgram" theme="simple"
											disabled="newTestProgram == true || testProgramLoaded == false">
											<s:text name="label.updateTestProgram" />
										</s:submit></td>
									<td class="sectionHeadingGrey"><s:if
											test="currentLocaleString == 'en_US'">
											<s:select list="projects" listKey="project_id"
												listValue="name" name="currentProjectID"
												value="currentProjectID" headerKey="0"
												headerValue="%{noProjectOption}" onchange="projectChanged()" />
										</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
											<s:select list="projects" listKey="project_id"
												listValue="name" name="currentProjectID"
												value="currentProjectID" headerKey="0"
												headerValue="%{noProjectOptionD}"
												onchange="projectChanged()" />
										</s:elseif></td>
								</tr>
								<tr>
									<td class="sectionHeadingGrey"><s:submit type="button"
											name="buttonName" value="exportImages" theme="simple"
											disabled="testProgramLoaded == false">
											<s:text name="label.exportImages" />
										</s:submit></td>
									<td class="sectionHeadingGrey"><s:submit type="button"
											name="buttonName" value="importTestProgram" theme="simple"
											disabled="newTestProgram == true">
											<s:text name="label.importTestProgram" />
										</s:submit></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</s:else>
		</s:else>
	</s:form>
</body>
</html>

<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<table style="width: 100%">
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>

	<s:if test="newValueGroup">
		<tr>
			<td class="sectionHeadingGrey"><s:text name="label.valueGroupE" />
				<s:textfield name="currentValueGroupForAdminName" theme="simple" />
				<s:fielderror cssClass="errorText">
					<s:param value="%{'currentValueGroupForAdminName'}" />
				</s:fielderror> <s:submit type="button" name="buttonName" value="saveValueGroup">
					<s:text name="label.saveValueGroup" />
				</s:submit></td>
			<td class="sectionHeadingGrey"><s:submit type="button"
					name="buttonName" value="cancelNewValueGroup">
					<s:text name="label.cancel" />
				</s:submit></td>
		</tr>
		<tr>
			<td class="sectionHeadingGrey"><s:text name="label.valueGroupD" />
				<s:textfield name="currentValueGroupForAdminNameD" theme="simple" />
				<s:fielderror cssClass="errorText">
					<s:param value="%{'currentValueGroupForAdminNameD'}" />
				</s:fielderror></td>
		</tr>
	</s:if>
	<s:else>
		<tr>
			<td class="sectionHeadingGrey"><s:if
					test="currentLocaleString == 'en_US'">
					<s:text name="label.valueGroup" />
					<s:select list="valueGroups" listKey="valueGroup_id"
						listValue="nameE" name="currentValueGroupForAdminID" headerKey="0"
						headerValue="%{defaultOption}"
						onchange="updateValueGroupSpecificData()" />
				</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
					<s:text name="label.valueGroup" />
					<s:select list="valueGroups" listKey="valueGroup_id"
						listValue="nameD" name="currentValueGroupForAdminID" headerKey="0"
						headerValue="%{defaultOptionD}"
						onchange="updateValueGroupSpecificData()" />
				</s:elseif> <s:submit type="button" name="buttonName" value="newValueGroup">
					<s:text name="label.newValueGroup" />
				</s:submit></td>
			<td class="sectionHeadingGrey"><s:submit type="button"
					name="buttonName" value="updateValueGroup">
					<s:text name="label.updateValueGroup" />
				</s:submit></td>
		</tr>
	</s:else>
	<tr class="oddGrey">
		<td colspan="3"><s:if test="currentLocaleString == 'en_US'">
				<s:optiontransferselect leftTitle="%{getText('label.allValues')}"
					rightTitle="%{getText('label.valuesForValueGroup')}"
					name="notSelectedValueIDsForValueGroup" list="notSelectedValuesForValueGroup"
					listKey="valueID" listValue="attributeAndValue"
					allowSelectAll="false" doubleName="selectedValueIDsForValueGroup"
					doubleList="selectedValuesForValueGroup" doubleListKey="valueID"
					doubleListValue="attributeAndValue" allowUpDownOnLeft="false"
					allowUpDownOnRight="false" />
			</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
				<s:optiontransferselect leftTitle="%{getText('label.allValues')}"
					rightTitle="%{getText('label.valuesForValueGroup')}"
					name="notSelectedValueIDsForValueGroup" list="notSelectedValuesForValueGroup"
					listKey="valueID" listValue="attributeAndValueD"
					allowSelectAll="false" doubleName="selectedValueIDsForValueGroup"
					doubleList="selectedValuesForValueGroup" doubleListKey="valueID"
					doubleListValue="attributeAndValueD" allowUpDownOnLeft="false"
					allowUpDownOnRight="false" />
			</s:elseif></td>
	</tr>
</table>
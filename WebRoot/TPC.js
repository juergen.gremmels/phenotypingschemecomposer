/*
Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation
*/

function updateTestProgramDetails() {
	document.getElementById("submitMethod").value = "testProgramChanged";
	TestProgramComposerAction.submit();
}
function projectChanged() {
	document.getElementById("submitMethod").value = "projectChanged";
	TestProgramComposerAction.submit();
}
function removeEntityValuePair(index) {
	document.getElementById("entityValueIndex").value = index;
}

function updateProjectSpecificData() {
	document.getElementById("submitMethod").value = "updateProjectSpecificData";
	TPCAdministrationAction.submit();
}

function updateValueGroupSpecificData() {
	document.getElementById("submitMethod").value = "updateValueGroupSpecificData";
	TPCAdministrationAction.submit();
}

function updateEntityData() {
	document.getElementById("submitMethod").value = "updateEntityData";
	TPCAdministrationAction.submit();
}
function updateAttributeAndValueData() {
	document.getElementById("submitMethod").value = "updateAttributeAndValueData";
	TPCAdministrationAction.submit();
}

/*
 * Much of this Javascript is borrowed from the early chapters of another
 * Manning title, Ajax in Action. Thanks to the authors of that book ( Dave
 * Crane, Eric Pascarello, Darren James ) for letting us borrow some Ajax
 * connectivity code.
 */

var req = null;
var console = null;
var READY_STATE_UNINITIALIZED = 0;
var READY_STATE_LOADING = 1;
var READY_STATE_LOADED = 2;
var READY_STATE_INTERACTIVE = 3;
var READY_STATE_COMPLETE = 4;

function sendRequest(url, params, HttpMethod) {

	if (!HttpMethod) {
		HttpMethod = "GET";
	}
	req = initXMLHTTPRequest();

	if (req) {
		req.onreadystatechange = onReadyState;
		req.open(HttpMethod, url, true);
		req.setRequestHeader("Content-Type",
				"application/x-www-form-urlencoded");
		req.send(params);
	}
}

function initXMLHTTPRequest() {
	var xRequest = null;
	if (window.XMLHttpRequest) {
		xRequest = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		xRequest = new ActiveXObject("Microsoft.XMLHTTP");
	}
	return xRequest;
}

/*
 * This function will handle the result returned by the Ajax call
 */
function onReadyState() {
	var ready = req.readyState;
	var jsonObject = null;

	if (ready == READY_STATE_COMPLETE) {

		/*
		 * Use the JavaScript eval() function to convert the JSON response text
		 * into a JavaScript object
		 */
		jsonObject = eval("(" + req.responseText + ")");
		toFinalConsole(jsonObject);
	}
}

function toFinalConsole(jsonObject) {
	
	var inputNameDValue = "";
	var inputPOidValue = "";
	var inputDefinitionValue = "";
	
	if (jsonObject != null) {
		inputNameDValue = jsonObject.entity.parameterD_;
		inputPOidValue = jsonObject.entity.pO_id_;
		inputDefinitionValue = jsonObject.entity.definition_;
	}
	
	var inputNameD = document.getElementById("entityD");
	inputNameD.setAttribute("value", inputNameDValue);
	
	var inputPOid = document.getElementById("pO_Id");
	inputPOid.setAttribute("value", inputPOidValue);

	var inputDefinition = document.getElementById("definition");
	inputDefinition.innerHTML = inputDefinitionValue;
}

function removeAllChildren(node) {
	var childCount = node.childNodes.length;
	for ( var count = 1; count <= childCount; count++) {
		node.removeChild(node.childNodes[0]);
	}
}

function fetchEntity() {
	//console = document.getElementById('console');
	var selectBox = document
			.getElementById('TPCAdministrationAction_currentEntityForAdminID');
	var selectedIndex = selectBox.selectedIndex;
	var selectedValue = selectBox.options[selectedIndex].value;
	sendRequest("TPCAdministrationAction.action?submitMethod=updateEntityData",
			"currentEntityForAdminID=" + selectedValue, "POST");
}

function fetchUser() {
	console = document.getElementById('console');
	var selectBox = document.getElementById('AjaxRetrieveUser_username');
	var selectedIndex = selectBox.selectedIndex;
	var selectedValue = selectBox.options[selectedIndex].value

	// note that we are calling a Struts2 action
	// response will be handled by the function onReadyState()
	sendRequest("AjaxRetrieveUser.action", "username=" + selectedValue, "POST");
}
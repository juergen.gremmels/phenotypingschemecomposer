<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<table style="width: 100%">
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr class="oddGrey">
		<s:if test="newEntity">
			<td>&nbsp;</td>
			<td><s:submit type="button" name="buttonName" value="saveEntity">
					<s:text name="label.saveEntity" />
				</s:submit> <s:submit type="button" name="buttonName" value="cancelNewEntity">
					<s:text name="label.cancel" />
				</s:submit></td>
		</s:if>
		<s:else>
			<td><s:text name="label.entity" /></td>
			<td><s:select list="entitiesForAdmin" listKey="organID"
					listValue="parameter" name="currentEntityForAdminID" headerKey="0"
					headerValue="%{defaultOption}" onchange="updateEntityData()" /> <s:submit
					type="button" name="buttonName" value="updateEntity">
					<s:text name="label.updateEntity" />
				</s:submit></td>
			<td><s:submit type="button" name="buttonName" value="newEntity">
					<s:text name="label.newEntity" />
				</s:submit></td>
		</s:else>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.entityE" /></td>
		<td><s:textfield name="currentEntityForAdminParameter" /> <s:fielderror
				cssClass="errorText">
				<s:param value="%{'currentEntityForAdminParameter'}" />
			</s:fielderror></td>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.entityD" /></td>
		<td colspan="2"><s:textfield id="entityD"
				name="currentEntityForAdminParameterD" cssStyle="width:100%" /> <s:fielderror
				cssClass="errorText">
				<s:param value="%{'currentEntityForAdminParameterD'}" />
			</s:fielderror></td>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.pO_Id" /></td>
		<td colspan="2"><s:textfield id="pO_Id"
				name="currentEntityForAdminPO_id" cssStyle="width:100%" /> <s:fielderror
				cssClass="errorText">
				<s:param value="%{'currentEntityForAdminPO_id'}" />
			</s:fielderror></td>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.definition" /></td>
		<td colspan="2"><s:textarea id="definition"
				name="currentEntityForAdminDefinition" rows="5" cols="60" /> <s:fielderror
				cssClass="errorText">
				<s:param value="%{'currentEntityForAdminDefinition'}" />
			</s:fielderror></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td><s:a href="%{linkToOntologyLookupService}" target="_blank">
				<s:text name="label.ontologyLookupService" />
			</s:a></td>
	</tr>
</table>
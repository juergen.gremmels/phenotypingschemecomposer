<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<HTML>
<head>
</head>
<body>
	<p>
	<h3>An Error occured! It may help to restart your web browser and
		then try again. It this does not work anyway, please contact your
		system administrator!</h3>
	</p>
	<p>
		Exception name:
		<s:property value="exception" />
	</p>
	<p>
		Exception stack:
		<s:property value="exceptionStack" />
	</p>
</body>
</HTML>


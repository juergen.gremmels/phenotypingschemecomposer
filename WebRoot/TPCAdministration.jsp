<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<link rel="stylesheet" type="text/css" href="TPCnew.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<sx:head debug="false" cache="false" compressed="false" />
<s:head />
<script type="text/javascript" src="TPC.js"></script>
</head>

<body>
	<s:form action="TPCAdministrationAction" theme="simple">
		<s:hidden id="submitMethod" name="submitMethod" value="" />
		<h3>TPC Administration Page</h3>
		<s:if test="model == null">
			<table>
				<tr>
					<td class="errorText"><s:text
							name="Test program composer did not terminate normally, please log on again!" />
					</td>
				</tr>
			</table>
		</s:if>
		<s:else>
			<s:if test="userAuthenticated == false">
				<table>
					<tr>
						<td class="errorText"><s:text
								name="No administration rights." /></td>
					</tr>
				</table>
			</s:if>
			<s:else>
				<table class="tpc">
					<tr>
						<td><sx:tabbedpanel
								cssStyle="min-height: 500px; min-width: 500px" id="tabContainer"
								selectedTab="%{selectedTab}">
								<sx:div label="%{getText('label.editEntities')}" id="tab1"
									refreshOnShow="false">
									<s:include value="Entities.jsp" />
								</sx:div>
								<sx:div label="%{getText('label.editAttributesAndValues')}"
									id="tab2" refreshOnShow="false">
									<s:include value="AttributesAndValues.jsp" />
								</sx:div>
								<sx:div label="%{getText('label.editValueGroups')}" id="tab3"
									refreshOnShow="false">
									<s:include value="ValueGroups.jsp" />
								</sx:div>
								<sx:div label="%{getText('label.editProjects')}" id="tab4"
									refreshOnShow="false">
									<s:include value="Projects.jsp" />
								</sx:div>
							</sx:tabbedpanel></td>
					</tr>
					<tr>
						<td><s:url action="TestProgramComposerAction" var="tpcUrl">
							</s:url> <s:a href="%{tpcUrl}">
								<s:text name="label.backToMainPage" />
							</s:a></td>
					</tr>
				</table>
			</s:else>
		</s:else>
	</s:form>
</body>
</html>

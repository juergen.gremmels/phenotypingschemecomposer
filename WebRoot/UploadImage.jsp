<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<link rel="stylesheet" type="text/css" href="TPCnew.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
</head>

<body>
	<s:form action="UploadImageAction" method="post"
		enctype="multipart/form-data" theme="simple">
		<table>
			<tr>
				<td><s:file name="currentImageFile" theme="simple"
						accept="image/*" /> <s:fielderror cssClass="errorText">
						<s:param value="%{'currentImageFile'}" />
					</s:fielderror></td>
			</tr>
			<tr>
				<td><s:text name="label.imageCopyright" /> <s:textfield
						name="currentImageCopyright" /> <s:fielderror
						cssClass="errorText">
						<s:param value="%{'currentImageCopyright'}" />
					</s:fielderror></td>
			</tr>
			<tr>
				<td><s:submit type="button" name="buttonName"
						value="loadImageFile">
						<s:text name="label.uploadImage" />
					</s:submit></td>
			</tr>
			<tr>
				<td class="sectionHeadingGrey"><s:submit type="button"
						name="buttonName" value="backToAdministrationPage">
						<s:text name="label.backToAdministrationPage" />
					</s:submit></td>
			</tr>
		</table>
	</s:form>
</body>
</html>
<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->

<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<table style="width: 100%">
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr class="oddGrey">
		<s:if test="newAttributeAndValue">
			<td>&nbsp;</td>
			<td><s:submit type="button" name="buttonName"
					value="saveAttributeAndValue">
					<s:text name="label.saveAttributeAndValue" />
				</s:submit></td>
			<td><s:submit type="button" name="buttonName"
					value="cancelNewAttributeAndValue">
					<s:text name="label.cancel" />
				</s:submit></td>
		</s:if>
		<s:else>
			<td><s:text name="label.attributeAndValue" /></td>
			<td><s:select list="valuesForAdmin" listKey="valueID"
					listValue="attributeAndValue" name="currentValueForAdminID"
					headerKey="0" headerValue="%{defaultOption}"
					onchange="updateAttributeAndValueData()" /> <s:submit
					type="button" name="buttonName" value="updateAttributeAndValue">
					<s:text name="label.updateAttributeAndValue" />
				</s:submit></td>
			<td><s:submit type="button" name="buttonName"
					value="newAttributeAndValue">
					<s:text name="label.newAttributeAndValue" />
				</s:submit></td>
		</s:else>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.attributeE" /></td>
		<td colspan="2"><s:textfield
				name="currentValueForAdminAttribut_e" cssStyle="width:100%" /> <s:fielderror
				cssClass="errorText">
				<s:param value="%{'currentValueForAdminAttribut_e'}" />
			</s:fielderror></td>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.attributeD" /></td>
		<td colspan="2"><s:textfield
				name="currentValueForAdminAttribut_d" cssStyle="width:100%" /> <s:fielderror
				cssClass="errorText">
				<s:param value="%{'currentValueForAdminAttribut_d'}" />
			</s:fielderror></td>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.value" /></td>
		<td colspan="2"><s:textfield name="currentValueForAdminValue"
				cssStyle="width:100%" /></td>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.valueD" /></td>
		<td colspan="2"><s:textfield name="currentValueForAdminValueD"
				cssStyle="width:100%" /> <s:fielderror cssClass="errorText">
				<s:param value="%{'currentAttributeAndValueForAdmin'}" />
			</s:fielderror></td>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.dataType" /></td>
		<td colspan="2"><s:select list="dataTypes" listKey="dataTypeID"
				listValue="dataTypeName" name="currentDataTypeForAdmin"
				headerKey="0" headerValue="%{defaultOption}" /></td>
	</tr>
	<tr class="oddGrey">
		<td><s:text name="label.formatPattern" /></td>
		<td colspan="2"><s:textfield name="currentFormatPatternForAdmin"
				cssStyle="width:100%" /> <s:fielderror cssClass="errorText">
				<s:param value="%{'currentFormatPatternForAdmin'}" />
			</s:fielderror></td>
	</tr>
	<tr class="oddGrey">
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<td colspan=3 align="center"><img
			alt="<s:property value="%{getText('label.noImage')}" />"
			src="<s:property value="currentImageFileForDisplay"/>" /></td>
	</tr>
	<s:if test='currentImageCopyright != \"\"'>
		<tr>
			<td colspan=3 align="center"><s:text name="label.imageCopyright" />
				<s:text name="currentImageCopyright" /></td>
		</tr>
	</s:if>
	<tr>
		<td><s:actionerror /></td>
	</tr>
	<tr class="oddGrey">
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<s:if test="aboutToSaveChanges == false">
			<td><s:submit type="button" name="buttonName"
					value="wantsToSaveChanges" disabled="currentValueForAdminID == 0">
					<s:text name="label.uploadImage" />
				</s:submit></td>
		</s:if>
		<s:else>
			<td><s:text name="label.saveChangesWarningMessage" /> <s:submit
					type="button" name="buttonName" value="cancelSaveChanges">
					<s:text name="label.cancel" />
				</s:submit> <s:submit type="button" name="buttonName" value="uploadImage">
					<s:text name="label.uploadImage" />
				</s:submit></td>
		</s:else>
	</tr>
</table>
<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="sx" uri="/struts-dojo-tags"%>

<table>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr>
		<s:if test="newProject">
			<td class="sectionHeadingGrey"><s:text name="label.project" />
				<s:textfield name="currentProjectForAdminName" theme="simple" /> <s:fielderror
					cssClass="errorText">
					<s:param value="%{'currentProjectForAdminName'}" />
				</s:fielderror> <s:submit type="button" name="buttonName" value="saveProject">
					<s:text name="label.saveProject" />
				</s:submit></td>
			<td class="sectionHeadingGrey"><s:submit type="button"
					name="buttonName" value="cancelNewProject">
					<s:text name="label.cancel" />
				</s:submit></td>
		</s:if>
		<s:else>
			<td class="sectionHeadingGrey"><s:if
					test="currentLocaleString == 'en_US'">
					<s:text name="label.project" />
					<s:select list="projects" listKey="project_id" listValue="name"
						name="currentProjectForAdminID" headerKey="0"
						headerValue="%{defaultOption}"
						onchange="updateProjectSpecificData()" />
				</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
					<s:select list="projects" listKey="project_id" listValue="name"
						name="currentProjectForAdminID" headerKey="0"
						headerValue="%{defaultOptionD}"
						onchange="updateProjectSpecificData()" />
				</s:elseif> <s:submit type="button" name="buttonName" value="newProject">
					<s:text name="label.newProject" />
				</s:submit></td>
			<td class="sectionHeadingGrey"><s:submit type="button"
					name="buttonName" value="updateProject">
					<s:text name="label.updateProject" />
				</s:submit></td>
		</s:else>
	</tr>
	<tr class="oddGrey">
		<td colspan="3"><s:if test="currentLocaleString == 'en_US'">
				<s:optiontransferselect leftTitle="%{getText('label.allEntities')}"
					rightTitle="%{getText('label.entitiesForProject')}"
					name="notSelectedEntityIDs" list="notSelectedEntities"
					listKey="organID" listValue="parameter" allowSelectAll="false"
					doubleName="selectedEntityIDs" doubleList="selectedEntities"
					doubleListKey="organID" doubleListValue="parameter"
					allowUpDownOnLeft="false" allowUpDownOnRight="false" />
			</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
				<s:optiontransferselect leftTitle="%{getText('label.allEntities')}"
					rightTitle="%{getText('label.entitiesForProject')}"
					name="notSelectedEntityIDs" list="notSelectedEntities"
					listKey="organID" listValue="parameterD" allowSelectAll="false"
					doubleName="selectedEntityIDs" doubleList="selectedEntities"
					doubleListKey="organID" doubleListValue="parameterD"
					allowUpDownOnLeft="false" allowUpDownOnRight="false" />
			</s:elseif></td>
	</tr>
	<tr>
		<td colspan=3>&nbsp;</td>
	</tr>
	<tr class="oddGrey">
		<td colspan="3"><s:if test="currentLocaleString == 'en_US'">
				<s:optiontransferselect leftTitle="%{getText('label.allValues')}"
					rightTitle="%{getText('label.valuesForProject')}"
					name="notSelectedValueIDs" list="notSelectedValues"
					listKey="valueID" listValue="attributeAndValue"
					allowSelectAll="false" doubleName="selectedValueIDs"
					doubleList="selectedValues" doubleListKey="valueID"
					doubleListValue="attributeAndValue" allowUpDownOnLeft="false"
					allowUpDownOnRight="false" />
			</s:if> <s:elseif test="currentLocaleString == 'de_DE'">
				<s:optiontransferselect leftTitle="%{getText('label.allValues')}"
					rightTitle="%{getText('label.valuesForProject')}"
					name="notSelectedValueIDs" list="notSelectedValues"
					listKey="valueID" listValue="attributeAndValueD"
					allowSelectAll="false" doubleName="selectedValueIDs"
					doubleList="selectedValues" doubleListKey="valueID"
					doubleListValue="attributeAndValueD" allowUpDownOnLeft="false"
					allowUpDownOnRight="false" />
			</s:elseif></td>
	</tr>
</table>
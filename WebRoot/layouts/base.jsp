<!--

Phenotyper - a tool for collecting phenotyping data using mobile terminals
Copyright (C) 2015,  gremmels(at)mpimp-golm.mpg.de

Phenotyper is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>

Contributors:
gremmels(at)mpimp-golm.mpg.de - initial API and implementation

-->
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<html>
<head>
<title><tiles:insertAttribute name="title" /></title>
<link rel=stylesheet type="text/css" href="TPCnew.css">
</head>
<body>
	<div>
		<div class="head">
			<h1>Phenotyping test program composer</h1>
		</div>
		<div class="left">
			<div class="mpimp_logo"><tiles:insertAttribute name="mpimp_logo" /></div>
			<div class="minerva"><tiles:insertAttribute name="minerva" /></div>
			<div class="impressum"><tiles:insertAttribute name="impressum" /></div>
			<br>
			<div class="version">Version: 2024-09-13</div>
		</div>
		<div class="content">
			<tiles:insertAttribute name="body" />
		</div>
	</div>
</body>
</html>